<?php
/**
 * Addons and code snippets for Nette Framework. (unofficial)
 *
 * @author   Jan Tvrdík
 * @license  MIT
 */

abstract
class
BaseControl
extends
Control{protected$autoSetupTemplateFile=TRUE;protected
function
createTemplate(){$template=parent::createTemplate();if($this->autoSetupTemplateFile)$template->setFile($this->getTemplateFilePath());return$template;}protected
function
getTemplateFilePath(){$reflection=$this->getReflection();$dir=dirname($reflection->getFileName());$filename=$reflection->getShortName().'.latte';return$dir.DIRECTORY_SEPARATOR.$filename;}}