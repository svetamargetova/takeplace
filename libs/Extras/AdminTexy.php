<?php

class AdminTexy extends Texy
{
    public function __construct()
    {
        parent::__construct();
        
        $this->linkModule->root = Environment::getVariable('baseUri') . "upload/pages/";
        $this->imageModule->fileRoot = WWW_DIR . "/upload/pages/";
        $this->imageModule->root = Environment::getVariable("baseUri") . "upload/pages/";
//        $this->addHandler('phrase', array('AdminTexy', 'phraseHandler'));
        $this->headingModule->top = 3;
        $this->headingModule->balancing = TexyHeadingModule::FIXED;
        $this->headingModule->moreMeansHigher = FALSE;
        // zbytek nastavení záleží na osobním vkusu uživatele
    }


}
