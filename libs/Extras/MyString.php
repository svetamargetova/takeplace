<?php
/**
 * MyString Helper
 *
 * @author     Endrju
 * @package    Application
 */
abstract class MyString {


        /**
         * Truncates string containing XHTML tags to maximal length
         * @param string UTF-8 encoding
         * @param int
         * @param string UTF-8 encoding
         * @return string
         * @copyright Jakub Vrána, http://php.vrana.cz/
         * @author Endrju (modifications)
         */
        public static function xhtmlTruncate($s, $maxLen, $append = "\xE2\x80\xA6")
        {
                // ma vubec smysl retezec zkracovat?
                if (iconv_strlen($s, 'UTF-8') > $maxLen) {
                        // zkratime $maxLen o delku $append
                        $maxLen = $maxLen - iconv_strlen($append, 'UTF-8');
                        // pokud je nyni $maxLen kratsi bez $appedm vratime samotonty $append
                        if ($maxLen < iconv_strlen($append, 'UTF-8')) {
                                return $append;
                        }

                        // vybrane znaky, ktere muzou ukoncovat vetu nebo vyznam casti vety
                        $separators = array (' ', ',', '.', ';', '?', '!', ':');
                        $pos = 0; // pozice posledniho nalezeneho separatoru

                        // prekodujeme z UTF-8 do windows-1250,
                        // znaky s diakritikou atp pak budou pocitany jako jeden cely znak
                        $s = iconv('UTF-8', 'windows-1250//TRANSLIT', $s);
                        $INITAL_S = $s; // originalni vstupni retezec $s, ktery nebude po dobu behu programu zmenen.
                        $append = iconv('UTF-8', 'windows-1250//TRANSLIT', $append);

                        // odstranime neviditelne znaky,
                        // ktere se ve vygenerovanem a zobrazenem HTML textu stejne nezobrazi
                        $customWhitespaces = array(chr(0x09),chr(0x0a),chr(0x0d),chr(0x00),chr(0x0b));
                        foreach ($customWhitespaces AS $customWhitespace) {
                                $s = trim($s, $customWhitespace);
                        }

                        $length = 0;
                        $tags = array(); // dosud neuzavřené značky
                        for ($i=0; $i < strlen($s) && $length < $maxLen; $i++) {
                                switch ($s[$i]) {
                                        case '<':
                                                // načtení značky
                                                $start = $i+1;
                                                while ($i < strlen($s) && $s[$i] != '>' && !ctype_space($s[$i])) {
                                                        $i++;
                                                }
                                                $tag = strtolower(substr($s, $start, $i - $start));
                                                // přeskočení případných atributů
                                                $in_quote = '';
                                                while ($i < strlen($s) && ($in_quote || $s[$i] != '>')) {
                                                        if (($s[$i] == '"' || $s[$i] == "'") && !$in_quote) {
                                                                $in_quote = $s[$i];

                                                        } elseif ($in_quote == $s[$i]) {
                                                                $in_quote = '';
                                                        }
                                                        $i++;
                                                }
                                                if ($s[$start] == '/') { // uzavírací značka
                                                        array_shift($tags); // v XHTML dokumentu musí být vždy uzavřena poslední neuzavřená značka

                                                } elseif ($s[$i-1] != '/') { // otevírací značka
                                                        array_unshift($tags, $tag);
                                                }
                                                break;

                                        case '&':
                                                $length++;
                                                while ($i < strlen($s) && $s[$i] != ';') {
                                                        $i++;
                                                }
                                                break;

                                        default:
                                                $length++;

                                                /* V případě kódování UTF-8:
                                                while ($i+1 < strlen($s) && ord($s[$i+1]) > 127 && ord($s[$i+1]) < 192) {
                                                        $i++;
                                                }
                                                */

                                                // je znak separatorem?
                                                if (in_array($s[$i], $separators)) {
                                                        // * a neni nasledujici (nebo predchozi) znak totozny,
                                                        // jako nyni nacteny separator?
                                                        if (($s[$i] != $s[$i+1]) && ($s[$i-1] != $s[$i])) {
                                                                $pos = $i; // pak ulozime pozici separatoru
                                                        } // nakonec tak ziskame pozici posledniho separatoru

                                                        // * tou druhou podminkou chceme zachytit pripady, kdy je v textu
                                                        // nekolik separatotu (napr. tecek) za sebou, pak to nebudeme povazovat za separator,
                                                        // ale jako vyznam k predchazejicimu slovu (vete).
                                                        // Navic pokud by takovy separator byl na konci oriznuteho textu, bude tak v podstate
                                                        // nahrazen volitelnym retezcem $append (defaultne trojtecka).
                                                }
                                }
                        }

                        // pokud nenalezneme hledany pocet znaku, obsah je nejspis slozen ciste z HTML tagu
                        // (napr. flashova videa a jine medialni objekty)
                        if ($length >= $maxLen) {

                                $s = substr($s, 0, $i);

                                // uzavreme vsechny tagy
                                $enclosingTags = "";
                                if ($tags) {
                                        $enclosingTags .= "</" . implode("></", $tags) . ">";
                                }

                                // Nyni potrebujeme probublat od konce pres vsechny tagy az na konec zobrazovaneho textu $s
                                // (tam pak budeme pridavat $append)
                                $s_beforeInnerEnclosingTags = $s;
                                $innerEnclosingTags = "";
                                while (substr(rtrim($s_beforeInnerEnclosingTags), - 1, 1) == ">") {
                                        $innerEnclosingTags = strrchr($s_beforeInnerEnclosingTags, "<");
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, strlen($s_beforeInnerEnclosingTags) - strlen($innerEnclosingTags));
                                }

                                // Pokud je nastaven $append na trojtecku,
                                // orezeme jeste samotne tecky na konci rezce (pokud nejake jsou)
                                if ($append == iconv('UTF-8', 'windows-1250//TRANSLIT', "\xE2\x80\xA6")) {
                                        $s_beforeInnerEnclosingTags = rtrim($s_beforeInnerEnclosingTags, '.');
                                }

                                // pokud byl nejaky separator nalezen
                                // a zaroven znak za zkracovanym textem neni separatorem
                                if (($pos > 0) && (!in_array(substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1), $separators))) {
                                        // tak orizneme text za poslednim nalezenym separatorem
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, $pos);
                                }
                                // nebo take pokud byl nejaky separator nalezen
                                // a zaroven 2 znaky za zkracovanym textem jsou 2 stejne separatory (viz. * o neco vyse - stejny pripad)
                                else if (($pos > 0) && (in_array(substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1), $separators)) && ((substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1) == ((substr($INITAL_S, strlen($s_beforeInnerEnclosingTags) + 1, 1)))))) {
                                        // tak orizneme text za poslednim nalezenym separatorem a dva vyse zminene ignorujeme
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, $pos);
                                }

                                // Nyni vse spojime dohromady a pripojime $append
                                $s = $s_beforeInnerEnclosingTags . $append . $innerEnclosingTags . $enclosingTags;
                            }

                        // Vystupni retezec prekodujeme zpet z windows-1250 do UTF-8
                        $s = iconv('windows-1250', 'UTF-8//TRANSLIT', $s);
                }

                return $s;
        }



        /**
         * Truncates string containing HTML tags to maximal length
         * @param string UTF-8 encoding
         * @param int
         * @param string UTF-8 encoding
         * @return string
         * @copyright Jakub Vrána, http://php.vrana.cz/
         * @author Endrju (modifications)
         */
        public static function htmlTruncate($s, $maxLen, $append = "\xE2\x80\xA6")
        {
                // ma vubec smysl retezec zkracovat?
                if (iconv_strlen($s, 'UTF-8') > $maxLen) {
                        // zkratime $maxLen o delku $append
                        $maxLen = $maxLen - iconv_strlen($append, 'UTF-8');
                        // pokud je nyni $maxLen kratsi bez $appedm vratime samotonty $append
                        if ($maxLen < iconv_strlen($append, 'UTF-8')) {
                                return $append;
                        }

                        static $empty_tags = array('area', 'base', 'basefont', 'br', 'col', 'frame', 'hr', 'img', 'input', 'isindex', 'link', 'meta', 'param');

                        // vybrane znaky, ktere muzou ukoncovat vetu nebo vyznam casti vety
                        $separators = array (' ', ',', '.', ';', '?', '!', ':');
                        $pos = 0; // pozice posledniho nalezeneho separatoru

                        // prekodujeme z UTF-8 do windows-1250,
                        // znaky s diakritikou atp pak budou pocitany jako jeden cely znak
                        $s = iconv('UTF-8', 'windows-1250//TRANSLIT', $s);
                        $INITAL_S = $s; // originalni vstupni retezec $s, ktery nebude po dobu behu programu zmenen.
                        $append = iconv('UTF-8', 'windows-1250//TRANSLIT', $append);

                        // odstranime neviditelne znaky,
                        // ktere se ve vygenerovanem a zobrazenem HTML textu stejne nezobrazi
                        $customWhitespaces = array("\0x09", "\0x0A", "\0x0D", "\0x00", "\0x0B");
                        foreach ($customWhitespaces AS $customWhitespace) {
                                $s = trim($s, $customWhitespace);
                        }

                        $length = 0;
                        $tags = array(); // dosud neuzavřené značky
                        for ($i=0; $i < strlen($s) && $length < $maxLen; $i++) {
                                switch ($s[$i]) {
                                        case '<':
                                                // načtení značky
                                                $start = $i+1;
                                                while ($i < strlen($s) && $s[$i] != '>' && !ctype_space($s[$i])) {
                                                        $i++;
                                                }
                                                $tag = strtolower(substr($s, $start, $i - $start));
                                                // přeskočení případných atributů
                                                $in_quote = '';
                                                while ($i < strlen($s) && ($in_quote || $s[$i] != '>')) {
                                                        if (($s[$i] == '"' || $s[$i] == "'") && !$in_quote) {
                                                            $in_quote = $s[$i];

                                                        } elseif ($in_quote == $s[$i]) {
                                                            $in_quote = '';
                                                        }
                                                        $i++;
                                                }
                                                if ($s{$start} == '/') { // uzavírací značka
                                                        $tags = array_slice($tags, array_search(substr($tag, 1), $tags) + 1);

                                                } elseif ($s{$i-1} != '/' && !in_array($tag, $empty_tags)) { // otevírací značka
                                                        array_unshift($tags, $tag);
                                                }
                                                break;

                                        case '&':
                                                $length++;
                                                while ($i < strlen($s) && $s[$i] != ';') {
                                                        $i++;
                                                }
                                                break;

                                        default:
                                                $length++;

                                                /* V případě kódování UTF-8:
                                                while ($i+1 < strlen($s) && ord($s[$i+1]) > 127 && ord($s[$i+1]) < 192) {
                                                        $i++;
                                                }*/

                                                // je znak separatorem?
                                                if (in_array($s[$i], $separators)) {
                                                        // * a neni nasledujici (nebo predchozi) znak totozny,
                                                        // jako nyni nacteny separator?
                                                        if (($s[$i] != $s[$i+1]) && ($s[$i-1] != $s[$i])) {
                                                                $pos = $i; // pak ulozime pozici separatoru
                                                        } // nakonec tak ziskame pozici posledniho separatoru

                                                        // * tou druhou podminkou chceme zachytit pripady, kdy je v textu
                                                        // nekolik separatotu (napr. tecek) za sebou, pak to nebudeme povazovat za separator,
                                                        // ale jako vyznam k predchazejicimu slovu (vete).
                                                        // Navic pokud by takovy separator byl na konci oriznuteho textu, bude tak v podstate
                                                        // nahrazen volitelnym retezcem $append (defaultne trojtecka).
                                                }
                                }
                        }

                        // pokud nenalezneme hledany pocet znaku, obsah je nejspis slozen ciste z HTML tagu
                        // (napr. flashova videa a jine medialni objekty)
                        if ($length >= $maxLen) {

                                $s = substr($s, 0, $i);

                                // uzavreme vsechny tagy
                                $enclosingTags = "";
                                if ($tags) {
                                        $enclosingTags .= "</" . implode("></", $tags) . ">";
                                }

                                // Nyni potrebujeme probublat od konce pres vsechny tagy az na konec zobrazovaneho textu $s
                                // (tam pak budeme pridavat $append)
                                $s_beforeInnerEnclosingTags = $s;
                                $innerEnclosingTags = "";
                                while (substr(rtrim($s_beforeInnerEnclosingTags), - 1, 1) == ">") {
                                        $innerEnclosingTags = strrchr($s_beforeInnerEnclosingTags, "<");
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, strlen($s_beforeInnerEnclosingTags) - strlen($innerEnclosingTags));
                                }

                                // Pokud je nastaven $append na trojtecku,
                                // orezeme jeste samotne tecky na konci rezce (pokud nejake jsou)
                                if ($append == iconv('UTF-8', 'windows-1250//TRANSLIT', "\xE2\x80\xA6")) {
                                        $s_beforeInnerEnclosingTags = rtrim($s_beforeInnerEnclosingTags, '.');
                                }

                                // pokud byl nejaky separator nalezen
                                // a zaroven znak za zkracovanym textem neni separatorem
                                if (($pos > 0) && (!in_array(substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1), $separators))) {
                                        // tak orizneme text za poslednim nalezenym separatorem
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, $pos);
                                }
                                // nebo take pokud byl nejaky separator nalezen
                                // a zaroven 2 znaky za zkracovanym textem jsou 2 stejne separatory (viz. * o neco vyse - stejny pripad)
                                else if (($pos > 0) && (in_array(substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1), $separators)) && ((substr($INITAL_S, strlen($s_beforeInnerEnclosingTags), 1) == ((substr($INITAL_S, strlen($s_beforeInnerEnclosingTags) + 1, 1)))))) {
                                        // tak orizneme text za poslednim nalezenym separatorem a dva vyse zminene ignorujeme
                                        $s_beforeInnerEnclosingTags = substr($s_beforeInnerEnclosingTags, 0, $pos);
                                }

                                // Nyni vse spojime dohromady a pripojime $append
                                $s = $s_beforeInnerEnclosingTags . $append . $innerEnclosingTags . $enclosingTags;
                        }

                        // Vystupni retezec prekodujeme zpet z windows-1250 do UTF-8
                        $s = iconv('windows-1250', 'UTF-8//TRANSLIT', $s);
                }

                return $s;
        }
}