<?php

class FrontTexy extends Texy
{
    public function __construct()
    {
        parent::__construct();
        
        $this->allowedTags += array('figure' => TRUE);
        $this->allowedTags += array('figcaption' => TRUE);
        $this->allowedTags += array('hgroup' => TRUE);
        $this->allowedTags += array('colgroup' => TRUE);
        $this->allowedTags += array('col' => TRUE);
        $this->allowedTags += array('nav' => TRUE);
        $this->allowedTags += array('input' => Texy::ALL);
        
        $this->nontextParagraph = null;
        
        $this->linkModule->root = Environment::getVariable('baseUri') . "upload/pages/texyla";
        $this->imageModule->fileRoot = WWW_DIR . "/upload/pages/texyla";
        $this->imageModule->linkedRoot = Environment::getVariable("baseUri") . "upload/pages/texyla";
        $this->imageModule->root = Environment::getVariable("baseUri") . "upload/pages/texyla";
        $this->addHandler('phrase', array('FrontTexy', 'phraseHandler'));
//        $this->addHandler('linkURL', array('FrontTexy', 'linkHandler'));
        $this->headingModule->top = 2;
        $this->headingModule->balancing = TexyHeadingModule::FIXED;
        $this->headingModule->moreMeansHigher = FALSE;

    }

    public static function phraseHandler($invocation, $phrase, $content, $modifier, $link)
    {
        // is there link?
        if (!$link) return $invocation->proceed();

        $presenter = Environment::getApplication()->getPresenter();
        
        if (substr($link->URL, 0, 8) === 'article:') {
            Debug::barDump(array('link_url' => $link->URL));
            $pagesModel = new Cms_PageModel();
            $page = $pagesModel->find(substr($link->URL, 8))->fetch();

            $matches = String::match($link->URL, '/#(\S*)/');
            
            $page->url = $pagesModel->constructUrl($page);
            
            // modifiy link
            if($page) {
                if(isset($matches[1]))
                    $link->URL = $presenter->link(":CMS:Default:view#{$matches[1]}", $page->url);
                else
                    $link->URL = $presenter->link(':CMS:Default:view', $page->url);
            }

        }/* else {
            // modifiy link
            $url = $link->URL;
            if(!preg_match('/http/i', $link->URL)) {
                $link->modifier->attrs['target'] = '_blank';
                //$link->modifier->attrs['onclick'] = 'pageTracker._trackPageview("/outgoing/' . $url . '")';
            }
        }/**/

        return $invocation->proceed();
    }


}
