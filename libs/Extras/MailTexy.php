<?php

class MailTexy extends Texy
{
    public function __construct()
    {
        parent::__construct();
        
        $this->imageModule->fileRoot = WWW_DIR . "/upload/pages";
        $this->imageModule->root = Environment::getVariable("baseUri") . "upload/pages";
        $this->headingModule->top = 2;
        $this->headingModule->balancing = TexyHeadingModule::FIXED;
        $this->headingModule->moreMeansHigher = FALSE;
        $this->linkModule->shorten = FALSE;
    }

}
