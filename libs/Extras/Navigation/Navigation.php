<?php

/**
 * Navigation
 *
 * @author Jan Marek
 * @license MIT
 */
class Navigation extends Control {

    /** @var NavigationNode */
    private $homepage;
    
    /** @var NavigationNode */
    private $current;

    /** @var bool */
    private $useHomepage = false;


    /**
     * Set node as current
     * @param NavigationNode $node
     */
    public function setCurrent(NavigationNode $node) {
        /*
        if (isset($this->current)) {
            $this->current->isCurrent = false;
        }
        */
        $node->isCurrent = true;
        $node->realCurrent = true;
        $this->current = $node;
        $parent = $this->current->parent;
        while($parent instanceof NavigationNode) {
            if($parent != $this->getComponent('homepage')) {
                $parent->isCurrent = true;
            }
            $parent->realCurrent = false;

            $parent = $parent->parent;
        }
    }


    /**
     * Add navigation node as a child
     * @param string $label
     * @param string $url
     * @return NavigationNode
     */
    public function add($label, $url, $menu = 1) {
        return $this->getComponent("homepage")->add($label, $url, $menu);
    }


    /**
     * Setup homepage
     * @param string $label
     * @param string $url
     * @return Navigation
     */
    public function setupHomepage($label, $url, $useHomepage = true) {
        $homepage = $this->getComponent("homepage");
        $homepage->label = $label;
        $homepage->url = $url;
        $this->useHomepage = $useHomepage;
        return $homepage;
    }


    /**
     * Homepage factory
     * @param string $name
     */
    protected function createComponentHomepage($name) {
        new NavigationNode($this, $name);
    }


    /**
     * Render menu
     * @param bool $renderChildren
     * @param NavigationNode $base
     * @param bool $renderHomepage
     */
    public function renderMenu($renderChildren = true, $base = null, $renderHomepage = true) {
        $template = $this->createTemplate()
            ->setFile(dirname(__FILE__) . "/menu.phtml");
        $template->homepage = $base ? $base : $this->getComponent("homepage");
        $template->useHomepage = $this->useHomepage && $renderHomepage;
        $template->renderChildren = $renderChildren;
        $template->children = $this->getComponent("homepage")->getComponents();
        $template->name = $this->name;
        $template->url = Environment::getApplication()->getPresenter()->getParam('url');
        $template->render();
    }


    /**
     * Render full menu
     */
    public function render() {
        $this->renderMenu();
    }


    /**
     * Render main menu
     */
    public function renderMainMenu() {
        $this->renderMenu(false);
    }

    
    /**
     * Render breadcrumbs
     */
    public function renderBreadcrumbs() {
        if (empty($this->current)) return;

        $items = array();
        $node = $this->current;

        $same = 0;
        while ($node instanceof NavigationNode) {
            $parent = $node->getParent();
            if (!$this->useHomepage && !($parent instanceof NavigationNode)) break;

            if(!$same)
                array_unshift($items, $node);

            if(($parent instanceof NavigationNode) && ($node->url === $parent->url))
                $same = 1;
            else
                $same = 0;
                
            $node = $parent;
        }

        $template = $this->createTemplate()
            ->setFile(dirname(__FILE__) . "/breadcrumbs.phtml");
        $template->items = $items;
        $template->name = $this->name;
        $template->url = Environment::getApplication()->getPresenter()->getParam('url');
        $template->render();
    }

}