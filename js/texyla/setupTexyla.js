
$.texyla.setDefaults({
    previewPath: "{{$texylaPreviewPath}}",
    toolbar: [ 'h2', 'h3', 'h4',  null, 'bold', 'italic', null, 'ul', 'ol', 'blockquote', 'table', 'symbol', null, 'link', 'img', null, 'files' ],
    baseDir: "{{$baseUri}}",
    iconPath: "{{$baseUri}}js/texyla/icons/%var%.png",
     bottomRightPreviewToolbar: "",

    // soubory
    filesPath: "{{$texylaFilesPath}}",
    filesThumbPath: "%var%",
    filesIconPath: "{{$baseUri}}js/texyla/plugins/files/icons/%var%.png",
    filesUploadPath: "{{$texylaFilesUploadPath}}"
});

// run texyla
$(function () {
    $("textarea").texyla();
});
