<?php

class Admin_ProductPresenter extends Admin_SecuredPresenter
{
    const thumb_w = 225;
    const thumb_h = 203;
    const small_w = 100;
    const small_h = 90;
    
    public function actionDefault()
    {
        $this->redirect('Catalog:default');
    }
    
    public function actionAdd($pageId = 0)
    {
        $this->setView('edit');
        $this->template->form = $this['editForm'];
        $this['editForm']['categories']->setDefaults($this->pagesModel->find($pageId)->fetchPairs('id', 'title'));
    }
    
    public function actionEdit($id)
    {
        $form = $this['editForm'];
        $form['product']->setDefaults($this->productsModel->find($id)->fetch());
        $form['categories']->setDefaults($this->productsModel->findRelations($id)->fetchPairs('id', 'productId'));
        $form['parameters']->setDefaults($this->productsModel->findParameters($id)->fetchPairs('id', 'value'));
        
        $this->template->form = $form;
        $this->template->product = $this->productsModel->find($id)->fetch();
    }
    
    protected function createComponentEditForm($name)
    {
        $form = new AppForm($this, $name);
        
        $sub = $form->addContainer('product');
        $sub->addText('title', 'Název:', 70)
            ->addRule(Form::FILLED, 'Vyplňte název produktu.');
        $sub->addText('url', 'URL:', 70);
        $sub->addTextArea('perex', 'Popis produktu:', 70, 10);
        $sub->addSelect('active', 'Aktivni', array(0 => 'Ne', 1 => 'Ano'))
            ->setDefaultValue(1);
        $sub->addText('price', 'Cena:');
        $sub->addText('position', 'Pozice:');
        $sub->addFile('image', 'Obrázek:');
        $sub->addText('link', 'Odkaz:', 70);
        
        $sub = $form->addContainer('categories');

        $category = $this->pagesModel->admin_findAll()->where('c.template = %s', 'kategorie')->orderBy('lft')->fetchAll();
        $categories = array();
        foreach($category as $item) {
            $categories = array_merge($categories, $this->pagesModel->admin_findAll()->where('lft >= %i', $item->lft)->and('rgt <= %i', $item->rgt)->orderBy('lft')->fetchAll());
        }
        foreach($categories as $kat) {
            $sub->addCheckbox($kat->id, $kat->title);
        }
        
        $sub = $form->addContainer('parameters');
        $parameters = $this->parametersModel->findAll()->fetchAll();
        foreach($parameters as $parameter) {
            $sub->addText($parameter->id, $parameter->title);
        }
        
        $form->addSubmit('save', 'Uložit')
            ->onClick[] = callback($this, 'save');
        
        return $form;
    }

    public function save($button)
    {
        $id = $this->getParam('id');
        
        $form = $button->form;
        
        $product = $form['product']->values;
        unset($product['image']);
        if($product['url'])
            $product['url'] = String::webalize($product['url']);
        else
            $product['url'] = String::webalize($product['title']);

        if(!$product['position'])
            $product['position'] = $this->productsModel->getMaxPozici() + 1;
        
        if($id) {
            $this->productsModel->update($id, $product);
            $this->flashMessage('Produkt byl upraven.');
        }
        else {
            $this->productsModel->insert($product);
            $id = dibi::insertId();
            $this->flashMessage('Produkt byl vytvořen.');
        }
        
        if($form['product']['image']->value)
            $this->uploadImage($id, $form);
        
        $categories = $form['categories']->values;
        $this->productsModel->deleteRelations($id);
        foreach($categories as $key => $value) {
            if($value)
                $this->productsModel->insertRelation($key, $id);
        }
        
        $parameters = $form['parameters']->values;
        $this->productsModel->deleteParameters($id);
        foreach($parameters as $key => $value) {
            if($value)
                $this->productsModel->insertParameter($id, $key, $value);
        }

        $this->redirect('default');
    }

    public function uploadImage($id, AppForm $form)
    {
        
        $uploaded = $form['product']['image']->value;
        
        if($uploaded->isOk()){
            $image = $uploaded->toImage();
 
            $thumb = clone $image;
            $small = clone $image;

            $thumb->resize(self::thumb_w,NULL);
            $small->resize(self::small_w,NULL);

            $smaller = Image::fromBlank(self::small_w, self::small_h, realpath('.') . 'images/blank_small.jpg');
            $smaller->place($small, '50%', '50%');
                            
            $thumber = Image::fromBlank(self::thumb_w, self::thumb_h, realpath('.') . 'images/blank_thumb.jpg');
            $thumber->place($thumb, '50%', '50%');
                            
            if(!is_dir(WWW_DIR . "/upload")) {
                mkdir(WWW_DIR . "/upload", 0777);
            }

            if(!is_dir(WWW_DIR . "/upload/products")) {
                mkdir(WWW_DIR . "/upload/products", 0777);
            }
            
            $image->save(WWW_DIR.'/upload/products/orig_'.$id.'-'.String::webalize($uploaded->getName(), '.'));
            chmod(WWW_DIR . "/upload/products/orig_".$id.'-'.String::webalize($uploaded->getName(), '.'), 0666);

            $thumber->save(WWW_DIR . '/upload/products/thumb_' . $id . '-' . String::webalize($uploaded->getName(), '.'));
            chmod(WWW_DIR . "/upload/products/thumb_" . $id . '-' . String::webalize($uploaded->getName(), '.'), 0666);

            $smaller->save(WWW_DIR . '/upload/products/small_' . $id . '-' . String::webalize($uploaded->getName(), '.'));
            chmod(WWW_DIR . "/upload/products/small_" . $id . '-' . String::webalize($uploaded->getName(), '.'), 0666);

            $this->productsModel->update($id, array('image' => $id . '-' . String::webalize($uploaded->getName(), '.')));
        }
    }

    
    public function actionDelete($id)
    {
        $this->setView('delete');

        $this['deleteForm']->onSubmit[] = array($this,'delete');

        $res = $this->productsModel->find($id)->fetch();
        $this->template->result = $res;
        $this->template->item = $res->title;
        $this->template->header = "Vymazat produkt";
    }

    protected function createComponentDeleteForm($name)
    {
        $form = new AppForm($this, $name);
        $form->addSubmit('yes', 'Ano');
        $form->addSubmit('no', 'Ne');
    }
    
    public function delete($form)
    {
        $id = $this->getParam('id');
        
        $this->productsModel->delete($id);
        
        $this->redirect('default');
    }
    

}