<?php

class Admin_PagePresenter extends Admin_SecuredPresenter
{
    const large_w = 800;
    const thumb_w = 200;
    const profil_h = 176;
    const small_w = 100;
    
    public $typy = array('' => 'Zamčeno', 'open' => 'Otevřeno');
    
    public function startup()
    {
        parent::startup();
        
        $this->template->pagesModel = $this->pagesModel;
        $this->template->pagesList = $this->pagesModel->findAll()->orderBy('lft', 'ASC')->fetchAll();
    }
    
    public function renderDefault()
    {
        $this->redirect('Content:default');
    }
    
    public function renderView($id)
    {
    }
    
    public function actionAdd($template = 'clanek', $parentId = 0)
    {
        $this->setView('edit');
        
        if($parentId)
            $this['editForm']['page']['parentId']->setDefaultValue($parentId);
            
        if($template)
            $this['editForm']['page']['template']->setDefaultValue($template);
            
        $this->template->form = $this['editForm'];
    }
    
    public function actionClone($pageId) {
        $this->setView('edit');
        $this->actionEdit($pageId);
    }
    
    public function actionEdit($id)
    {
        $page = $this->pagesModel->form_find($id)->fetch();
        $page->published = date('j.n.Y G:i', $page->published);

        $parent = $this->pagesModel->findParent($id)->fetch();
        if($parent)
            $page->parentId = $parent->id;
        
        $this['editForm']['page']->setDefaults($page);
        /*if($page->depth > 2)
            unset($this['editForm']['page']['menu']);/**/
        
        $def_params = array();
        $parameters = $this->pagesModel->findParameters($id)->fetchAll();
        Debug::barDump($parameters);
        foreach($parameters as $param) {
            switch($param->type) {
                case 'datepicker':                      
                    $def_params[$param->id] = date("j.n.Y G:i", $param->value);
                    break;
                default:
                    $def_params[$param->id] = $param->value;
                    break;
            }
        }
        $this['editForm']['parameters']->setDefaults($def_params);
        
        $this->template->form = $this['editForm'];
    }
    
    protected function createComponentEditForm($name)
    {
        $template = $this->getParam('template');
        $id = $this->getParam('id');
        
        $templates = $this->pagesModel->findTemplates()->orderBy('title')->fetchPairs('template', 'title');
        
        $pagesArr = array(0 => '=== Bez nadřazeného článku ===');
        if($id) {
            $page = $this->pagesModel->find($id)->fetch();
            $result = $this->pagesModel->findChildren($page, FALSE, TRUE)->fetchPairs('id', 'id');
            $ids = array_merge(array($page->id), array_values($result));
            Debug::barDump($ids);
            $pages = $this->pagesModel->form_findAll()->where('id NOT IN %in', $ids)->orderBy('lft', 'ASC')->fetchAll();
        } else {
            $pages = $this->pagesModel->form_findAll()->orderBy('lft', 'ASC')->fetchAll();
        }
        
        foreach($pages as $p) {
            $pagesArr[$p->id] = str_repeat('-', ($p->depth - 1) * 2) . " {$p->title}";
        }
        
        $form = new AppForm($this, $name);
        
        $sub = $form->addContainer('page');
        
        $sub->addSelect('parentId', 'Nadřazená stránka', $pagesArr);
        $sub->addText('title', 'Název:', 80)
            ->addRule(Form::FILLED, 'Vyplňte název článku.');
        $sub->addText('url', 'URL:', 80);
        $sub->addTextArea('perex', 'Perex:', 70);
        $sub->addTextArea('text', 'Text:', 70, 20);
        $sub->addSelect('active', 'Aktivni', array(0 => 'Ne', 1 => 'Ano'))
            ->setDefaultValue(1);
        $sub->addHidden('created')
            ->setValue(time());
        $sub->addDateTimePicker('published', 'Čas spuštění')
            ->addRule(Form::FILLED, 'Zadejte čas spuštění')
            ->setDefaultValue(date('j.n.Y G:i', time()));
        $sub->addHidden('author')
            ->setValue($this->user->identity->data['id']);
        $sub->addFile('image', 'Obrázek:');
        $sub->addSelect('template', 'Šablona', $templates)
            ->getControlPrototype()->class('template');
        if($template)
            $sub['template']->setDefaultValue($template);

        $sub->addSelect('menu', 'Položka menu?', array(0 => 'Ne', 1 => 'Ano'));
        $sub->addSelect('submenu', 'Má podpoložky', array(0 => 'Ne', 1 => 'Ano'));
        $sub->addSelect('sitemap', 'Položka sitemap?', array(0 => 'Ne', 1 => 'Ano'))
            ->setDefaultValue(1);
        $sub->addText('comment', 'Komentář:');
        $sub->addCheckbox('seo', 'SEO parametry:')
            ->addCondition(Form::EQUAL, TRUE) // conditional rule: if is checkbox checked...
            ->toggle('seo_title')
            ->toggle('seo_keywords')
            ->toggle('seo_description');
        $sub->addText('seo_title', 'Title:', 80);
        $sub->addText('seo_keywords', 'Keywords:', 80);
        $sub->addText('seo_description', 'Description:', 80);
        
        $sub = $form->addContainer('parameters');
        $templateId = 0;
        if($id) {
            $template_res = $this->templatesModel->findAll()->where('template = %s', $this->pagesModel->find($id)->fetch()->template)->fetch();
        } elseif ($template) {
            $template_res = $this->templatesModel->findAll()->where('template = %s', $template)->fetch();
        }

        if(isset($template_res)) {
            if(!$id) {
                $form['page']['perex']->setDefaultValue($template_res->perex);
                $form['page']['text']->setDefaultValue($template_res->text);
                $form['page']['sitemap']->setDefaultValue($template_res->sitemap);
            }
            
            $parameters = $this->templatesModel->findParameters($template_res->id)->fetchAll();
            foreach($parameters as $parameter) {
                switch($parameter->type) {
                    case 'textarea':
                        $sub->addTextArea($parameter->id, $parameter->title, 70, 10);
                        break;
                    case 'gallery':
                        $galleries = array('' => 'Bez fotogalerie');
                        $galleries += $this->galleriesModel->findAll()->fetchPairs('id', 'title');
                        $sub->addSelect($parameter->id, $parameter->title, $galleries);
                        break;
                    case 'typ':
                        $sub->addSelect($parameter->id, $parameter->title, $this->typy);
                        break;
                    case 'file':
                        $sub->addFile($parameter->id, $parameter->title);
                        break;
                    case 'datepicker':
                        $sub->addDatetimePicker($parameter->id, $parameter->title);
                        break;
                    default:
                        $sub->addText($parameter->id, $parameter->title, 80);
                }
            }
        }
        
        $form->addSubmit('stay', 'Uložit')
            ->onClick[] = callback($this, 'save');
        $form->addSubmit('save', 'Uložit a přejít zpět')
            ->onClick[] = callback($this, 'save');
        $form->addSubmit('clone', 'Klonovat')
            ->onClick[] = callback($this, 'save');
    }
    
    public function save($button)
    {
        $id = $this->getParam('id');
        
        $form = $button->form;

        $values = $form['page']->values;
        unset($values['image']);
        unset($values['seo']);
        
        if(isset($form['terminSubmit']) && $form['terminSubmit']->isSubmittedBy())
            $values['title'] = date("j.n.Y", $values['published']);
        
        if($values['url']) {
            if(!strstr($values['url'], 'http://'))
                $values['url'] = String::webalize($values['url'], '_');
        } else {
            $values['url'] = String::webalize($values['title'], '_');
        }

        if(!$values['seo_title'])
            $values['seo_title'] = $values['title'];
            
        try {        
            if($id) {
                $this->pagesModel->update($id, $values);
                $this->flashMessage('Článek byl úspěšně změněn.', 'done');
            } else {
                $id = $this->pagesModel->insert($values);
                $this->flashMessage('Článek byl vytvořen.', 'done');
            }
        } catch (DuplicateKeyException $e) {
            $form['page']['url']->addError('Článek se stejnou URL adresou již existuje, změňte ji.');
            return;
        }

        $this->pagesModel->deleteParameters($id);
        foreach($form['parameters']->controls as $key => $control) {
            if($control instanceOf FileUpload) {
                if($control->value)
                    $this->uploadParamImage($id, $key, $control->value);
            } else {
                if($control->value != '')
                    $this->pagesModel->insertParameter(array('pageId' => $id, 'parameterId' => $key, 'value' => $control->value));
            }
        }
        
        if(isset($form['page']['image']) && $form['page']['image']->value)
            $this->uploadImage($id, $form);
        
        if(isset($form['stay']) && $form['stay']->isSubmittedBy())
            $this->redirect('edit', $id);
        elseif(isset($form['clone']) && $form['clone']->isSubmittedBy())
            $this->redirect('clone', array('pageId' => $id));
        elseif(isset($form['terminSubmit']) && $form['terminSubmit']->isSubmittedBy())
            $this->redirect('Content:terminy');
        else
            $this->redirect('Content:default', $this->pagesModel->findParent($id)->fetch()->id);
    }

    public function uploadImage($id, AppForm $form)
    {
        
        $uploaded = $form['page']['image']->value;
        
        if($uploaded->isOk()){
            $image = $uploaded->toImage();
 
            $thumb = clone $image;
            $small = clone $image;
            $profile = clone $image;
            $large = clone $image;

            $thumb->resize(self::thumb_w,NULL);
            $small->resize(self::small_w,NULL);
            $profile->resize(NULL, self::profil_h);
            $large->resize(self::large_w, NULL);

           if(!is_dir(WWW_DIR . "/upload")) {
                mkdir(WWW_DIR . "/upload", 0777);
            }

            if(!is_dir(WWW_DIR . "/upload/pages")) {
                mkdir(WWW_DIR . "/upload/pages", 0777);
            }
            
            $image->save(WWW_DIR."/upload/pages/{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/pages/{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $thumb->save(WWW_DIR."/upload/pages/thumb_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/pages/thumb_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $small->save(WWW_DIR."/upload/pages/small_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/pages/small_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $profile->save(WWW_DIR."/upload/pages/profile_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/pages/profile_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $large->save(WWW_DIR."/upload/pages/large_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/pages/large_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $this->pagesModel->update($id, array('image' => $id . '-' . String::webalize($uploaded->getName(), '.')));
        }
    }

    public function uploadParamImage($id, $key, $uploaded)
    {
        
        if($uploaded->isOk()){
            $image = $uploaded->toImage();
 
            $profile = clone $image;
            $large = clone $image;

            $profile->resize(NULL, self::profil_h);
            $large->resize(self::large_w, NULL);

           if(!is_dir(WWW_DIR . "/upload")) {
                mkdir(WWW_DIR . "/upload", 0777);
            }

            if(!is_dir(WWW_DIR . "/upload/params")) {
                mkdir(WWW_DIR . "/upload/params", 0777);
            }
            
            $image->save(WWW_DIR."/upload/params/{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/params/{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $profile->save(WWW_DIR."/upload/params/profile_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/params/profile_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $large->save(WWW_DIR."/upload/params/large_{$id}-".String::webalize($uploaded->getName(), '.'), 100);
            chmod(WWW_DIR . "/upload/params/large_{$id}-".String::webalize($uploaded->getName(), '.'), 0666);

            $this->pagesModel->insertParameter(array('pageId' => $id, 'parameterId' => $key, 'value' => $id . '-' . String::webalize($uploaded->getName(), '.')));
        }
    }

    
    public function handleLoadParameters($template)
    {
        $id = $this->templatesModel->findAll()->where('template = %s', $template)->fetch()->id;
        
        $parameters = $this->templatesModel->findParameters($id)->fetchAll();
        unset($this['editForm']['parameters']);
        $sub = $this['editForm']->addContainer('parameters');
        foreach($parameters as $parameter) {
            switch($parameter->type) {
                case 'textarea':
                    $sub->addTextArea($parameter->id, $parameter->title, 70);
                    break;
                default:
                    $sub->addText($parameter->id, $parameter->title, 80);
            }
        }
        
        unset($parameters);
        $parameters = $this->pagesModel->findParameters($this->getParam('id'))->fetchPairs('id', 'value');
        $this['editForm']['parameters']->setDefaults($parameters);

        $this->template->form = $this['editForm'];
        
        if ($this->isAjax()) {
            $this->invalidateControl('parameters');
        }
    }

    
    /**
    * Mazání článků
    */
    
    public function actionDelete($id)
    {
        $this->setView('delete');

        $this['deleteForm']->onSubmit[] = array($this,'delete');

        $res = $this->pagesModel->find($id)->fetch();
        $this->template->result = $res;
        $this->template->item = $res->title;
        $this->template->header = "Vymazat článek";
    }

    protected function createComponentDeleteForm($name)
    {
        $form = new AppForm($this, $name);
        $form->addSubmit('yes', 'Ano');
        $form->addSubmit('no', 'Ne');
    }
    
    public function delete($form)
    {
        $id = $this->getParam('id');
        
        $page = $this->pagesModel->find($id)->fetch();
        
        if($form['yes']->isSubmittedBy() && !$page->system) {
            $this->pagesModel->delete($id);
            $this->flashMessage('Stránka byla smazána', 'done');
        }
            
        
        $this->redirect('default');
    }
    
}
