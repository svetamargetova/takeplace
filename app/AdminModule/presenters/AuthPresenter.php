<?php

final class Admin_AuthPresenter extends Admin_BasePresenter
{

    /** @persistent */
    public $backlink = '';

    public function actionLogin()
    {
        $this->template->form = $this['loginForm'];
    }
    
    protected function createComponentLoginForm($name)
    {
        $form = new AppForm($this, $name);
        
        $form->addText('username', 'Email:', 45)
             ->addRule(Form::FILLED, 'Prosím zadejte registrační email.')
             ->addRule(Form::EMAIL, 'Prosím zadejte validní email.');
        $form['username']->getControlPrototype()->class('input-text');

        $form->addPassword('password', 'Heslo:', 45)
             ->addRule(Form::FILLED, 'Prosím zadejte heslo.');
        $form['password']->getControlPrototype()->class('input-text');

        $form->addProtection('Prosím odešlete přihlašovací údaje znova (vypršela platnost tzv. bezpečnostního tokenu).');
        $form->addSubmit('send', 'Přihlásit')
            ->getControlPrototype()->class('input-submit');

        $form->onSubmit[] = array($this, 'loginFormSubmitted');
    }

    public function loginFormSubmitted($form)
    {
        try {
            $user = Environment::getUser();
            $user->login($form['username']->value, $form['password']->value);
            $this->getApplication()->restoreRequest($this->backlink);
            $this->redirect('Default:default');
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

}
