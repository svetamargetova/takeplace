<?php

class Admin_CatalogPresenter extends Admin_SecuredPresenter
{
    
    public function actionDefault()
    {
        
        $category = $this->pagesModel->admin_findAll()->where('c.template = %s', 'kategorie')->orderBy('lft')->fetchAll();
        $categories = array();
        foreach($category as $item) {
            $categories = array_merge($categories, $this->pagesModel->admin_findAll()->where('lft >= %i', $item->lft)->and('rgt <= %i', $item->rgt)->orderBy('lft')->fetchAll());
        }
        $this->template->categories = $categories;
        
        $this->template->nonRelated = $this->productsModel->findNonRelated()->fetchAll();
        
        $this->template->productsModel = $this->productsModel;
    }
    
}