<?php

final class Admin_UserPresenter extends Admin_SecuredPresenter
{

    const width = 150;
    
    public function startup()
    {
        $user = Environment::getUser();
        if(!$user->isInRole('admin')) {
            $this->flashMessage('Nemáte právo přistupovat do správy uživatelů.', 'warning');
            $this->redirect('Default:default');
        }
            
        parent::startup();
    }
    
    public function actionDefault()
    {
        $this->template->users = $this->usersModel->findAll()->orderBy('sureName', 'ASC')->fetchAll();
    }
    
    public function actionView($id)
    {
        $this->template->user = $this->usersModel->find($id)->fetch();
    }
    
    public function actionAdd()
    {
        $this->setView('edit');
        
        $this->template->jmenoUzivatele = 'Přidat uživatele';
        
        $this->template->form = $this['uzivatel'];
        $this->template->form['ulozit']->caption = 'Založit uživatele';
    }
    
    public function actionDetail()
    {
        $this->setView('edit');
        $user = Environment::getUser();
        $this->forward('User:edit', $user->identity->data['id']);
    }
    
    public function actionEdit($id)
    {
        $uzivatel = $this->usersModel->find($id)->fetch();
        
        $this->template->jmenoUzivatele = $uzivatel->name;
        
        $form = $this['uzivatel'];
        $form['uzivatel']->setDefaults($uzivatel);
        
        $this->template->form = $form;
    }
    
    protected function createComponentUzivatel($name)
    {
        $id = $this->getParam('id');
        
        $form = new AppForm($this, $name);
        
        $sub = $form->addContainer('uzivatel');
        $sub->addText('firstName', 'Jméno: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte jméno uživatele.');
        $sub->addText('sureName', 'Příjmení: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte příjmení uživatele.');
        $sub->addText('email', 'Email: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte email.')
            ->addRule(Form::EMAIL, 'Zadejte e-mail ve validním tvaru.');

        if($id) {
            $sub->addCheckbox('changePass', 'Změnit heslo:')
                ->addCondition(Form::EQUAL, TRUE) // conditional rule: if is checkbox checked...
                ->toggle('password')
                ->toggle('password2');
            $sub->addPassword('password', 'Heslo: *', 20)
                ->addConditionOn($sub['changePass'], Form::FILLED)
                ->addRule(Form::FILLED, 'Zadejte heslo')
                ->addRule(Form::MIN_LENGTH, 'Heslo je příliš krátké, musí být aspoň %d znaků dlouhé', 5);

            $sub->addPassword('password2', 'Zopakovat heslo: *', 20)
                ->addConditionOn($sub['changePass'], Form::FILLED)
                ->addConditionOn($sub['password'], Form::VALID)
                    ->addRule(Form::FILLED, 'Zopakujte heslo')
                    ->addRule(Form::EQUAL, 'Hesla si neodpovídají', $sub['password']);
        } else {
            $sub->addPassword('password', 'Heslo: *', 20)
                ->addRule(Form::FILLED, 'Zadejte heslo')
                ->addRule(Form::MIN_LENGTH, 'Heslo je příliš krátké, musí být aspoň %d znaků dlouhé', 5);

            $sub->addPassword('password2', 'Zopakovat heslo: *', 20)
                ->addConditionOn($sub['password'], Form::VALID)
                    ->addRule(Form::FILLED, 'Zopakujte heslo')
                    ->addRule(Form::EQUAL, 'Hesla si neodpovídají', $sub['password']);
        }

        $sub->addSelect('role', 'Role v systému:', array('' => 'Bez uživatelské role', 'admin' => 'Admin', 'interni' => 'Interní člen'));
        $sub->addCheckbox('blocked', 'Zablokovat uživatele:');
        
        $form->addSubmit('ulozit', 'Uložit změny')
            ->onClick[] = array($this, 'upravUzivatele');
        
        return $form;
    }
    
    public function upravUzivatele($button)
    {
        $form = $button->form;

        $id = $this->getParam('id');
        
        $uzivatel = $form['uzivatel']->values;
        if($id && !$uzivatel['changePass']) {
            unset($uzivatel['password']);
        } else {
            $config = Environment::getConfig('security');
            $uzivatel['salt'] = $this->Random_Password();
            $uzivatel['password'] = hash_hmac('sha256', $uzivatel['password'] . $uzivatel['salt'] , $config->hmacKey);
            if($id)
                $this->flashMessage('Heslo uživatele bylo změněno', 'done');
        }
        unset($uzivatel['changePass']);
        unset($uzivatel['password2']);

        try {        
            if($id) {
                $this->usersModel->update($id, $uzivatel);
                $this->flashMessage('Uživatel byl upraven', 'done');
            } else {
                $this->usersModel->insert($uzivatel);
                $this->flashMessage('Uživatel byl vytvořen', 'done');
                $id = dibi::insertId();
            }
        } catch (DuplicateKeyException $e) {
            $form['email']->addError('Zvolte jiný email, zadaný již existuje');
            return;
        }

        $this->redirect('default');
    }
    
}
