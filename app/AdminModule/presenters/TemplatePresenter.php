<?php
  
class Admin_TemplatePresenter extends Admin_SecuredPresenter
{

    public function renderDefault()
    {
        $this->redirect('Content:default');
    }
    
    public function renderEdit($id)
    {
        $template = $this->templatesModel->find($id)->fetch();
        $parameters = $this->templatesModel->findParameters($id)->fetchPairs('id', 'title');
        
        $this['editForm']['template']->setDefaults($template);
        $this['editForm']['parameters']->setDefaults($parameters);
        
        $this->template->form = $this['editForm'];
    }
    
    protected function createComponentEditForm($name)
    {
        $form = new AppForm($this, $name);
        
        $sub = $form->addContainer('template');
        $sub->addText('title', 'Název šablony: *')
            ->addRule(Form::FILLED, 'Musíte zadat název šablony.');
        $sub->addText('template', 'Název souboru: *')
            ->addRule(Form::FILLED, 'Musíte zadat název souboru.');
        $sub->addText('subtemplate', 'Název souboru následovníka:');
        $sub->addTextArea('perex', 'Perex šablony:', 70);
        $sub->addTextArea('text', 'Text šablony:', 70);
        $sub->addSelect('sitemap', 'Sitemap:', array(0 => 'Ne', 1 => 'Ano'));
            
        $parameters = $this->parametersModel->findAll()->fetchAll();
        $sub = $form->addContainer('parameters');
        foreach($parameters as $parameter) {
            $sub->addCheckbox($parameter->id, $parameter->title);
        }
        
            
        $form->addSubmit('save', 'Vložit')
            ->onClick[] = callback($this, 'save');
    }

    public function save($button)
    {
        $id = $this->getParam('id');

        $form = $button->form;
        
        $values = $form['template']->values;
        
        try {        
            if($id) {
                $this->templatesModel->update($id, $values);
                $this->flashMessage('Šablona byla změněna.', 'done');
            } else {
                $this->templatesModel->insert($values);
                $this->flashMessage('Šablona byla založena.', 'done');
                $id = dibi::insertId();
            }
        } catch (DuplicateKeyException $e) {
            $form['template']['template']->addError('Šablona s tímto souborem již existuje.');
            return;
        }
        
        if($form['parameters']) {
            $parameters = $form['parameters']->values;
            $this->templatesModel->deleteParameters($id);
            foreach($parameters as $key => $value) {
                if($value)
                    $this->templatesModel->insertParameter(array('templateId' => $id, 'parameterId' => $key));
            }
        }

        $this->redirect('default');
    }
    
    
    
    public function actionDelete($id)
    {
        $this->setView('delete');

        $this['deleteForm']->onSubmit[] = array($this,'delete');

        $res = $this->templatesModel->find($id)->fetch();
        $this->template->result = $res;
        $this->template->item = $res->title;
        $this->template->header = "Vymazat šablonu";
    }

    protected function createComponentDeleteForm($name)
    {
        $form = new AppForm($this, $name);
        $form->addSubmit('yes', 'Ano');
        $form->addSubmit('no', 'Ne');
    }
    
    public function delete($form)
    {
        $id = $this->getParam('id');
        
        $this->templatesModel->delete($id);
        
        $this->redirect('default');
    }

}
