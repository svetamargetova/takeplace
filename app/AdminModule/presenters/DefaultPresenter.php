<?php

final class Admin_DefaultPresenter extends Admin_SecuredPresenter
{

    public function actionLogout()
    {
        Environment::getUser()->logout();
        $this->flashMessage('Byli jste úspěšně odhlášeni z administrace.');
        $this->redirect('Auth:login');
    }
    
    public function actionDefault()
    {
        $this->redirect('Content:default');
    }
}
