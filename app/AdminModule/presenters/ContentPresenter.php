<?php
  
class Admin_ContentPresenter extends Admin_SecuredPresenter
{
    /** @persist */
    public $id;
    
    public function actionDefault($id = 0)
    {
        if($id && $this->getParam('direction')) {
            $page = $this->pagesModel->admin_findParent($id)->fetch();
        } else {
            $page = $this->pagesModel->admin_find($id)->fetch();
        }

        if($id) {
            $pages = $this->pagesModel->admin_findAll()->where('lft > %i', $page->lft)->and('rgt < %i', $page->rgt)->where('depth = %i', $page->depth + 1)->orderBy('lft', 'ASC')->fetchAll();
            $this->template->page = $page;

            $parents = $this->pagesModel->findParent($id, FALSE, 'ASC')->fetchAll();
            $this->template->parents = $parents;
            
        } else {
            $pages = $this->pagesModel->admin_findAll()->where('depth = %i', 1)->orderBy('lft', 'ASC')->fetchAll();
        }
        
        foreach($pages as $p) {
            $p->pages = count($this->pagesModel->admin_findAll()->where('lft > %i', $p->lft)->and('rgt < %i', $p->rgt)->fetchAll());
        }

        $this->template->pages = $pages;
    }
    
    public function actionTerminy($id = 17)
    {
        $pageId = 17;
        $page = $this->pagesModel->admin_find($pageId)->fetch();
        Debug::barDump($page);
        
        $this->template->pages = $this->pagesModel->findAll()->where('lft > %i', $page->lft)->and('rgt < %i', $page->rgt)->where('depth = %i', $page->depth + 1)->orderBy('published', 'DESC')->fetchAll();
        
    }
    
    public function handleMove($id, $direction)
    {
        if(!$id)
            $this->redirect('default');
        
        $parent = $this->pagesModel->findParent($id)->fetch();
        if($direction == 'u')
            $this->pagesModel->shiftLeft($id);
        else
            $this->pagesModel->shiftRight($id);
        
        $this->redirect('this', $parent->id);
    }
    
    
    public function actionLekce($id = 62)
    {
        $pageId = 62;
        $page = $this->pagesModel->admin_find($pageId)->fetch();
        
        $this->template->lekce = $this->pagesModel->findAll()->where('lft > %i', $page->lft)->and('rgt < %i', $page->rgt)->where('depth = %i', $page->depth + 1)->orderBy('lft', 'ASC')->fetchAll();
        
    }
    
    public function actionBanner()
    {
        $pageId = 212;
        $page = $this->pagesModel->admin_find($pageId)->fetch();
        
        $this->template->bannery = $this->pagesModel->findAll()->where('lft > %i', $page->lft)->and('rgt < %i', $page->rgt)->where('depth = %i', $page->depth + 1)->orderBy('lft', 'ASC')->fetchAll();
        
    }
    
    public function actionZazitky($id = 216)
    {
        $pageId = 216;
        $page = $this->pagesModel->admin_find($pageId)->fetch();
        
        $zazitky = $this->pagesModel->findAll()->where('lft > %i', $page->lft)->and('rgt < %i', $page->rgt)->where('depth = %i', $page->depth + 1)->orderBy('lft', 'ASC')->fetchAll();
        foreach($zazitky as $z) {
            $parameters = $this->pagesModel->findParameters($z->id)->fetchAll();
            foreach($parameters as $parameter) {
                if(isset($parameter)) {
                    $z->{str_replace('-', '', String::webalize($parameter->title))} = $parameter->value;
                }
            }
        }
        $this->template->zazitky = $zazitky;
        
    }
    
}
