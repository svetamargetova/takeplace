<?php
  
class Admin_FotogaleriePresenter extends Admin_SecuredPresenter
{
    
    const large_w = 800;
    const thumb_w = 300;
    const small_w = 116;
    
    public function actionDefault()
    {
        $this->template->fotogalerie = $this->galleriesModel->findAll()->fetchAll();
    }
    
    public function actionAdd()
    {
        $this->setView('edit');
    }

    public function actionEdit($id)
    {
        $galerie = $this->galleriesModel->form_find($id)->fetch();
        $galerie->published = date('j.n.Y G:i', $galerie->published);
        
        $fotky = array(0 => 'Bez hlavní fotky');
        $fotky += $this->photosModel->findAll()->where('galleryId = %i', $id)->fetchPairs('id', 'title');
        
        $this['editForm']->setDefaults($galerie);
        $this['editForm']['photo']->setItems($fotky);
    }    
                          
    public function actionActivate($id)
    {
        $this->galleriesModel->update($id, array('active' . $this->ln => 1));
        
        $this->flashMessage('Fotogalerie byla aktivována', 'done');
        
        $this->redirect('default');
    }
    
    protected function createComponentEditForm($name)
    {
        $id = $this->getParam('id');
        
        $user = Environment::getUser();
        
        $form = new AppForm($this, $name);
        
        $form->addText('title' . $this->ln, 'Název: *')
            ->addRule(Form::FILLED, 'Vyplňtě název fotogalerie');
        $form->addText('url' . $this->ln, 'Url:');
        /*$form->addTextArea('perex' . $this->ln, 'Krátký text:', 70);
        $form->addTextArea('text' . $this->ln, 'Text:', 70, 30);*/
        $form->addSelect('active' . $this->ln, 'Aktivní:', array(0 => 'Ne', 1 => 'Ano'))
            ->setDefaultValue(1);
        $form->addDateTimePicker('published', 'Čas spuštění:')
            ->setDefaultValue(date('j.n.Y G:i', time()));
        $form->addHidden('created')
            ->setValue(time());
        $form->addHidden('userId')
            ->setValue($user->identity->data['id']);
        $form->addMultipleFileUpload("upload","Fotky");
        if($id)
            $form->addSelect('photo', 'Hlavní fotka:', array());
        
        if($id)
            $form->addCheckbox('deletePhotos', 'Smazat již nahrané fotky?');
        
        $form->addSubmit('odeslat', 'Uložit');
        $form->onSubmit[] = array($this,"ulozit");
            
    }
    
    public function ulozit($form)
    {
        @set_time_limit(0);
        
        $id = $this->getParam('id');
        
        $values = $form->values;

        // Předáme data do šablony
        $this->template->values = $values;

        if($id && $values['deletePhotos']) {
            $this->photosModel->deleteByGalleryId($id);
            $this->flashMessage('Fotky galerie byly smazány', 'done');
        }
        
        unset($values['deletePhotos']);
        unset($values['upload']);
        
        if($values['url' . $this->ln])
            $values['url' . $this->ln] = String::webalize($values['url' . $this->ln]);
        else
            $values['url' . $this->ln] = String::webalize($values['title' . $this->ln]);

        try {        
            if($id) {
                $this->galleriesModel->update($id, $values);
                $this->flashMessage('Fotogalerie byla upravena.', 'done');
            } else {
                $this->galleriesModel->insert($values);
                $id = dibi::insertId();
                $this->flashMessage('Fotogalerie byla založena.', 'done');
            }
        } catch (DuplicateKeyException $e) {
            $form['url' . $this->ln]->addError('Fotogalerie se stejnou URL adresou již existuje, změňte ji.');
            return;
        }

        if($form['upload']->value) {
            foreach($form["upload"]->value AS $file) {

                if($file->isOk()){
                    $image = $file->toImage();
         
                    $large = clone $image;
                    $thumb = clone $image;
                    $small = clone $image;

                    $large->resize(self::large_w,NULL);
                    $thumb->resize(self::thumb_w,NULL);
                    $small->resize(self::small_w,NULL);

                    if(!is_dir(WWW_DIR . "/upload")) {
                        mkdir(WWW_DIR . "/upload", 0777);
                    }

                    if(!is_dir(WWW_DIR . "/upload/fotogalerie")) {
                        mkdir(WWW_DIR . "/upload/fotogalerie", 0777);
                    }
                    
                    if(!is_dir(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}")) {
                        mkdir(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}", 0777);
                    }
                    
                    $image->save(WWW_DIR."/upload/fotogalerie/{$values['url' . $this->ln]}/".String::webalize($file->getName(), '.'), 100);
                    chmod(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}/".String::webalize($file->getName(), '.'), 0666);

                    $large->save(WWW_DIR."/upload/fotogalerie/{$values['url' . $this->ln]}/large_".String::webalize($file->getName(), '.'), 100);
                    chmod(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}/large_".String::webalize($file->getName(), '.'), 0666);

                    $thumb->save(WWW_DIR."/upload/fotogalerie/{$values['url' . $this->ln]}/thumb_".String::webalize($file->getName(), '.'), 100);
                    chmod(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}/thumb_".String::webalize($file->getName(), '.'), 0666);

                    $small->save(WWW_DIR."/upload/fotogalerie/{$values['url' . $this->ln]}/small_".String::webalize($file->getName(), '.'), 100);
                    chmod(WWW_DIR . "/upload/fotogalerie/{$values['url' . $this->ln]}/small_".String::webalize($file->getName(), '.'), 0666);

                    $photo = array(
                        'galleryId'     => $id,
                        'title' . $this->ln => $file->getName(),
                        'filename'          => String::webalize($file->getName(), '.'),
                        'position' . $this->ln => $this->photosModel->getMaxPosition($id) + 1
                    );
                    
                    $this->photosModel->insert($photo);
                    $this->flashMessage('Fotka ' . $file->getName() . " byla úspěšně uložena.", 'done');
                }
                
            }
        }        

        $this->redirect('default');
    }

    public function actionDelete($id)
    {
        $this->setView('delete');

        $this['deleteForm']->onSubmit[] = array($this,'delete');

        $res = $this->galleriesModel->find($id)->fetch();
        $this->template->result = $res;
        $this->template->item = $res->nazev;
        $this->template->header = "Vymazat fotogalerii";
    }

    protected function createComponentDeleteForm($name)
    {
        $form = new AppForm($this, $name);
        $form->addSubmit('yes', 'Ano');
        $form->addSubmit('no', 'Ne');
    }
    
    public function delete($form)
    {
        $id = $this->getParam('id');
        
        $this->galleriesModel->delete($id);
        
        $this->redirect('default');
    }
    

    public function actionManage($id)
    {
        if(!$id)
            $this->redirect('default');
            
        $this->template->album = $this->galleriesModel->find($id)->fetch();
        
        $this->template->fotky = $this->photosModel->findAll()->where('galleryId = %i', $id)->orderBy('position%sql', $this->ln, 'ASC')->fetchAll();
        
        $this->template->form = $this['fotkyForm'];
    }
    
    public function actionPosun($id, $smer = 'u')
    {
        if(!$id)
            $this->redirect('default');
        
        $fotka = $this->photosModel->find($id)->fetch();
            
        if($smer == 'u')
            $this->photosModel->moveUp($id);
        else
            $this->photosModel->moveDown($id);
            
        $this->redirect('manage', $fotka->galleryId);
    }
    
    protected function createComponentFotkyForm($name)
    {
        $id = $this->getParam('id');
        
        $fotky = $this->photosModel->findAll()->where('galleryId = %i', $id)->fetchAll();
        
        $form = new AppForm($this, $name);
        
        foreach($fotky as $fotka) {
            $sub = $form->addContainer($fotka->id);
            $sub->addText('position' . $this->ln, 'Pozice', 1)
                ->setDefaultValue($fotka->position);
            $sub->addText('title' . $this->ln, 'Název:', 40)
                ->addRule(Form::FILLED, 'Vyplňte název fotky')
                ->setDefaultValue($fotka->title);
            $sub->addTextArea('text' . $this->ln, 'Text:', 50, 2)
                ->setDefaultValue($fotka->text);
            $sub->addCheckbox('delete', 'Smazat fotku z alba');
        }
        
        $form->addSubmit('ulozit', 'Uložit')
            ->onClick[] = callback($this, 'ulozitFotky');
    }
    
    public function ulozitFotky($button)
    {
        
        $form = $button->form;
        
        $values = $form->values;
        
        foreach($values as $id => $value) {
            if($value['delete']) {
                $this->photosModel->delete($id);
                $this->flashMessage('Fotka ' . $value['nazev' . $this->ln] . " byla smazána.", 'done');
            } else {
                unset($value['delete']);
                $this->photosModel->update($id, $value);
            }
        }
        
        $this->flashMessage('Fotky byly upraveny.', 'done');
        $this->redirect('default');
    }
}
