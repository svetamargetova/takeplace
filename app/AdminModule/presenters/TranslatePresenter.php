<?php
  
class Admin_TranslatePresenter extends Admin_SecuredPresenter
{

    public $langs = array('cs', 'en');
    
    public function actionDefault()
    {
        $this->template->langs = $this->langs;
        $this->template->table = $this->translationsModel->findAll()->fetchPairs('id', 'key');
    }

    
    protected function createComponentTable($name)
    {
        $cols = 40;
        
        $result = $this->translationsModel->findAll()->fetchAll();
        
        $form = new AppForm($this, $name);
        
        foreach($result as $row) {
            $sub = $form->addContainer($row->id);
            foreach($this->langs as $ln) {
                $sub->addText($ln, $ln, $cols)
                    ->setDefaultValue($row->$ln);
            }
        }
        
        $sub = $form->addContainer('new');
        $sub->addText('key', 'Key');
        foreach($this->langs as $ln) {
            $sub->addText($ln, $ln, $cols);
        }
        
        $form->addSubmit('save', 'Uložit')
            ->onClick[] = callback($this, 'saveTable');
    }
    
    
    public function saveTable($button)
    {
        
        $form = $button->form;
        
        foreach($form->components as $comp) {
            if($comp instanceOf FormContainer) {
                if(is_numeric($comp->name)) {
                    $this->translationsModel->update($comp->name, $comp->values);
                } else {
                    if($comp['key']->value)
                        $this->translationsModel->insert($comp->values);
                }
            }
        }
        
        $this->flashMessage('Překladová tabulka byla uložena.', 'done');
        $this->redirect('this');
    }
    
}
