<?php

abstract class Admin_SecuredPresenter extends Admin_BasePresenter
{
    public function startup()
    {
        $user = Environment::getUser();

        if (!$user->isLoggedIn() || !$user->isInRole('admin')) {
            if ($user->getLogoutReason() === User::INACTIVITY) {
                $this->flashMessage('Systém vás z důvodů neaktivity odhlásil. Prosím přihlašte se znovu.', 'warning');
            }

            $this->flashMessage('Pro vstup do administrace se musíte přihlásit!', 'warning');
            $backlink = $this->getApplication()->storeRequest();
            $this->redirect(':Admin:Auth:login', array('backlink' => $backlink));
        } elseif (!$user->isInRole('admin')) {
            $this->flashMessage('Pro přístup do administrace musíte mít odpovídající práva.', 'warning');
            $user->logout(TRUE);
            $this->redirect('this');
        } else {
            $uzivatel = $this->usersModel->find($user->identity->data['id'])->fetch();
            if($uzivatel->blocked) {
                $this->flashMessage('Byl vám zakázán přístup.', 'warning');
                $user->logout(TRUE);
                $this->redirect('this');
            }
        }

        parent::startup();
    }

    protected function beforeRender()
    {
        $user = Environment::getUser();

        $this->template->isAdmin = $user->isInRole('admin');
        
        parent::beforeRender();
    }
}
