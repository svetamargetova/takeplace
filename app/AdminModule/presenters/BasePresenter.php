<?php

abstract class Admin_BasePresenter extends BasePresenter
{
    
    public function startup()
    {
        parent::startup();
        
        $this->ln = "_{$this->ln}";
        
        $this->template->templateForm = $this['templateForm'];
        $this->template->templates = $this->templatesModel->findAll()->orderBy('title')->fetchAll();
        $this->template->templatesModel = $this->templatesModel;
        
        $this->template->parameterForm = $this['parameterForm'];
        $this->template->parameters = $this->parametersModel->findAll()->orderBy('title')->fetchAll();
        $this->template->parametersModel = $this->parametersModel;
    }

    protected function beforeRender()
    {
        $texy = new AdminTexy;

        $this->template->registerHelper('texy', array($texy, 'process'));

        $user = Environment::getUser();
        $this->template->uzivatel = ($user->isLoggedIn()) ? $user->getIdentity() : NULL;
        
        parent::beforeRender();
    }

    protected function createComponentTemplateForm($name)
    {
        $form = new AppForm($this, $name);
        $form->setAction($this->link('Template:default', array('do' => 'editForm-submit')));
        
        $sub = $form->addContainer('template');
        $sub->addText('title', 'Název šablony: *')
            ->addRule(Form::FILLED, 'Musíte zadat název šablony.');
        $sub->addText('template', 'Název souboru: *')
            ->addRule(Form::FILLED, 'Musíte zadat název souboru.');
        $sub->addText('subtemplate', 'Název souboru následovníka:');
        $sub->addHidden('sitemap')
            ->setValue(1);
            
        $form->addSubmit('save', 'Vložit')
            ->onClick[] = callback($this, 'save');
    }

    protected function createComponentParameterForm($name)
    {
        $form = new AppForm($this, $name);
        $form->setAction($this->link('Parameter:default', array('do' => 'editForm-submit')));
        
        $form->addText('title', 'Název parametru: *')
            ->addRule(Form::FILLED, 'Musíte zadat název parametru.');
        $form->addSelect('type', 'Typ pole:', 
            array(
                'text'      => 'Jednořádkové pole (text)',
                'textarea'  => 'Víceřádkové pole (textarea)',
                'gallery'   => 'Fotogalerie',
                'typ'   => 'Typ novinky',
                'datepicker'   => 'Výběr data',
                'file'   => 'Soubor'
            ));
            
        $form->addSubmit('save', 'Vložit')
            ->onClick[] = callback($this, 'save');
    }
    
    
}
