<?php
  
class Admin_ParameterPresenter extends Admin_SecuredPresenter
{

    public function renderDefault()
    {
        $this->redirect('Content:default');
    }
    
    public function renderEdit($id)
    {
        $parameter = $this->parametersModel->find($id)->fetch();
        
        $this['editForm']->setDefaults($parameter);
    }
    
    protected function createComponentEditForm($name)
    {
        $form = new AppForm($this, $name);
        
        $form->addText('title', 'Název parametru: *')
            ->addRule(Form::FILLED, 'Musíte zadat název parametru.');
        $form->addSelect('type', 'Typ pole:', 
            array(
                'text'      => 'Jednořádkové pole (text)',
                'textarea'  => 'Víceřádkové pole (textarea)',
                'gallery'   => 'Fotogalerie',
                'typ'   => 'Typ novinky',
                'datepicker'   => 'Výběr data',
                'file'  => 'Soubor'
            ));
            
        $form->addSubmit('save', 'Vložit')
            ->onClick[] = callback($this, 'save');
    }

    public function save($button)
    {
        $id = $this->getParam('id');

        $form = $button->form;
        
        $values = $form->values;

        try {        
            if($id)
                $this->parametersModel->update($id, $values);
            else
                $this->parametersModel->insert($values);
        } catch (DuplicateKeyException $e) {
            $form['title']->addError('Parametr s tímto názvem již existuje.');
            return;
        }
            
        if($id)
            $this->flashMessage('Parametr byl změněn.', 'done');
        else
            $this->flashMessage('Parametr byl založen.', 'done');

        $this->redirect('Content:default');
    }
    
    
    
    public function actionDelete($id)
    {
        $this->setView('delete');

        $this['deleteForm']->onSubmit[] = array($this,'delete');

        $res = $this->parametersModel->find($id)->fetch();
        $this->template->result = $res;
        $this->template->item = $res->title;
        $this->template->header = "Vymazat parametr";
    }

    protected function createComponentDeleteForm($name)
    {
        $form = new AppForm($this, $name);
        $form->addSubmit('yes', 'Ano');
        $form->addSubmit('no', 'Ne');
    }
    
    public function delete($form)
    {
        $id = $this->getParam('id');
        
        $this->parametersModel->delete($id);
        
        $this->redirect('Content:default');
    }

}
