<?php
  
class Admin_SubscriberPresenter extends Admin_SecuredPresenter
{
    
    public function actionDefault()
    {
        $this->template->subscribers = $this->newslettersModel->findAll()->orderBy('date', 'ASC')->fetchAll();
    }
    
    
}
