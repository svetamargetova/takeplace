<?php

abstract class BaseModel extends Object
{
    /********************* Connection handling *********************/

    /** @var DibiConnection */
    public static $defaultConnection;

    /**
     * Ustaví připojení k DB
     */
    public static function connect()
    {
        // Použije konfiguraci z config.ini
        self::$defaultConnection = dibi::connect(Environment::getConfig('database'));
        //dibi::getProfiler()->setFile(WWW_DIR . '/log/log.sql');
    }

    /**
     * Odpojí od DB
     */
    public static function disconnect()
    {
        self::$defaultConnection->disconnect();
    }


    /********************* Model behaviour *********************/

    /** @var DibiConnection */
    protected $db;

    /** @var string object name */
    protected $table = 'page';

    /** @var string primary key name */
    protected $pk = 'id';

    /** @var bool autoincrement? */
    protected $autoIncrement = TRUE;

    protected $ln;
    
    public function __construct(DibiConnection $connection = NULL)
    {
        $this->db = ($connection !== NULL ? $connection : self::$defaultConnection);
        
        $ln = Environment::getSession('ln');
        if(!$ln->ln)
            $ln->ln = 'cs';
            
        if($this->ln !== $ln->ln)
            $this->ln = '_' . $ln->ln;
    }


    /**
     * Výběr všech záznamů z DB
     * @return DibiResult
     */
    public function findAll($front = FALSE)
    {
        $result = $this->db->select('*')
            ->from($this->table);
        if($front)
            $result->and('aktivni%sql', $this->ln, ' = %i', 1);
        return $result;
    }

    /**
     * Vybere záznam dle zadaného ID
     * @param int $id
     * @return DibiResult
     */
    public function find($id, $front = FALSE)
    {
        $result = $this->db->select('*')
            ->from($this->table)
            ->where('%n = %i',$this->pk, $id);
        if($front)
            $result->and('aktivni%sql', $this->ln, ' = %i', 1);
        return $result;
    }

    public function form_findAll()
    {
        return self::findAll();
    }
    
    public function form_find($id)
    {
        return self::find($id);
    }
    
    public function findByUrl($url, $front = FALSE)
    {
        $result = $this->db->select('*')
            ->from($this->table)
            ->where('url%sql', $this->ln, ' = %s', $url);
        if($front)
            $result->and('aktivni%sql', $this->ln, ' = %i', 1);
        return $result;
    }
        
    /**
    * Update záznamu v DB
    * 
    * @param mixed $id
    * @param mixed $arr
    * @param mixed $table
    * @param mixed $pk
    */
    public function update($id, $values, $table = '', $pk = '')
    {
        if(!$table) $table = $this->table;
        if(!$pk) $pk = $this->pk;
        try {
            $this->db->update($table, $values)
                ->where("$pk = %i",$id)
                ->execute();
        } catch (DibiException $e) {
            if ($e->getCode() === DibiMySqlDriver::ERROR_DUPLICATE_ENTRY) {
                throw new DuplicateKeyException;
            } else {
                throw $e;
            }
        }
    }

    /**
    * Insert záznamu do DB
    * 
    * @param mixed $arr
    * @param mixed $table
    */
    public function insert($values, $table = '')
    {
        if(!$table) $table = $this->table;
        try {
            $this->db->insert($table, $values)
                ->execute($this->autoIncrement ? dibi::IDENTIFIER : NULL);
        } catch (DibiException $e) {
            if ($e->getCode() === DibiMySqlDriver::ERROR_DUPLICATE_ENTRY) {
                throw new DuplicateKeyException;
            } else {
                throw $e;
            }
        }
    }

    /**
    * Delete záznamu z DB
    * 
    * @param mixed $id
    * @param mixed $table
    * @param mixed $pk
    */
    public function delete($id, $table = '', $pk = '')
    {
        if(!$table) $table = $this->table;
        if(!$pk) $pk = $this->pk;
        $this->db->delete($table)
            ->where("$pk = %i",$id)
            ->execute();
    }
    
    public function getPoradi($id)
    {
        return $this->db->select('pozice')
            ->from($this->table)
            ->where('id = %i', $id)
            ->fetchSingle();
    }

    public function getMinPosition($parentId = 0)
    {
        $result = $this->db->select('MIN(position) as position')
            ->from($this->table);
        if($parentId)
            $result->where('parentId = %i', $parentId);
        return $result->fetchSingle();
    }
    
    public function getMaxPosition($parentId = null)
    {
        $result = $this->db->select('MAX(position) as position')
            ->from($this->table);
        if($parentId != null)
            $result->where('parentId = %i', $parentId);
        return $result->fetchSingle();
    }
    
    public function moveUp($id)
    {
        $r = $this->db->select('t2.id, t2.position as novePoradi, t1.position as starePoradi')
            ->from('%n as t1', $this->table)
            ->innerJoin('%n as t2', $this->table)->on('t1.parentId = t2.parentId')
            ->where('t2.pozice < t1.pozice')
            ->and('t1.id = %i', $id)
            ->orderBy('t2.pozice', 'DESC')
            ->fetch();
        
        $this->update($id, array('pozice' => $r->novePoradi));
        $this->update($r->id, array('pozice' => $r->starePoradi));
    }
    
    public function moveDown($id)
    {
        $r = $this->db->select('t2.id, t2.pozice as novePoradi, t1.pozice as starePoradi')
            ->from('%n as t1', $this->table)
            ->innerJoin('%n as t2', $this->table)->on('t1.parentId = t2.parentId')
            ->where('t2.pozice > t1.pozice')
            ->and('t1.id = %i', $id)
            ->orderBy('t2.pozice', 'ASC')
            ->fetch();
        
        $this->update($id, array('pozice' => $r->novePoradi));
        $this->update($r->id, array('pozice' => $r->starePoradi));
    }

    public function resort()
    {
        $items = $this->findAll()->orderBy('parentId, pozice', 'ASC')->fetchAll();
        
        $i = 1;
        $lastParent = 0;
        foreach($items as $item) {
            if($item->parentId != $lastParent)
                $i = 1;
            
            $this->update($item->id, array('pozice' => $i));
            
            $lastParent = $item->parentId;
            
            $i++;
        }
    }
    
}

class DuplicateKeyException extends InvalidArgumentException
{
}
class NoImageException extends InvalidArgumentException
{
}
