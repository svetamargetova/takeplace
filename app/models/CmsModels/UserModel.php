<?php

class Cms_UserModel extends BaseModel implements IAuthenticator
{
    protected $table = 'cms_user';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function authenticate(array $credentials)
    {
        $login = $credentials[self::USERNAME];
        $row = $this->findByLogin($login);

        if(!$row) {
            throw new AuthenticationException("Chybně zadané uživatelské jméno nebo heslo.", self::IDENTITY_NOT_FOUND);
        }

        $config = Environment::getConfig('security');
        $password =  hash_hmac('sha256', $credentials[self::PASSWORD] . $row->salt , $config->hmacKey);
        
        if($row->password !== $password) {
            throw new AuthenticationException("Chybně zadané uživatelské jméno nebo heslo.", self::INVALID_CREDENTIAL);
        }
        
        if($row->blocked) {
            throw new AuthenticationException('Zadanému uživateli byl zablokován přístup do redakčního systému.', self::INVALID_CREDENTIAL);
        }

        unset($row->password, $row->salt);
        $this->update($row->id, array('lastLogin' => time()));
        return new Identity($row->name, $row->role, $row);
    }

    public function findAll($front = FALSE)
    {
        return $this->db->select('
            u.*,
            CONCAT_WS(" ", u.firstName, u.sureName) as name')
            ->from($this->table, 'as u');
    }
    
    public function find($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('u.id = %i', $id);
    }
    
    public function findByLogin($login)
    {
        $row = $this->findAll()
          ->where('u.email = %s', $login)
          ->fetch();

        return ($row) ? $row : NULL;
    }

}
