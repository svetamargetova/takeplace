<?php

class Cms_ProductModel extends BaseModel
{

    protected $table = 'cms_product';
    
    protected $pageproduct = 'cms_pageproduct';
    
    protected $page = 'cms_page';
    
    protected $productparameter = 'cms_productparameter';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function findRelations($id)
    {
        return $this->db->select('p.pageId as id, p.productId')
            ->from($this->pageproduct, 'as p')
            ->where('productId = %i', $id);
    }
    
    public function insertRelation($pageId, $productId)
    {
        parent::insert(array('pageId' => $pageId, 'productId' => $productId), $this->pageproduct);
    }
    
    public function deleteRelations($id)
    {
        parent::delete($id, $this->pageproduct, 'productId');
    }
    
    public function findParameters($id)
    {
        return $this->db->select('p.parameterId as id, p.value')
            ->from($this->productparameter, 'as p')
            ->where('productId = %i', $id);
    }
    
    public function insertParameter($productId, $parameterId, $value)
    {
        parent::insert(array('productId' => $productId, 'parameterId' => $parameterId, 'value' => $value), $this->productparameter);
    }

    public function deleteParameters($id)
    {
        parent::delete($id, $this->productparameter, 'productId');
    }
    
    public function form_find($id)
    {
        return parent::find($id);
    }

    public function find($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('id = %i', $id);
    }
    
    public function findAll($front = FALSE)
    {
        $result = $this->db->select('
                pr.id, 
                pr.title%sql', $this->ln,' as title,
                pr.url%sql', $this->ln,' as url,
                pr.perex%sql', $this->ln,' as perex,
                pr.active%sql', $this->ln,' as active,
                pr.price, 
                pr.position%sql', $this->ln,' as position, 
                pr.image,
                pr.link')
            ->from($this->table, 'as pr');
        if($front)
            $result->where('active = %i', 1);
            
        return $result;
    }
    
    public function findByPageId($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->innerJoin($this->pageproduct, 'as pa')->on('pr.id = pa.productId')
            ->where('pa.pageId = %i', $id);
    }

    public function findRandom($pages, $front = FALSE)
    {
        return $this->findAll($front)
            ->innerJoin($this->pageproduct, 'as pa')->on('pr.id = pa.productId')
            ->where('pa.pageId IN %in', $pages);
    }

    public function findNonRelated()
    {
        return $this->findAll()
            ->where('id NOT IN (SELECT productId FROM %n)', $this->pageproduct);
    }

}
