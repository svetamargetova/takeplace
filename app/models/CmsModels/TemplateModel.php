<?php

class Cms_TemplateModel extends BaseModel
{

    protected $table = 'cms_template';
    
    protected $parameter = 'cms_parameter';
    
    protected $templateparameter = 'cms_templateparameter';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function insertParameter($values)
    {
        parent::insert($values, $this->templateparameter);
    }
    
    public function deleteParameters($id)
    {
        parent::delete($id, $this->templateparameter, 'templateId');
    }
    
    public function findParameters($id)
    {
        return $this->db->select('p.id, p.title, p.type')
            ->from($this->templateparameter, 'as tp')
            ->innerJoin($this->parameter, 'as p')->on('tp.parameterId = p.id')
            ->where('tp.templateId = %i', $id);
    }
    
}