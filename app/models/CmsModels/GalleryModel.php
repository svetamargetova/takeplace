<?php

class CMS_GalleryModel extends BaseModel
{

    protected $table = 'cms_gallery';
    
    protected $fotka = 'cms_photo';
    
    protected $pk = 'id';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id, $table = '', $pk = '')
    {
        parent::delete($id, $this->fotka, 'galleryId');
        
        parent::delete($id);
    }
    
    public function form_findAll($front = FALSE)
    {
        return parent::findAll($front);
    }
    
    public function form_find($front = FALSE)
    {
        return parent::find($front);
    }
    
    public function findAll($front = FALSE)
    {
        $result = $this->db->select('
            t.id, 
            t.title%sql', $this->ln, ' as title,
            t.url%sql', $this->ln, ' as url,
            t.perex%sql', $this->ln, ' as perex,
            t.text%sql', $this->ln, ' as text,
            t.active%sql', $this->ln, ' as active,
            t.created,
            t.published,
            COUNT(f.id) as photosCount,
            f2.filename as photo,
            f.filename as photo2')
            ->from($this->table, 'as t')
            ->leftOuterJoin($this->fotka, 'as f')->on('t.id = f.galleryId')
            ->leftOuterJoin($this->fotka, 'as f2')->on('f2.id = t.photo');
        if($front)
            $result->where('t.active%sql', $this->ln, ' = %i', 1);
        return $result->groupBy('t.id');
    }

    public function find($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('t.id = %i', $id);
    }

    public function findByUrl($url, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('t.url%sql', $this->ln, ' = %s', $url);
    }

    public function pocetFotek($id)
    {
        return $this->db->select('COUNT(id)')
            ->from($this->fotka)
            ->where('galleryId = %i', $id)
            ->fetchSingle();
    }
    
}