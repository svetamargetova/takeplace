<?php

class CMS_PhotoModel extends BaseModel
{

    protected $table = 'cms_photo';
    
    protected $galerie = 'cms_gallery';
    
    protected $pk = 'id';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($values, $table = '') {
        $this->db->query('INSERT INTO ', $this->table, $values, ' ON DUPLICATE KEY UPDATE %a', $values);
    }
    
    public function deleteByGalleryId($id)
    {
        parent::delete($id, $this->table, 'galleryId');
    }

    public function form_findAll($front = FALSE)
    {
        return parent::findAll($front);
    }
    
    public function form_find($front = FALSE)
    {
        return parent::find($front);
    }
    
    public function findAll($front = FALSE)
    {
        return $this->db->select('
            t.id, 
            t.galleryId,
            t.title%sql', $this->ln, ' as title,
            t.text%sql', $this->ln, ' as text,
            t.filename,
            g.title%sql', $this->ln, 'as galleryTitle,
            g.url%sql', $this->ln, 'as galleryUrl,
            t.position%sql', $this->ln, 'as position')
            ->from($this->table, 'as t')
            ->innerJoin($this->galerie, 'as g')->on('g.id = galleryId')
            ->orderBy('t.position%sql', $this->ln, 'ASC');
    }

    public function find($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('t.id = %i', $id);
    }

    public function getMaxPosition($galleryId = 0)
    {
        return $this->db->select('MAX(position%sql', $this->ln, ') as position')
            ->from($this->table)
            ->where('galleryId = %i', $galleryId)
            ->fetchSingle();
    }
    
    public function moveUp($id)
    {
        $r = $this->db->select('t2.id, t2.position%sql', $this->ln, 'as novePoradi, t1.position%sql', $this->ln, 'as starePoradi')
            ->from('%n as t1', $this->table)
            ->innerJoin('%n as t2', $this->table)->on('t1.galleryId = t2.galleryId')
            ->where('t2.position%sql', $this->ln, ' < t1.position%sql', $this->ln)
            ->and('t1.id = %i', $id)
            ->orderBy('t2.position%sql', $this->ln, 'DESC')
            ->fetch();
        
        $this->update($id, array('position' . $this->ln => $r->novePoradi));
        $this->update($r->id, array('position' . $this->ln => $r->starePoradi));
    }
    
    public function moveDown($id)
    {
        $r = $this->db->select('t2.id, t2.position%sql', $this->ln, 'as novePoradi, t1.position%sql', $this->ln, 'as starePoradi')
            ->from('%n as t1', $this->table)
            ->innerJoin('%n as t2', $this->table)->on('t1.galleryId = t2.galleryId')
            ->where('t2.position%sql', $this->ln, ' > t1.position%sql', $this->ln)
            ->and('t1.id = %i', $id)
            ->orderBy('t2.position%sql', $this->ln, 'ASC')
            ->fetch();
        
        $this->update($id, array('position' . $this->ln => $r->novePoradi));
        $this->update($r->id, array('position' . $this->ln => $r->starePoradi));
    }

}