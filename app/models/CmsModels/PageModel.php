<?php

class Cms_PageModel extends BaseModel
{

    protected $table = 'cms_page';
    
    protected $template = 'cms_template';
    
    protected $pageparameter = 'cms_pageparameter';
    
    protected $parameter = 'cms_parameter';
    
    protected $pageproduct = 'cms_pageproduct';
    
    const lft = 'lft';
    const rgt = 'rgt';
    const depth = 'depth';
    
    const MOVE_LEFT = -1;
    const MOVE_RIGHT = 1;
    
    /**
     * Insert child node of parent
     * @param int $parentId id of parent node
     * @param array $data data to insert to node
     * @return bool true if success else false
     */
    public function insert($values, $table = '') {
        $this->db->query("START TRANSACTION");

        $parent = $this->find($values['parentId'])->fetch();
        unset($values['parentId']);
        if($parent) {
            $this->db->query("UPDATE {$this->table} SET ".self::lft." = ".self::lft." + 2 WHERE ".self::lft." > ".$parent[self::rgt]);
            $this->db->query("UPDATE {$this->table} SET ".self::rgt." = ".self::rgt." + 2 WHERE ".self::rgt." >= ".$parent[self::rgt]);
            
            $values[self::lft] = isset($parent->rgt) ? $parent[self::rgt] : 1;
            $values[self::rgt] = isset($parent->rgt) ? $parent[self::rgt] + 1 : 2;
            $values[self::depth] = isset($parent->depth) ? $parent[self::depth] + 1 : 1;
        } else {
            $neighbour = $this->findAll()->where('depth = %i', 1)->orderBy('lft', 'DESC')->fetch();
            
            $values[self::lft] = isset($neighbour->rgt) ? $neighbour[self::rgt] + 1 : 1;
            $values[self::rgt] = isset($neighbour->rgt) ? $neighbour[self::rgt] + 2 : 2;
            $values[self::depth] = isset($neighbour->depth) ? $neighbour[self::depth] : 1;
        }

        parent::insert($values, $this->table);
        $result = $this->db->insertId();
        $this->db->query("COMMIT");
        
        return $result;
    }
    
    /**
     * Remove node with subtree
     * @param int $nodeId
     * @return bool true if success else false
     */
    public function delete($id, $table = '', $pk = '') {
        $node = $this->find($id)->fetch();
        $this->db->query("START TRANSACTION");
        try {
            $result = $this->db->delete($this->table)
                ->where(self::lft.">=%i", $node[self::lft])
                ->and(self::rgt."<=", $node[self::rgt])
                ->execute();
        } catch (DibiException $e) {
            switch($e->getCode()) {
                case 1451:
                    throw new FKeyConstraintException;
                    break;
                default:
                    throw $e;
            }
        }
        // optimalize tree
        $space = $node[self::rgt] - $node[self::lft] + 1;
        $this->db->query("UPDATE {$this->table} SET ".self::lft." = ".self::lft." - %i", $space," WHERE ".self::lft.">%i", $node[self::rgt]);        
        $this->db->query("UPDATE {$this->table} SET ".self::rgt." = ".self::rgt." - %i", $space," WHERE ".self::rgt.">%i", $node[self::rgt]);        
        $this->db->query("COMMIT");
        
        return $result;
    }
    
    public function update($id, $values, $table = '', $pk = '')
    {
        if(isset($values['parentId'])) {
            $new_parentId = $values['parentId'];
            unset($values['parentId']);
        }
        
        $parent = $this->findParent($id)->fetch();
        $old_parent = 0;
        if($parent)
            $old_parent = $parent->id;
        
        $this->db->query("START TRANSACTION");
        
        parent::update($id, $values);
        
        if(isset($new_parentId) && ($new_parentId != $old_parent))
            $this->move($id, $new_parentId);

        $this->db->query("COMMIT");
    }
    
    /**
     * Move Node in tree
     * @param int $nodeId
     * @return unknown_type
     */
    public function move($id, $parentId) {
        $this->db->query("START TRANSACTION");
        
        $node = $this->find($id)->fetch();
        $newParent = $this->find($parentId)->fetch();
        
        $size = $node[self::rgt] - $node[self::lft] + 1;
        
        if (!$newParent) { // node is root
            $lft = $this->db->fetchSingle("SELECT IFNULL(MAX(".self::rgt.") + 1, 1) FROM {$this->table}");
            $depth = 0;
        } else {
            $lft = $newParent[self::rgt];
            $depth = $newParent[self::depth] + 1;
        }
        if ($lft > $node[self::lft]) {
            $lft -= $size;
        }
        if ($lft != $node["lft"]) {
            $min_lft = min($lft, $node[self::lft]);
            $max_rgt = max($lft + $size - 1, $node[self::rgt]);
            $shift = $lft - $node[self::lft];
            if ($lft > $node["lft"]) {
                $size = -$size;
            }
        
            $result = $this->db->query("
                UPDATE {$this->table}
                SET ".self::depth." = ".self::depth." + IF (
                        @subtree := ".self::lft." >= %i AND ".self::rgt." <= %i,",$node[self::lft], $node[self::rgt],
                        "%i,", $depth - $node[self::depth],
                        "%i),", 0, 
                    self::lft." = ".self::lft. " + IF(
                        @subtree,
                        %i,", $shift,
                        "IF(".self::lft." >= %i, %i, %i)),", $min_lft, $size, 0,
                    self::rgt." = ".self::rgt." + IF(
                        @subtree, 
                        %i,", $shift,
                        "IF(".self::rgt." <= %i, %i, %i))", $max_rgt, $size, 0,
                    "WHERE ".self::rgt." >= %i AND ".self::lft." <= %i", $min_lft, $max_rgt);
            }
        $this->db->query("COMMIT");
        
        return $result;
    }
    
    
    /**
     * Shift node on the same level to right or left
     * @param int $nodeId
     * @param int $direction self::MOVE_LEFT for left, self::MOVE_RIGHT for right
     * @return bool
     */
    
    public function shift($id, $direction) {
        
        $this->db->query("START TRANSACTION");
        
        $node = $this->find($id)->fetch();
        if ($direction === self::MOVE_LEFT) {
            $swapNode = $this->findLeft($id)->fetch();
        } elseif ($direction === self::MOVE_RIGHT) {
            $swapNode = $this->findRight($id)->fetch();
        } else {
            throw new InvalidArgumentException("Direction must be one of constants of TTreeMapper");
        }
        if (!$swapNode) {
            return false;
        }

        $min_lft = min($node[self::lft], $swapNode[self::lft]);
        $max_rgt = max($node[self::rgt], $swapNode[self::rgt]);
        
        $nodeSize = $node[self::rgt] - $node[self::lft] + 1;
        $swapNodeSize = $swapNode[self::rgt] - $swapNode[self::lft] + 1;
        $nodeSize *= (-1);
        
        if ($direction === self::MOVE_LEFT) {
            $nodeSize *= (-1);
            $swapNodeSize *= (-1);
        }

        $res = dibi::query("
        UPDATE {$this->table}
        SET ".self::lft." = ".self::lft. " + IF(
                @subtree := ".self::lft." >= %i AND ".self::rgt." <= %i,",$node[self::lft], $node[self::rgt],
                "%i,", $swapNodeSize,
                "%i", $nodeSize, "),"
            .self::rgt." = ".self::rgt." + IF(
                @subtree, 
                %i,", $swapNodeSize,
                "%i", $nodeSize, ")
            WHERE ".self::lft." >= %i AND ".self::rgt." <= %i", $min_lft, $max_rgt);
        dibi::query("COMMIT");
        return $res;
    } 
    
    public function shiftLeft($id) {
        $this->shift($id, self::MOVE_LEFT);
    }
    
    public function shiftRight($id) {
        $this->shift($id, self::MOVE_RIGHT);
    }
    
    
    public function findParameters($id)
    {
        return $this->db->select('p.parameterId as id, p.value, pa.title, pa.type')
            ->from($this->pageparameter, 'as p')
            ->innerJoin($this->parameter, 'as pa')->on('p.parameterId = pa.id')
            ->where('pageId = %i', $id);
    }
    
    public function insertParameter($values)
    {
        parent::insert($values, $this->pageparameter);
    }
    
    public function deleteParameters($id)
    {
        parent::delete($id, $this->pageparameter, 'pageId');
    }
 
    public function form_find($id)
    {
        return parent::find($id);
    }
    
    public function form_findAll()
    {
        return parent::findAll();
    }
    
    public function admin_findAll()
    {
        return $this->db->select('
            c.id,
            c.depth,
            c.lft,
            c.rgt,
            c.url,
            c.title,
            c.template,
            t.subtemplate,
            c.active,
            c.menu,
            c.published,
            c.viewed,
            c.system')
            ->from($this->table, 'as c')
            ->innerJoin($this->template, 'as t')->on('c.template = t.template');
    }

    public function admin_find($id)
    {
        return $this->admin_findAll()
            ->where('c.id = %i', $id);
    }

    public function admin_findParent($id, $front = FALSE, $order = 'DESC')
    {
        $page = $this->find($id)->fetch();
        
        return $this->admin_findAll($front)
            ->where('c.lft < %i', $page->lft)
            ->and('c.rgt > %i', $page->rgt)
            ->orderBy('lft', $order);
    }
    
    public function findAll($front = FALSE)
    {
        $result = $this->db->select('
            c.id,
            c.depth,
            c.lft,
            c.rgt,
            c.title,
            c.url,
            c.perex,
            c.text,
            c.active,
            c.created,
            c.published,
            c.author,
            c.image,
            c.template,
            c.menu,
            c.system,
            c.viewed,
            c.seo_title,
            c.seo_keywords,
            c.seo_description')
            ->from($this->table, 'as c');
        if($front)
            $result->where('c.active = %i', 1)
                ->and('c.published <= %i', time());
        return $result;
    }
    
    public function find($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('c.id = %i', $id);
    }    

    public function findLeft($id) {
        $node = $this->find($id)->fetch();
        
        return $this->findAll()
            ->where(self::rgt."=%i", $node[self::lft] - 1)
            ->and(self::depth."=%i", $node[self::depth]);
    }
    
    public function findRight($id) {
        $node = $this->find($id)->fetch();
        
        return $this->findAll()
            ->where(self::lft."=%i", $node[self::rgt] + 1)
            ->and(self::depth."=%i", $node[self::depth]);
    }
    
    public function findByUrl($url, $front = FALSE)
    {
        return $this->findAll($front)
            ->where('c.url = %s', $url);
    }    

    public function findParent($id, $front = FALSE, $order = 'DESC')
    {
        $page = $this->find($id)->fetch();
        
        return $this->findAll($front)
            ->where('c.lft < %i', $page->lft)
            ->and('c.rgt > %i', $page->rgt)
            ->orderBy('lft', $order);
    }
    
    public function findChildren($page, $front = FALSE, $deep = FALSE, $sitemap = FALSE)
    {
        //Debug::barDump($page);
        
        $result = $this->findAll($front)
            ->where('c.lft > %i', $page->lft)
            ->and('c.rgt < %i', $page->rgt);
            
        if(!$deep)
            $result->and('c.depth = %i', $page->depth + 1);
            
        if($sitemap)
            $result->and('c.sitemap = %i', 1);
            
        return $result;
    }
    
    public function constructUrl($page, $depth = 0)
    {
        $urls = $this->db->select('url')
            ->from($this->table)
            ->where('lft < %i', $page->lft)
            ->and('rgt > %i', $page->rgt)
            ->and('depth > %i', $depth)
            ->orderBy('lft', 'ASC')
            ->fetchAll();
            
        $url = "";
        foreach($urls as $key => $value) {
            $url .= "{$value->url}/";
        }
        $url .= $page->url;
        return $url;
    }
    
    public function findTemplates()
    {
        return $this->db->select('
            id,
            template,
            title')
            ->from($this->template);
    }

    public function findByProductId($id, $front = FALSE)
    {
        return $this->findAll($front)
            ->innerJoin($this->pageproduct, 'as p')->on('c.id = p.pageId')
            ->where('p.productId = %i', $id);
    }
    
    
    public function findByParam($param = 12, $front = TRUE)
    {
        $result = $this->db->select('
            c.id,
            c.depth,
            c.lft,
            c.rgt,
            c.title,
            c.url,
            c.perex,
            c.text,
            c.active,
            c.created,
            c.published,
            c.author,
            c.image,
            c.template,
            c.menu,
            c.system,
            c.viewed,
            c.seo_title,
            c.seo_keywords,
            c.seo_description,
            pp.value as datum_kurzu')
            ->from($this->table, 'as c')
            ->innerJoin($this->pageparameter, 'as pp')->on('c.id = pp.pageId')
            ->where('pp.parameterId = %i', $param);
        
        if($front) {
            $result
                ->and('c.active = %i', 1)
                ->and('c.published <= %i', time())
                ->and('pp.value > %i', time());
        }
        
        return $result;
    }
    
    public function findZazitky($front = TRUE)
    {
        $page = $this->find(216)->fetch();
        
        $result = $this->db->select('
            c.id,
            c.depth,
            c.lft,
            c.rgt,
            c.title,
            c.url,
            c.perex,
            c.text,
            c.active,
            c.created,
            c.published,
            c.author,
            c.image,
            c.image_hp,
            c.template,
            c.menu,
            c.system,
            c.viewed,
            c.seo_title,
            c.seo_keywords,
            c.seo_description,
            datum.value as datum,
            cena.value as cena')
            ->from($this->table, 'as c')
            ->innerJoin($this->pageparameter, 'as datum')->on('c.id = datum.pageId AND datum.parameterId = %i', 2)
            ->innerJoin($this->pageparameter, 'as cena')->on('c.id = cena.pageId AND cena.parameterId = %i', 13)
            ->where('c.lft > %i', $page->lft)
            ->and('c.rgt < %i', $page->rgt);
        
        if($front) {
            $result
                ->and('c.active = %i', 1)
                ->and('c.published <= %i', time())
                ->and('datum.value > %i', time());
        }
        
        return $result;
    }
    
}
