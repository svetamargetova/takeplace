<?php



// Step 1: Load Nette Framework
// this allows load Nette Framework classes automatically so that
// you don't have to litter your code with 'require' statements
require LIBS_DIR . '/Nette/loader.php';

$session = Environment::getSession();
$session->setSavePath(APP_DIR . '/sessions/');
$session->setExpiration(3600);


/*
// Step 2: Configure environment
// 2a) enable Nette\Debug for better exception and error visualisation
$emailHeaders = array(
    'From' => 'no-reply@sorudo.cz',
    'To'   => 'tomas.macuga@divdesign.cz',
    'Subject' => 'Chyba na serveru %host%',
    'Body' => '%date% - %message%. Pro více informací shlédněte error log.',
);
// Step 2: Configure environment
// 2a) enable Debug for better exception and error visualisation
Debug::enable(DEBUG::DETECT, APP_DIR .'/log/php_error.log', $emailHeaders);
*/

Debug::$logDirectory = WWW_DIR . '/log';
Debug::$email = 'tomas.macuga@divdesign.cz';
Debug::$strictMode = FALSE;
Debug::$maxDepth = 4;
Debug::enable();

/*
Environment::setMode(Environment::DEVELOPMENT);
Debug::enable(DEBUG::DEVELOPMENT, APP_DIR .'/log/php_error.log', $emailHeaders);
*/

// 2b) load configuration from config.ini file
Environment::loadConfig();


// Step 3: Configure application
// 3a) get and setup a front controller
$application = Environment::getApplication();
$application->errorPresenter = 'CMS:Error';
$application->catchExceptions = Environment::isProduction();


//Environment::setMode(Environment::PRODUCTION, FALSE);

function Form_addDateTimePicker(Form $_this, $name, $label, $cols = NULL, $maxLength = NULL)
{
  return $_this[$name] = new DateTimePicker($label, $cols, $maxLength);
}

function FormContainer_addDateTimePicker(FormContainer $_this, $name, $label, $cols = NULL, $maxLength = NULL)
{
  return $_this[$name] = new DateTimePicker($label, $cols, $maxLength);
}

Form::extensionMethod('Form::addDateTimePicker', 'Form_addDateTimePicker');  // v PHP 5.2
FormContainer::extensionMethod('FormContainer::addDateTimePicker', 'FormContainer_addDateTimePicker');  // v PHP 5.2
Form::extensionMethod('Form::addSuggestInput', 'SuggestInput_addSuggestInput');
FormContainer::extensionMethod('FormContainer::addCheckboxList', 'CheckboxList_addCheckboxList');
FormContainer::extensionMethod("FormContainer::addDependentSelectBox", array("DependentSelectBox","formAddDependentSelectBox"));
FormContainer::extensionMethod("FormContainer::addJsonDependentSelectBox", array("JsonDependentSelectBox","formAddJsonDependentSelectBox"));

// Step 4: Setup application router
$router = $application->getRouter();

// mod_rewrite detection
//if (function_exists('apache_get_modules') && in_array('mod_rewrite', apache_get_modules())) {

    Route::addStyle('url', NULL);
    Route::setStyleProperty('url', Route::PATTERN, '.*?');
    
    # AdminModule routes
    $router[] = new Route('admin/eshop/<presenter>/<action>/<id>', array(
        'module'    => 'Admin:Eshop',
        'presenter' => 'Default',
        'action'    => 'default',
        'id'        => null
    ));

    $router[] = new Route('admin/<presenter>/<action>/<id>', array(
        'module'    => 'Admin',
        'presenter' => 'Default',
        'action'    => 'default',
        'id'        => null
    ));

    $router[] = new Route('index.php', array(
        'module' => 'CMS',
        'presenter' => 'Default',
        'action'    => 'default'
    ), Route::ONE_WAY);

    $router[] = new Route('en-<url>', array(
        'module' => 'CMS',
        'presenter' => 'Default',
        'action'    => 'view',
        'url'       => null
    ), Route::ONE_WAY);

    $router[] = new Route('cs-<url>', array(
        'module' => 'CMS',
        'presenter' => 'Default',
        'action'    => 'view',
        'url'       => null
    ), Route::ONE_WAY);

    $router[] = new Route('<url>', array(
        'module'    => 'CMS',
        'presenter' => 'Default',
        'action'    => 'view'
    ));

    $router[] = new Route('<presenter>', array(
        'module'    => 'CMS',
        'presenter' => 'Default',
        'action'    => 'default',
    ));

    $router[] = new Route('<presenter>/<action>/<url>', array(
        'module'    => 'CMS',
        'presenter' => 'Default',
        'action'    => 'view',
        'url'        => null
    ));


$application->onStartup[] = 'BaseModel::connect';
//$application->onShutdown[] = 'BaseModel::disconnect';

MultipleFileUpload::register();

//Panel::register();

// Step 5: Run the application!
$application->run();
