<?php

class CMS_DefaultPresenter extends CMS_BasePresenter
{
    
    public $page;
    
    public $xml = FALSE;
    
    protected $blocksId = array('en' => 9, 'cs' => 38);
    protected $homeId = array('en' => 1, 'cs' => 32);
    protected $karieraId = 58;
    
    public function actionView($url)
    {
        if($this->isAjax())
            $this->setLayout('layout_min');/**/

        if(!$url) {
            $page = $this->pagesModel->find($this->homeId[$this->ln])->fetch();
        } elseif($url == 'sitemap.xml') {
            $this->xml = TRUE;
            $page = $this->pagesModel->findByUrl('sitemap', TRUE)->fetch();
        /*} elseif(strstr($url, 'newsletter/confirm')) {
            $page = $this->pagesModel->findByUrl('confirm')->fetch();
        } elseif(strstr($url, 'newsletter/deactivate')) {
            $page = $this->pagesModel->findByUrl('deactivate')->fetch();/**/
        } else {
            $exp = explode('/', $url);

            $this->template->url = $exp[count($exp) - 1];
            
            $page = $this->pagesModel->findByUrl($exp[count($exp) - 1], TRUE)->fetch();
            if($page) {
                $page->url = $this->pagesModel->constructUrl($page);

                if($page->url != $url) {
                    $exp = explode("/", $page->url);
                    unset($exp[0]);
                    $t_url = implode("/", $exp);
                    if($t_url != $url) {
                        $this->redirect('Default:view', $page->url);/**/
                    }
                    
                }
            }
            
            if(!$page)    
                throw new BadRequestException('Stránka nenalezena.', 404);
        }

        if($page) {
            if($page->depth == 1 && !$url)
                $this->redirect('view', $page->url);
            
            if($page->perex == "redirect") {
                $subpage = $this->pagesModel->findChildren($page, TRUE)->orderBy('lft', 'ASC')->fetch();
                $subpage->url = $this->pagesModel->constructUrl($subpage);
                $this->redirect('view', $subpage->url);
            }

            $this->pagesModel->update($page->id, array('viewed' => $page->viewed+1));
            $this->setView($page->template);
            $this->page = $page;
            $this->template->page = $page;
            
            $parameters = $this->pagesModel->findParameters($page->id)->fetchAll();
            foreach($parameters as $parameter) {
                if(isset($parameter)) {
                    $page->{str_replace('-', '', String::webalize($parameter->title))} = $parameter->value;
                }
            }

            /*$partners = $this->pagesModel->find($this->partnersId)->fetch();
            $this->template->l_partners = $this->pagesModel->findChildren($partners, TRUE, TRUE)->fetchAll();/**/
            
            $blocks = $this->pagesModel->find($this->blocksId[$this->ln])->fetch();
            $result = $this->pagesModel->findChildren($blocks, TRUE, TRUE)->fetchAll();
            $this->template->block = new ArrayHash();
            foreach($result as $block) {
                $title_exp = explode("-", $block->url, 2);
                if(isset($title_exp[1])) {
                    $block_name = str_replace('-', '_', $title_exp[1]);
                    if(($title_exp[0] == $this->ln && !isset($this->template->block->$block_name)) || $title_exp[0] == $page->id)
                        $this->template->block->$block_name = $block;
                }
            }
            
            Debug::barDump($this->template->block, 'bloky');
            
            $youtube[] = array('video' => '<iframe width="262" height="147" src="http://www.youtube.com/embed/pJJ4eW1Ifks?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Adam Hazdra, MSc.');
            $youtube[] = array('video' => '<iframe width="262" height="147" src="http://www.youtube.com/embed/YBRwUujAm0A?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Mgr. Michal Hrabí');
            $youtube[] = array('video' => '<iframe width="262" height="147" src="http://www.youtube.com/embed/SkLFGAkris4?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Michal Vallo');

            switch($page->template) {
                case 'default':
                    unset($youtube);
                    $youtube[] = array('video' => '<iframe width="279" height="159" src="http://www.youtube.com/embed/pJJ4eW1Ifks?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Adam Hazdra, MSc.');
                    $youtube[] = array('video' => '<iframe width="279" height="159" src="http://www.youtube.com/embed/YBRwUujAm0A?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Mgr. Michal Hrabí');
                    $youtube[] = array('video' => '<iframe width="279" height="159" src="http://www.youtube.com/embed/SkLFGAkris4?rel=0" frameborder="0" allowfullscreen></iframe>', 'jmeno' => 'Michal Vallo');
                case 'about':
                    $this->template->kariera = $this->pagesModel->findChildren($this->pagesModel->find($this->karieraId)->fetch(), TRUE)->orderBy('published', 'DESC')->fetchAll();
                    break;
            }

            $video = $youtube[rand(0, count($youtube) - 1)];
            
            $testimonial = "<h2>{$this->template->tr['testimonial']}</h2>";
            $testimonial .= "<div class='bubble'>";
            $testimonial .= "{$video['video']}";
            $testimonial .= "</div>";
            $testimonial .= "**{$video['jmeno']}**";

            $this->template->block->testimonial->text = $testimonial;
            
        
        }
    
    }
    
    public function renderSitemap($url)
    {
        $this->template->page = $this->page;
        
        if($this->xml)
            $this->setView('xml');
        
        $pages = $this->pagesModel->findAll(TRUE)->where('sitemap = %i', 1)->orderBy('lft')->fetchAll();
        foreach($pages as $page) {
            $page->url = $this->pagesModel->constructUrl($page);
        }
        $this->template->pages = $pages;
    }
    
    protected function createComponentPaginator()
    {
        $visualPaginator = new VisualPaginator();
        $visualPaginator->paginator->itemsPerPage = 3;
        return $visualPaginator;
    }    

}
