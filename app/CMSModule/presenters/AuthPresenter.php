<?php

final class CMS_AuthPresenter extends CMS_BasePresenter
{

    /** @persistent */
    public $backlink = '';

    protected function createComponentLoginForm($name)
    {
        $form = new AppForm($this, $name);

        $form->addText('username', 'Email:')
             ->addRule(Form::FILLED, 'Prosím zadejte registrační email.');

        $form->addPassword('password', 'Heslo:')
             ->addRule(Form::FILLED, 'Prosím zadejte heslo.');

        $form->addProtection('Prosím odešlete přihlašovací údaje znova (vypršela platnost tzv. bezpečnostního tokenu).');
        $form->addSubmit('send', 'Přihlásit');

        $form->onSubmit[] = array($this, 'loginFormSubmitted');
    }

    public function loginFormSubmitted($form)
    {
        try {
            $user = Environment::getUser();
            $user->login($form['username']->value, $form['password']->value);
            $this->getApplication()->restoreRequest($this->backlink);
            $this->redirect('Default:default');
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

}
