<?php
  
class CMS_UzivatelPresenter extends CMS_SecuredPresenter
{
    
    /** @persistent */
    public $backlink = '';

    public $level;
    
    public function startup()
    {
        parent::startup();
        
        $this->level = $this['navigation']->add('Správa uživatele', $this->link('Uzivatel:default'), 0);
    }
    
    public function actionDefault()
    {
        $user = Environment::getUser();
        $userId = $user->identity->data['id'];
        
        $this->template->user = $this->usersModel->find($userId)->fetch();
        
        $this->template->absolvovane = $this->ucastiModel->findAll()->where('userId = %i', $userId)->and('casOd <= %i', time())->fetchAll();
        $this->template->objednane = $this->ucastiModel->findAll()->where('userId = %i', $userId)->and('casOd > %i', time())->fetchAll();
        
        $this['navigation']->setCurrent($this->level);
    }
    
    public function actionDetail()
    {
        $this->setView('edit');

        $user = Environment::getUser();

        $uzivatel = $this->usersModel->find($user->identity->data['id'])->fetch();
        
        $this->template->jmenoUzivatele = $uzivatel->prijmeni . " " . $uzivatel->jmeno;
        
        $form = $this['uzivatel'];
        $form['uzivatel']->setDefaults($uzivatel);
        
        $this->template->form = $form;
        
        $this->level = $this->level->add('Detail uživatele', $this->link('this'));
        $this['navigation']->setCurrent($this->level);
    }
        
    protected function createComponentUzivatel($name)
    {
        $user = Environment::getUser();
        $id = $user->identity->data['id'];
        
        $form = new AppForm($this, $name);
        
        $sub = $form->addContainer('uzivatel');
        $sub->addText('jmeno', 'Jméno: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte jméno uživatele.');
        $sub->addText('prijmeni', 'Příjmení: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte příjmení uživatele.');
        $sub->addText('email', 'Email: *', 50, 250)
            ->addRule(Form::FILLED, 'Vyplňte email.')
            ->addRule(Form::EMAIL, 'Zadejte e-mail ve validním tvaru.');

        if($id) {
            $sub->addCheckbox('changePass', 'Změnit heslo:')
                ->addCondition(Form::EQUAL, TRUE) // conditional rule: if is checkbox checked...
                ->toggle('password')
                ->toggle('password2');
            $sub->addPassword('heslo', 'Heslo: *', 20)
                ->addConditionOn($sub['changePass'], Form::FILLED)
                ->addRule(Form::FILLED, 'Zadejte heslo')
                ->addRule(Form::MIN_LENGTH, 'Heslo je příliš krátké, musí být aspoň %d znaků dlouhé', 5);

            $sub->addPassword('heslo2', 'Zopakovat heslo: *', 20)
                ->addConditionOn($sub['changePass'], Form::FILLED)
                ->addConditionOn($sub['heslo'], Form::VALID)
                    ->addRule(Form::FILLED, 'Zopakujte heslo')
                    ->addRule(Form::EQUAL, 'Hesla si neodpovídají', $sub['heslo']);
        } else {
            $sub->addPassword('heslo', 'Heslo: *', 20)
                ->addRule(Form::FILLED, 'Zadejte heslo')
                ->addRule(Form::MIN_LENGTH, 'Heslo je příliš krátké, musí být aspoň %d znaků dlouhé', 5);

            $sub->addPassword('heslo2', 'Zopakovat heslo: *', 20)
                ->addConditionOn($sub['heslo'], Form::VALID)
                    ->addRule(Form::FILLED, 'Zopakujte heslo')
                    ->addRule(Form::EQUAL, 'Hesla si neodpovídají', $sub['heslo']);
        }

        $sub->addText('mobil', 'Mobil', 50, 50);
        $sub->addText('vek', 'Věk *', 10, 10)
            ->addRule(Form::FILLED, 'Zadejte svůj věk.');
        $sub->addSelect('pohlavi', 'Pohlaví:', array('M' => 'Muž', 'F' => 'Žena'));
        $sub->addText('mesto', 'Město (Obec)  *', 50, 250)
            ->addRule(Form::FILLED, 'Zadejte město z kterého pocházíte.');
        $sub->addText('zamestnani', 'Obor zaměstnání nebo studia *', 50, 250)
            ->addRule(Form::FILLED, 'Zadejte obor vašeho zaměstnání eventuálně obor studia.');
        $sub->addTextArea('zkusenostbu', 'Zkušenosti s BU (co a kolik let) *')
            ->addRule(Form::FILLED, 'Dejte nám vědět o vašich zkušenostech s bojovým uměním.');
        $sub->addTextArea('zkusenostsb', 'Zkušenosti s sebeobranou (co a kolik let) *')
            ->addRule(Form::FILLED, 'Dejte nám vědět o vašich zkušenostech s sebeobranou.');

        
        $form->addSubmit('ulozit', 'Uložit změny')
            ->onClick[] = array($this, 'upravUzivatele');
        
        return $form;
    }
    
    public function upravUzivatele($button)
    {
        $form = $button->form;

        
        
        $user = Environment::getUser();
        $id = $user->identity->data['id'];
        
        $uzivatel = $form['uzivatel']->values;
        if(!$uzivatel['changePass'] && $id) {
            unset($uzivatel['heslo']);
        } else {
            $config = Environment::getConfig('security');
            $uzivatel['salt'] = $this->Random_Password();
            $uzivatel['heslo'] = hash_hmac('sha256', $uzivatel['heslo'] . $uzivatel['salt'] , $config->hmacKey);
            if($id)
                $this->flashMessage('Heslo uživatele bylo změněno', 'info');
        }
        unset($uzivatel['changePass']);
        unset($uzivatel['heslo2']);

        try {        
            $this->usersModel->update($id, $uzivatel);
        } catch (DuplicateKeyException $e) {
            $form['uzivatel']['email']->addError('Zvolte jiný email, zadaný již existuje');
            return;
        }

        $this->flashMessage('Uživatel byl upraven', 'info');
        $this->getApplication()->restoreRequest($this->backlink);
        $this->redirect('default');
    }
    
    public function actionDoporuceni($id)
    {
        if(!$id)
            $this->redirect('default');
        
        $user = Environment::getUser()->identity;
        
        $ucast = $this->ucastiModel->find($id)->fetch();
        
        if($user->data['id'] != $ucast->userId) {
            $this->flashMessage('Nemáte právo přidávat názor někoho jiného.');
            $this->redirect('default');
        }
            
        $this['doporuceni']['ucastId']->setValue($ucast->id);

        $this->level = $this->level->add('Zpětná vazba z kurzu', $this->link('this'));
        $this['navigation']->setCurrent($this->level);
    }
    
    protected function createComponentDoporuceni($name)
    {
        $form = new AppForm($this, $name);
        
        $form->addHidden('ucastId');
        $form->addTextArea('text', 'Váš názor:')
            ->addRule(Form::FILLED, 'Nemá smysl ukládat prázdný názor, prosím podělte se o něj s námi.');
            
        $form->addSubmit('odeslat', 'Odeslat')
            ->getControlPrototype()->class('submit');
        $form['odeslat']->onClick[] = callback($this, 'doporuceni');
    }
    
    public function doporuceni($button)
    {
        
        $form = $button->form;
        
        $values = $form->values;
        
        $values['casVlozeni'] = time();
        
        $this->doporuceniModel->insert($values);
        
        $this->flashMessage('Děkujeme za váš názor.');
        $this->redirect('default');
    }
    
    public function renderOdhlasit()
    {
        $user = Environment::getUser();
        
        $user->logout(TRUE);
        $this->getApplication()->restoreRequest($this->backlink);
        $this->redirect('default');
    }
    
}
