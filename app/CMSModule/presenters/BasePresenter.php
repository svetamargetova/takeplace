<?php

class CMS_BasePresenter extends BasePresenter
{

    public $level;

    public $clevel;

    public $mesice = array('1' => 'Leden', '2' => 'Únor', '3' => 'Březen', '4' => 'Duben', '5' => 'Květen', '6' => 'Červen', '7' => 'Červenec', '8' => 'Srpen', '9' => 'Září', '10' => 'Ŕíjen', '11' => 'Listopad', '12' => 'Prosinec');
    public $mesice_kratke = array('1' => 'Led', '2' => 'Úno', '3' => 'Bře', '4' => 'Dub', '5' => 'Kvě', '6' => 'Čer', '7' => 'Čvc', '8' => 'Srp', '9' => 'Zář', '10' => 'Ŕíj', '11' => 'Lis', '12' => 'Pro');
    
    public function startup()
    {
        parent::startup();

        $user = Environment::getUser();
        
        $this->template->loggedIn = FALSE;
        if($user->isLoggedIn()) {
            $this->template->logged = $user->identity->data;
            $this->template->loggedIn = TRUE;
        }
        
        $this->template->ln = $this->ln;
        
    }
    
    protected function beforeRender()
    {
        $texy = new FrontTexy;

        $this->template->backlink = $this->getApplication()->storeRequest();
        
        $this->template->registerHelper('texy', array($texy, 'process'));

        $this->template->mesice = $this->mesice;
        $this->template->mesice_k = $this->mesice_kratke;

        parent::beforeRender();
    }

    protected function createComponentNavigation($name)
    {
        $link = $this->link('this');
        
        $url = $this->getParam('url');
        
        $exp = explode("/", $url);
        
        $extra = array();
        $page = $this->pagesModel->findByUrl($exp[count($exp) - 1], TRUE)->fetch();
        
        $current = $this->link('Default:view', $url);
        
        $nav = new Navigation($this, $name);
        $navigation = $nav;
        
        $homepage = $this->pagesModel->findByUrl($this->ln)->fetch();
        
        $home = $nav->setupHomepage($homepage->title, $this->link("Default:view", $homepage->url));
        if($link === $home->url) $navigation->setCurrent($home);
        
        $pages = $this->pagesModel->findAll(TRUE)
            ->where('lft > %i', $homepage->lft)->and('rgt < %i', $homepage->rgt)->and('depth = %i', $homepage->depth + 1)->orderBy('lft')->fetchAll();

        $temp = $homepage->depth + 1;
        foreach($pages as $page) {
            if($page->depth > $temp) {
                $nav = $sec;
            } elseif($page->depth < $temp) {
                $nav = $sec->parent;
            }
            $temp = $page->depth;
            if(!strstr($page->url, 'http://'))
                $url = $this->pagesModel->constructUrl($page, false);
            $sec = $nav->add($page->seo_title, strstr($page->url, 'http://') ? $page->url : $this->link('Default:view', $url), $page->menu);
            if($current === $sec->url) {
                if(empty($extra)) {
                    $navigation->setCurrent($sec);
                } else {
                    foreach($extra as $page) {
                        $url .= "/".$page->url;
                        $sec = $sec->add($page->title, strstr($page->url, 'http://') ? $page->url : $this->link('Default:view', $url), 0);
                    }
                    $navigation->setCurrent($sec);
                }
            }
            unset($url);
        }
    }

    protected function createComponentNews($name)
    {
        $news = new NewsControl($this, $name);
    }
    
    protected function createComponentSlider($name)
    {
        $slider = new SliderControl($this, $name);
    }
    
    protected function createComponentContactForm($name)
    {
        $form = new ContactFormControl($this, $name);
    }
    
    protected function createComponentNewsletter($name)
    {
        $form = new AppForm($this, $name);
        
        $form->addText('email', 'Email')
            ->addRule(Form::FILLED, 'Fill you e-mail.')
            ->addRule(Form::EMAIL, 'E-mail has to be valid.')
            ->setEmptyValue('@');

        $noSpam = $form->addText('nospam', 'Fill in „nospam“')
            ->addRule(Form::FILLED, 'You are a spambot! If not, please contact us on info@mobera.eu')
            ->addRule(Form::EQUAL, 'You are a spambot! If not, please contact us on info@mobera.eu', 'nospam');

        $noSpam->getLabelPrototype()->class('nospam');
        $noSpam->getControlPrototype()->class('nospam');

        $form->addSubmit('save', 'Sign up')
            ->onClick[] = callback($this, 'newsletter');
    }
    
    public function newsletter($button)
    {
        $form = $button->form;
        
        $values = $form->values;
        unset($values['nospam']);
        $values['date'] = time();
        $values['hash'] = md5($values['email'] . $values['date']);
        $values['active'] = 0;
        
        try {
            $this->newslettersModel->insert($values);
            $this->reviewNewsletter($form, $values);
        } catch (DuplicateKeyException $e) {
            $form['email']->addError('This e-mail is already in our database, please type another.');
            return;
        }
        
        $this->flashMessage('You are almost signed-up. We sent you email with confirmation link.');
        $this->redirect('this');
    }
    
    public function reviewNewsletter($form, $values)
    {
        $texy = new MailTexy;
        
        $template = new FileTemplate;
        $template->registerHelper('texy', array($texy, 'process'));
        $template->registerFilter(new /*Nette\Templates\*/LatteFilter);
        foreach($values as $key => $value) {
            $template->$key = $value;
        }
        $template->presenter = $this->getPresenter();
        $template->setFile(dirname(__FILE__).'/../templates/Mails/newsletterSetup.latte');
        
        try {
            $mail = new Mail;
            
            $mail->setFrom('no-reply@mobera.eu');
                
            $mail->addTo($values['email']);
            $mail->setHtmlBody($template);                          

            try {
                $mail->send();
            } catch (InvalidStateException $e) {
                $form->addError("Chyba při odesílání e-mailu na mail %s", $values['email']);
            }
        } catch (InvalidArgumentException $e) {
            $form->addError("Mail %s není validní", $values['email']);
        }
        
    }

    public function odesliMail($button)
    {
        $form = $button->form;
        
        $values = $form->values;
        
        $template = new FileTemplate;
        $template->registerFilter(new /*Nette\Templates\*/LatteFilter);
        $template->name = $values['name'];
        $template->phone = $values['phone'];
        $template->predmet = $values['subject'];
        $template->vzkaz = $values['note'];
        $template->setFile(dirname(__FILE__).'/../templates/Mails/kontakt.phtml');
        
        try {
            $mail = new Mail;
            
            if($values['email'])
                $mail->setFrom($values['email'], $values['name']);
            else
                $mail->setFrom(Environment::getConfig('site')->mail);
                
            $mail->addTo(Environment::getConfig('site')->mail);
            $mail->setHtmlBody($template);                          

            try {
                $mail->send();
            } catch (InvalidStateException $e) {
                $form->addError("Chyba při odesílání e-mailu na mail %s", $values['email']);
            }
        } catch (InvalidArgumentException $e) {
            $form->addError("Mail %s není validní", $values['email']);
        }
        
        $this->flashMessage('Vzkaz byl úspěšně odeslán. Děkujeme', 'info');
        $this->redirect('this');
    }
    
}
