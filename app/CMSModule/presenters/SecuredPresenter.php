<?php

abstract class CMS_SecuredPresenter extends CMS_BasePresenter
{
    public function startup()
    {
        $user = Environment::getUser();

        if (!$user->isLoggedIn()) {
            if ($user->getLogoutReason() === User::INACTIVITY) {
                $this->flashMessage('Systém vás z důvodů neaktivity odhlásil. Prosím přihlašte se znovu.', 'warning');
            }

            $this->flashMessage('Pro vstup do zákaznické části se musíte přihlásit!', 'warning');
            $backlink = $this->getApplication()->storeRequest();
            $this->redirect('Auth:login', array('backlink' => $backlink));
        } else {
            $uzivatel = $this->usersModel->find($user->identity->data['id'])->fetch();
            if($uzivatel->blokace) {
                $this->flashMessage('Byl vám zakázán přístup.', 'warning');
                $user->logout(TRUE);
                $this->redirect('this');
            }
        }

        parent::startup();
    }

    protected function beforeRender()
    {
        $user = Environment::getUser();

        $this->template->isAdmin = $user->isInRole('admin');
        
        parent::beforeRender();
    }
}
