<?php
    
class SliderControl extends BaseControl
{
    public function render($images, $id = 'slider')
    {
        $texy = new FrontTexy;
        $this->template->registerHelper('texy', array($texy, 'process'));

        /*
        $pagesModel = new Cms_PageModel;
        foreach($images as $image) {
            $image->url = $pagesModel->constructUrl($image);
        }*/

        $this->template->images = $images;
        $this->template->id = $id;
        $this->template->render();
    }
}