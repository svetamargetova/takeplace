<?php
    
class ContactFormControl extends BaseControl
{
    public function render()
    {
        $texy = new FrontTexy;
        $this->template->registerHelper('texy', array($texy, 'process'));

        $this->template->form = $this['contactForm'];
        $this->template->render();
    }

    protected function createComponentContactForm($name)
    {
        $form = new AppForm($this, $name);
        $form->getElementPrototype()->class('contact-form');
        
        $form->addText('name', 'Jméno a příjmení', 30)
            ->addRule(Form::FILLED, 'Vyplňte své jméno.');
        $form->addText('email', 'E-mail', 30)
            ->addRule(Form::EMAIL, 'Zadaný e-mail není ve validním tvaru');
        $form->addTextArea('note', 'Váš dotaz', 36);
            
        $noSpam = $form->addText('nospam', 'Fill in „nospam“')
            ->addRule(Form::FILLED, 'You are a spambot! If not, please contact us on ' . Environment::getConfig('site')->mail)
            ->addRule(Form::EQUAL, 'You are a spambot! If not, please contact us on ' . Environment::getConfig('site')->mail, 'nospam');

        $noSpam->getLabelPrototype()->class('nospam');
        $noSpam->getControlPrototype()->class('nospam');

        $form->addSubmit('send', 'Odeslat')
            ->onClick[] = callback($this, 'send');
    }
    
    public function send($button)
    {
        $form = $button->form;
        
        $values = $form->values;
        
        $template = new FileTemplate;
        $template->registerFilter(new /*Nette\Templates\*/LatteFilter);
        $template->name = $values['name'];
        $template->phone = $values['phone'];
        $template->predmet = $values['subject'];
        $template->vzkaz = $values['note'];
        $template->setFile(dirname(__FILE__).'/kontakt.phtml');
        
        try {
            $mail = new Mail;
            
            if($values['email'])
                $mail->setFrom($values['email'], $values['name']);
            else
                $mail->setFrom(Environment::getConfig('site')->mail);
                
            $mail->addTo(Environment::getConfig('site')->mail);
            $mail->setHtmlBody($template);                          

            try {
                $mail->send();
            } catch (InvalidStateException $e) {
                $form->addError("Chyba při odesílání e-mailu na mail %s", $values['email']);
            }
        } catch (InvalidArgumentException $e) {
            $form->addError("Mail %s není validní", $values['email']);
        }
        
        $this->getPresenter()->flashMessage('Vzkaz byl úspěšně odeslán. Děkujeme', 'info');
        $this->redirect('this');
    }
    
}