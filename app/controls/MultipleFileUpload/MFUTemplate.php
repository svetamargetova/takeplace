<?php

class MFUTemplate extends FileTemplate {

	function  __construct() {
		parent::__construct();
		$this->onPrepareFilters[] = callback($this, "registerFilters");
	}

	function registerFilters() {
		$this->registerFilter(new LatteFilter());
	}

}