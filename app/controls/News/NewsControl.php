<?php
    
class NewsControl extends BaseControl
{
    public $mesice = array('1' => 'Leden', '2' => 'Únor', '3' => 'Březen', '4' => 'Duben', '5' => 'Květen', '6' => 'Červen', '7' => 'Červenec', '8' => 'Srpen', '9' => 'Září', '10' => 'Ŕíjen', '11' => 'Listopad', '12' => 'Prosinec');
    public $mesice_kratke = array('1' => 'Led', '2' => 'Úno', '3' => 'Bře', '4' => 'Dub', '5' => 'Kvě', '6' => 'Čer', '7' => 'Čvc', '8' => 'Srp', '9' => 'Zář', '10' => 'Ŕíj', '11' => 'Lis', '12' => 'Pro');

    public function render($news)
    {
        $texy = new FrontTexy;
        $this->template->registerHelper('texy', array($texy, 'process'));

        $this->template->mesic = $this->mesice_kratke;
        
        $pagesModel = new Cms_PageModel;
        $news = isset($news) ? $news->fetchAll() : array();
        /*
        foreach($news as $new) {
            $new->url = $pagesModel->constructUrl($new);
        }*/
        
        $parent = $pagesModel->findParent($news[0])->fetch();
        $parent->url = $pagesModel->constructUrl($parent);
        $this->template->parent = $parent;
        $this->template->news = $news;
        $this->template->render();
    }
}