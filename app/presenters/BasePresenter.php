<?php


abstract class BasePresenter extends Presenter
{
    public $ln;
    
    /** @var Cms_GalleryModel */
    protected $galleryM;

    /** @var Cms_NewsletterModel */
    protected $newsletterM;

    /** @var ClanekModel */
    protected $pageM;

    /** @var CMS_PhotoModel */
    protected $photoM;

    /** @var ProductModel */
    protected $productM;

    /** @var ParametrModel */
    protected $parameterM;

    /** @var PartnerModel */
    protected $partnerM;

    /** @var TemplateModel */
    protected $templateM;

    /** @var Cms_TranslationModel */
    protected $translationM;

    /** @var UserModel */
    protected $userM;

    /** @var Shop_AreaModel.php **/
    protected $s_areaM;
    
    /** @var Shop_CarrierModel.php **/
    protected $s_carrierM;
    
    /** @var Shop_CountryModel.php **/
    protected $s_countryM;
    
    /** @var Shop_CurrencyModel.php **/
    protected $s_currencyM;
    
    /** @var Shop_ManufacturerModel.php **/
    protected $s_manufacturerM;
    
    /** @var Shop_OrderModel.php **/
    protected $s_orderM;
    
    /** @var Shop_OrderPartModel.php **/
    protected $s_orderPartM;
    
    /** @var Shop_ParameterModel.php **/
    protected $s_parameterM;
    
    /** @var Shop_PaymentModel.php **/
    protected $s_paymentM;
    
    /** @var Shop_ProductModel.php **/
    protected $s_productM;
    
    /** @var Shop_ShippingModel.php **/
    protected $s_shippingM;
    
    /** @var Shop_UserModel.php **/
    protected $s_userM;
    
    public $pohlavi = array('' => 'Nezadáno', 'M' => 'Muž', 'F' => 'Žena');
    
    public $langs = array('en', 'cs');

    
    public function startup()
    {
        $ln = Environment::getSession('ln');
        if(!$ln->ln) {
            $detected = Environment::getHttpRequest()->detectLanguage($this->langs);
            $ln->ln = $detected ? $detected : $this->langs[0];
        }
        
        if($this->getParam('url')) {
            $exp = explode("/", $this->getParam('url'));
            Debug::barDump($exp[0]);
            switch($exp[0]) {
                case 'cs':
                case 'en':
                    $ln->ln = $exp[0];
                    break;
                default:
                    //$this->redirect(':CMS:Default:view', 'cs');
                    break;
            }
        }
        
        if($this->ln !== $ln->ln)
            $this->ln = $ln->ln;
        
        $this->template->site = Environment::getConfig('site')->name;
        $this->template->copyright = Environment::getConfig('site')->copyright;
        $this->template->description = Environment::getConfig('site')->description;
        $this->template->keywords = Environment::getConfig('site')->keywords;
            
        $this->template->langs = $this->langs;
        $this->template->tr = $this->translationsModel->findAll()->fetchPairs('key', $this->ln);
            
        parent::startup();
    }
    
    /*protected function createTemplate()
    {
        $template = parent::createTemplate();
        $template->setTranslator(/*\Nette\Environment::getService('Nette\ITranslator'));
        return $template;
    }*/

    protected function beforeRender()
    {
        parent::beforeRender();
        
        $this->template->pohlavi = $this->pohlavi;
    }

    public function afterRender()
    {
        if ($this->isAjax() && $this->hasFlashSession())
            $this->invalidateControl('flashes');
            
        parent::afterRender();
    }

    protected function createComponentJs($name) {
        $js = new JavaScriptLoader($this, $name);

        $baseUri = Environment::getVariable("baseUri");
        $js->tempUri = $baseUri . "webtemp";
        $js->tempPath = WWW_DIR . "/webtemp";
        $js->sourcePath = WWW_DIR . "/js";

        $filter = new VariablesFilter(array(
            // texyla
            "baseUri" => $baseUri,
            "texylaPreviewPath" => $this->link("Texyla:preview"),
            "texylaFilesPath" => $this->link("Texyla:listFiles"),
            "texylaFilesUploadPath" => $this->link("Texyla:upload"),
        ));

        $js->filters[] = array($filter, "apply");

        return $js;
    }

    protected function createComponentCss($name)
    {
        $css = new CssLoader($this, $name);

        // cesta na disku ke zdroji
        $css->sourcePath = WWW_DIR . "/css";

        // cesta na webu ke zdroji (kvůli absolutizaci cest v css souboru)
        $css->sourceUri = Environment::getVariable("baseUri") . "css";

        // cesta na webu k cílovému adresáři
        $css->tempUri = Environment::getVariable("baseUri") . "webtemp";

        // cesta na disku k cílovému adresáři
        $css->tempPath = WWW_DIR . "/webtemp";

        /*
        //doporučené (a logické) pořadí, nepřehazovat
        $css->filters[] = array(new OneLineCommentsFilter);
        $css->filters[] = array(new RemoveCommentsFilter);
        $css->filters[] = array(new MixinsFilter);
        $css->filters[] = array(new VariablesFilter);
        $css->filters[] = array(new NestingFilter);
        $css->filters[] = array(new CompressFilter);
        */

        return $css;
    }
        
    public function random_password($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $output = '';
        $possibles = strlen($chars)-1;

        for ($i=0;$i<$length;$i++) {
            $output .= $chars[mt_rand(0,$possibles)];
        }

        return $output;
    }

    public function getGalleriesModel()
    {
        if($this->galleryM === NULL) $this->galleryM = new CMS_GalleryModel();
        return $this->galleryM;
    }
    
    public function getNewslettersModel()
    {
        if($this->newsletterM === NULL) $this->newsletterM = new Cms_NewsletterModel();
        return $this->newsletterM;
    }
    
    public function getPagesModel()
    {
        if($this->pageM === NULL) $this->pageM = new Cms_PageModel();
        return $this->pageM;
    }
    
    public function getPhotosModel()
    {
        if($this->photoM === NULL) $this->photoM = new CMS_PhotoModel();
        return $this->photoM;
    }
    
    public function getProductsModel()
    {
        if($this->productM === NULL) $this->productM = new Cms_ProductModel();
        return $this->productM;
    }
    
    public function getParametersModel()
    {
        if($this->parameterM === NULL) $this->parameterM = new Cms_ParameterModel();
        return $this->parameterM;
    }
    
    public function getPartnersModel()
    {
        if($this->partnerM === NULL) $this->partnerM = new Cms_PartnerModel();
        return $this->partnerM;
    }
    
    public function getTemplatesModel()
    {
        if($this->templateM === NULL) $this->templateM = new Cms_TemplateModel();
        return $this->templateM;
    }
    
    public function getTranslationsModel()
    {
        if($this->translationM === NULL) $this->translationM = new Cms_TranslationModel();
        return $this->translationM;
    }
    
    public function getUsersModel()
    {
        if($this->userM === NULL) $this->userM = new Cms_UserModel();
        return $this->userM;
    }
    
    /**************************************************************************
    * E-shop models
    **************************************************************************/
    
    public function getSAreasModel()
    {
        if($this->s_areaM === NULL) $this->s_areaM = new Shop_AreaModel();
        return $this->s_areaM;
    }
    
    public function getSCarriersModel()
    {
        if($this->s_carrierM === NULL) $this->s_carrierM = new Shop_CarrierModel();
        return $this->s_carrierM;
    }
    
    public function getSCountriesModel()
    {
        if($this->s_countryM === NULL) $this->s_countryM = new Shop_CountryModel();
        return $this->s_countryM;
    }
    
    public function getSCurrenciesModel()
    {
        if($this->s_currencyM === NULL) $this->s_currencyM = new Shop_CurrencyModel();
        return $this->s_currencyM;
    }
    
    public function getSManufacturersModel()
    {
        if($this->s_manufacturerM === NULL) $this->s_manufacturerM = new Shop_ManufacturerModel();
        return $this->s_manufacturerM;
    }
    
    public function getSOrdersModel()
    {
        if($this->s_orderM === NULL) $this->s_orderM = new Shop_OrderModel();
        return $this->s_orderM;
    }
    
    public function getSOrderPartsModel()
    {
        if($this->s_orderPartM === NULL) $this->s_orderPartM = new Shop_OrderPartModel();
        return $this->s_orderM;
    }
    
    public function getSParametersModel()
    {
        if($this->s_parameterM === NULL) $this->s_parameterM = new Shop_ParameterModel();
        return $this->s_parameterM;
    }
    
    public function getSPaymentsModel()
    {
        if($this->s_paymentM === NULL) $this->s_paymentM = new Shop_PaymentModel();
        return $this->s_paymentM;
    }
    
    public function getSProductsModel()
    {
        if($this->s_productM === NULL) $this->s_productM = new Shop_ProductModel();
        return $this->s_productM;
    }
    
    public function getSShippingsModel()
    {
        if($this->s_shippingM === NULL) $this->s_shippingM = new Shop_ShippingModel();
        return $this->s_shippingM;
    }
    
    public function getSUsersModel()
    {
        if($this->s_userM === NULL) $this->s_userM = new Shop_();
        return $this->s_userM;
    }
    
}
