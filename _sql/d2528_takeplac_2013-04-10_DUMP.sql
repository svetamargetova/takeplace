-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cms_gallery`;
CREATE TABLE `cms_gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_cs` varchar(255) DEFAULT NULL,
  `url_cs` varchar(255) DEFAULT NULL,
  `perex_cs` text,
  `text_cs` text,
  `active_cs` tinyint(3) unsigned DEFAULT NULL,
  `created` int(10) unsigned DEFAULT NULL,
  `published` int(10) unsigned DEFAULT NULL,
  `userId` int(10) unsigned DEFAULT NULL,
  `photo` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_cs` (`url_cs`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `depth` int(10) unsigned NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rgt` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `perex` text NOT NULL,
  `text` text NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  `published` int(10) unsigned NOT NULL DEFAULT '0',
  `viewed` int(10) unsigned NOT NULL DEFAULT '0',
  `author` int(10) unsigned NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(255) NOT NULL DEFAULT 'view',
  `menu` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `submenu` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sitemap` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `system` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_cs` (`url`),
  KEY `autor` (`author`),
  KEY `lft` (`lft`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_page` (`id`, `depth`, `lft`, `rgt`, `title`, `url`, `perex`, `text`, `active`, `created`, `published`, `viewed`, `author`, `image`, `template`, `menu`, `submenu`, `sitemap`, `system`, `comment`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1,	1,	1,	58,	'Home',	'en',	'',	'',	1,	1298808862,	1298808660,	22979,	1,	'',	'default',	1,	0,	1,	0,	'',	'Organising Events',	'',	''),
(2,	2,	2,	5,	'Takeplace features',	'features',	'',	'.[links clearfix]\r\n* \"Academic institutions\":[article:4#academic-institutions]\r\n* \"Enterprise & commerce\":[article:4#enterprise-commerce]\r\n* \"Public service\":[article:4#public-service]\r\n\r\n/--div .[clearfix]\r\n/--div .[management box clearfix]\r\n== Effortless management ==\r\n\r\n* No installation, world-wide access\r\n* Easy event setup & maintenance\r\n* Adjustable organizing workflow\r\n* Full life cycle support\r\n* Online teamwork environment\r\n* Multi-track event platform\r\n\r\n\\--\r\n\r\n/--div .[participants box clearfix]\r\n== Administering participants ==\r\n\r\n* Addressing & registering attendees\r\n* Flexible registration routines\r\n* Ticket sale and validation\r\n* Modular pricing with discounts\r\n* Online payments and invoicing\r\n* Accommodation management .[grey]\r\n\\--\r\n\\--\r\n\r\n/--div .[clearfix]\r\n/--div .[contributions box clearfix]\r\n== Reviewing contributions ==\r\n\r\n* Collecting contributions\r\n* Adjustable multi-stage revision\r\n* Bidding for review\r\n* Balanced peer review assignment\r\n* Resolving conflicts of interest\r\n* Revealed or anonymous reviewing\r\n\\--\r\n\r\n/--div .[scheduling box clearfix]\r\n== Scheduling programme ==\r\n\r\n* Inviting speakers\r\n* Selecting / rejecting contributions\r\n* Setting up rooms and stages\r\n* Administering parallel tracks\r\n* Instant programme publishing\r\n* Typesetting printed materials .[grey]\r\n\\--\r\n\\--\r\n== ADDITIONAL FEATURES == .[feature-h2]\r\n\r\n/--div .[additional]\r\n/--div .[control box clearfix]\r\n=== Executive control ===\r\n\r\n* Nominating committees\r\n* Assigning tasks & rights\r\n* Intuitive event progress monitoring\r\n* Effective communication support\r\n* Distributing notifications .[grey]\r\n* Daily agenda and global reporting .[grey]\r\n\\--\r\n\r\n/--div .[security box clearfix]\r\n=== Credibility & security ===\r\n\r\n* Transparent and adjustable privacy\r\n* Role-based event control\r\n* Control consistency\r\n* Running on dedicated servers\r\n* Advanced import & export\r\n* Accepting secure payments\r\n\\--\r\n\r\n/--div .[event box clearfix]\r\n=== B2B meetings ===\r\n\r\n* Intuitive request & offer system\r\n* Associate partners’ profile control\r\n* Bidding for cooperation\r\n* Preference lists with personal notes\r\n* Automated meeting scheduling\r\n* Smart text/voice notifications  .[grey]\r\n\\--\r\n\r\n/--div .[community box clearfix]\r\n=== Community ===\r\n\r\n* Maintaining career profiles\r\n* Distributing invitations\r\n* Developing virtual expert communities\r\n* Sharing documents and media .[grey]\r\n* Collecting feedback and references .[grey]\r\n* Multilingual support .[grey]\r\n\r\n\\--\r\n\\--',	1,	1345756175,	1298808840,	1441,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Features',	'',	''),
(4,	2,	6,	7,	'Overview',	'overview',	'',	'.[tabs]\r\n* \"Academic institutions .{rel:academic-institutions}\":#academic-institutions\r\n* \"Enterprise & commerce .{rel:enterprise-commerce}\":#enterprise-commerce\r\n* \"Public service .{rel:public-service}\":#public-service\r\n\r\n/--div .[tab_container]\r\n/--div .[tab_content #academic-institutions]\r\nSteering conferences or running symposia is a full-time job for every responsible organizer. Takeplace takes the pain away and puts the fun back, saving time, money and most importantly&nbsp;—&nbsp;organizers’ sanity. Takeplace allows to focus on what is truly important: people,&nbsp;not&nbsp;the administration. .[heading]\r\n\r\n----------------------\r\n\r\n== Academic conferences ==\r\n\r\nConference organizers may easily assign committees, collect submissions and reviews, schedule talks within a multi-track programme and publish instant web timetables and printed proceedings. Steering events has never been easier — setting up the whole conference is a matter of few clicks in a user friendly interface.\r\n\r\n== Lectures and seminars ==\r\n\r\nTeachers will appreciate preparing educational seminars online, sharing lecture notes, distributing assignments and collecting students’ feedback. Takeplace holistically brings together efficiency, collaboration and social and educational networking. It conveys the next generation feeling of event planning and successful education management.\r\n\r\n== Professional cooperation ==\r\n\r\nWe are especially proud of actually making respected event organizers collaborate with us and help us to develop Takeplace. The result is a state-of-the-art conference management tool made by insiders, for insiders.\r\n\\--\r\n\r\n/--div .[tab_content #enterprise-commerce]\r\nWhen attempting to grow your business, why don’t you try hosting an event? Invite people to a professional presentation of your products! Are you doubtful you won’t be able to manage it? With Takeplace, of course you will! And it does not matter if you are a market leading player or an innovative startup company. .[heading]\r\n\r\n------------------------\r\n\r\n== Truffle oil importer? ==\r\n\r\nHost the first workshop on truffle oil in your region. Invite other importers, foreign exporters and members of the chamber of commerce, with different registration fees for each. Collect contributions, generate schedules, share knowledge with other participants — everything in a simple and problem-proof interface. Then repeat the workshop every six months, with decreasing effort and increasing impact.\r\n\r\n== Ambitious winemaker? ==\r\n\r\nInvite the sommeliers of the country’s best known restaurants for a wine tasting seminar. Allow them to check for accommodation and register for selected degustation programme online, seamlessly share negotiation notes via phones while holding a glass of merlot in the other hand, seeding a long-term business relationships with a *grâce*.\r\n\r\n== Experienced chef? ==\r\n\r\nLaunch a culinary school for gourmands! You can use Takeplace for simple registration and payments — or for sharing recipes and guides to local foodmakers. After each lesson you can send the participants a thank-you note and post a fresh photo gallery to Takeplace!\r\n\\--\r\n\r\n/--div .[tab_content #public-service]\r\nOrganizing events may be tricky when working for a government — groove agenda and tight budgets often prevent organizers from purchasing comprehensive and integrated event management tools. Luckily, Takeplace is easy to use, fully equipped and above all affordable, sparing your office hardware and software expenses. .[heading]\r\n\r\n---------------------------\r\n\r\n== Council meetings ==\r\n\r\nCouncil house meetings may be arranged more efficiently than any time before. Anyone involved can join with a single click the proper discussion panel according to the topic of interest. Administrative and statute resources may be shared even though officers don’t run the same operating system or office software suite!\r\n\r\n== Government symposia ==\r\n\r\nOrganizers of symposia may finally have the contributions reviewed on time and published in proceedings and slide hand-outs with professional typesetting and design, so that no one will believe a single cent wasn’t spent on graphic designers.\r\n\r\n== Educational seminars ==\r\n\r\nOrganizing seminars will gain from the dead-simple Takeplace interface, running in any web browser as well as in several cell phones, which makes the service friendly even to rookie users. And the unique Takeplace features will come handy in many more cases.\r\n\\--\r\n\\--\r\n\r\n----------------------\r\n\r\n== Consult us. We’ll take care of your events. ==\r\n\r\nTell us what kind of event you organize. We will assist you in finding the right solution.\r\n\r\nask@takeplace.eu\r\n\r\n&nbsp;\r\n\r\n\"See more Takeplace features » .[more]\":[article:2]',	1,	1345641550,	1298808900,	2204,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Overview',	'',	''),
(5,	2,	8,	9,	'References',	'customers',	'',	'== Clients & events ==\r\n\r\n\"[* logo_mu.png .(MUNI) *]\":http://www.muni.cz\r\n\"[* logo_vut.png .(VUT Brno) *]\":http://www.vutbr.cz\r\n\"[* logo_mic.png .(MIC) *]\":http://www.msic.cz/\r\n[* loga/tedxBratislava.jpg *]\r\n[* loga/ale2012.jpg *]\r\n[* loga/piknik.jpg *]\r\n[* loga/match2nmp.jpg *]\r\n[* loga/mali_BLP_logo1_master.jpg *]\r\n[* loga/konzulat_NY.jpg *]\r\n[* loga/smart-frame.jpg *]\r\n[* loga/ideaRS.jpg *]\r\n[* loga/mdevcamp.jpg *]\r\n[* loga/mobileDevCamp.jpg *]\r\n[* loga/jic.jpg *]\r\n[* loga/designwalks.jpg *]\r\n[* blc-ref.png *]\r\n[* ny-brno-days-ref.png *]\r\n[* techsquare-ref.png *]\r\n\r\nAll logos and trademarks are the property of their respective owners. .[grey]\r\n\r\n== Our achievements ==\r\n\r\n- Nápad roku 2010\r\n- Student podnikatel 2010\r\n- Cebit 2011\r\n- Cebit 2012\r\n- Seedcamp 2012\r\n- Qualcomm Venture Prize -QPrize 2012',	1,	1351593202,	1298809080,	1444,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'References',	'',	''),
(58,	1,	113,	116,	'Kariéra',	'kariera',	'',	'',	1,	1350941697,	1350941520,	0,	1,	'',	'kariera',	0,	0,	0,	0,	'',	'Kariéra',	'',	''),
(6,	2,	10,	13,	'Newsroom',	'newsroom',	'',	'/--div .[clearfix]\r\n/--div\r\n== Wrote about Takeplace ==\r\n\r\n* \"EKONOM magazine\":http://ekonom.ihned.cz/c1-44439000-hlavne-se-z-toho-nezblaznit\r\n No. 25/2010, June 24, page 22–23 .[czech]\r\n* \"Financial News\":http://www.financninoviny.cz/zajimavosti/zpravy/napadem-roku-2010-se-stal-portal-ukazjakbydlis-cz/479911&id_seznam\r\n Czech News Agency economical service, May 20, 2010 .[czech]\r\n* \"MUNI.cz\":http://info.muni.cz/index.php?option=com_content&task=view&id=1956&Itemid=89\r\n Masaryk University newspaper, September 2010, page 5 .[czech]\r\n* \"Interview for Czech TV\":http://www.youtube.com/watch?v=vJr82CEp5Ds&feature=plcp\r\n Události v regionech, July 19, 2012 .[czech]\r\n* \"Takeplace: Jeden z nejslibnějších českých startupů\":http://zpravy.e15.cz/byznys/technologie-a-media/takeplace-jeden-z-nejslibnejsich-ceskych-startupu-876006\r\n E15,  July 31.2012 .[czech]\r\n\\--\r\n\r\n/--div\r\n== Press releases ==\r\n\r\n* \"May 30, 2010\":[article:26] \"[* pdf.png .(PDF version) *]\":http://www.takeplace.eu/press/takeplace_press_20100530.pdf\r\n* \"Takeplace launches B2B Matchmaking\":takeplace-launched-b2b-matchmaking-recently.docx\r\n* \"Takeplace spouští B2B Matchmaking\":tz-takeplace-b2bmatchmaking-akademici.docx\r\n\r\n\\--\r\n\\--\r\n\r\n/--div\r\n== Publicity ==\r\n\r\n* **Takeplace Media Kit**  \"[* pdf.png .(TP flyer in PDF) *]\":mediakit-cz.pdf\r\n* Takeplace logo \"[* png.png .(TP logo in PNG) *]\":http://www.take-place.com/press/takeplace_logo.png \"[* pdf.png .(TP logo in PDF) *]\":http://www.take-place.com/press/takeplace_logo.pdf\r\n* Takeplace info flyer \"[* pdf.png .(TP flyer in PDF) *]\":http://www.take-place.com/press/takeplace_promo.pdf\r\n\\--',	1,	1352664519,	1298809080,	1024,	1,	'',	'press',	1,	0,	1,	0,	'',	'Newsroom',	'',	''),
(7,	2,	14,	15,	'Support',	'support',	'',	'',	0,	1298841769,	1298809140,	0,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Support',	'',	''),
(8,	2,	16,	17,	'About us',	'about-us',	'',	'== We enjoy organizing events. Enjoy it too ! ==\r\n\r\nWe’ve been supporting & promoting academic conferences and business meetings since 1993, going through a good deal of online event steering tools. We were always lacking something. Performance. Usability. Comfort. That’s why we made Takeplace and filled it with features we were missing. Share the joy of organizing with us !\r\n\r\n[* takeplace-core-team.png *]',	1,	1352846790,	1298809140,	1371,	1,	'',	'about',	1,	0,	1,	0,	'',	'About us',	'',	''),
(32,	1,	59,	110,	'Úvod',	'cs',	'',	'',	1,	1345638078,	1298808660,	4273,	1,	'',	'default',	1,	0,	1,	0,	'',	'Organising Events',	'',	''),
(33,	2,	62,	65,	'Vlastnosti',	'vlastnosti',	'',	'.[links clearfix]\r\n* \"Akademické instituce\":[article:34#academic-institutions]\r\n* \"Firmy & komerce\":[article:34#enterprise-commerce]\r\n* \"Veřejná správa\":[article:34#public-service]\r\n\r\n/--div .[clearfix]\r\n/--div .[management box clearfix]\r\n== Snadná správa ==\r\n\r\n* Bez instalace, přístup odkudkoliv\r\n* Jednoduché nastavení a správa akcí\r\n* Volitelné nastavení celé akce\r\n* Podpora celého životního cyklu akce\r\n* Denní agenda a celkový přehled\r\n* Podpora souběžných a navazujících akcí\r\n\r\n\\--\r\n\r\n/--div .[participants box clearfix]\r\n== Administrace účastníků ==\r\n\r\n* Oslovení a předregistrace účastníků\r\n* Flexibilní nastavení registrace\r\n* Online rezervace komponent programu\r\n* Modulární výpočet ceny a slev\r\n* Online platby a fakturace\r\n* Správa ubytování .[grey]\r\n\\--\r\n\\--\r\n\r\n/--div .[clearfix]\r\n/--div .[contributions box clearfix]\r\n== Recenzování příspěvků ==\r\n\r\n* Příjem příspěvků\r\n* Výzvy k recenzování\r\n* Rovnoměrné přiřazování recenzí\r\n* Řešení konfliktů zájmů\r\n* Nastavitelné vícestupňové revize\r\n* Veřejné nebo anonymní hodnocení\r\n\\--\r\n\r\n/--div .[scheduling box clearfix]\r\n== Rozvržení programu ==\r\n\r\n* Výběr zvaných přednášek\r\n* Výběr a zamítání příspěvků\r\n* Zajištění společenského programu\r\n* Tvorba konfigurovatelných rozvrhů\r\n* Správa paralelních přednášek a sekcí\r\n* Automatizovaná sazba sborníku abstraktů .[grey]\r\n\\--\r\n\\--\r\n\r\n== DALŠÍ VLASTNOSTI == .[feature-h2]\r\n\r\n/--div .[additional]\r\n/--div .[control box clearfix]\r\n=== Usnadnění řídicích procesů ===\r\n\r\n* Online prostředí týmové práce\r\n* Navrhování výborů\r\n* Přiřazování úkolů a práv\r\n* Podpora efektivní komunikace\r\n* Rozesílání upozornění\r\n* Intuitivní monitorování průběhu akce .[grey]\r\n\\--\r\n\r\n/--div .[security box clearfix]\r\n=== Spolehlivost a bezpečnost ===\r\n\r\n* Přístup podle přidělených rolí\r\n* Transparentně nastavitelné soukromí\r\n* Provoz na dedikovaných serverech\r\n* Jednotné ovládání\r\n* Bohaté možnosti importu a exportu\r\n* Bezpečný příjem plateb\r\n\\--\r\n\r\n/--div .[event box clearfix]\r\n=== Propagace akcí ===\r\n\r\n* Okamžitá úprava a publikování rozvrhu\r\n* Příprava jmenovek a seznamů účastníků\r\n* Tiskoviny a web v jednotném designu\r\n* Preference lists with personal notes\r\n* Pohodlná tvorba webu\r\n* Sazba plakátů a sborníků\r\n\\--\r\n\r\n/--div .[community box clearfix]\r\n=== Komunita===\r\n\r\n* Vytváření a správa uživatelských profilů\r\n* Vícejazyčná podpora\r\n* Diskuzní fóra a skupinové zprávy\r\n* Plánování schůzek\r\n* Sdílení dokumentů a fotografií .[grey]\r\n* Sbírání zpětné vazby a referencí .[grey]\r\n* Rozvoj sociální sítě\r\n\r\n\\--\r\n\\--',	1,	1350846717,	1298808840,	1123,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Vlastnosti',	'',	''),
(9,	2,	18,	57,	'Blocks',	'en-blocks',	'',	'',	1,	1298809749,	1298809680,	0,	1,	'',	'bloky',	0,	0,	0,	0,	'',	'Blocks',	'',	''),
(10,	3,	19,	46,	'Home blocks',	'1-blocks',	'',	'',	1,	1351270685,	1298811060,	0,	1,	'',	'bloky',	0,	0,	0,	0,	'',	'Home-block',	'',	''),
(11,	4,	20,	21,	'Banner',	'1-banner',	'home-banner',	'/--div\r\n<h1>Make your events run</h1>\r\n\r\nTakeplace brings comfort and joy into online management \r\n of professional events, supporting effective planning, \r\n steering and event promotion.\r\n\r\n\"TRY IT FOR FREE .[try-button]{ onclick:_gaq.push([\'_trackEvent\', \'Button tracking\', \'Try click\'])}\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp\r\n\\--',	1,	1351594596,	1298811060,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Banner',	'',	''),
(12,	4,	22,	23,	'Academic institutions',	'1-academic-institutions',	'home-case first',	'== \"Academic institutions\":[article:4#academic-institutions] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Organizing academic conferences<br/>&nbsp;&bull;&nbsp;&nbsp;Steering scientific congresses<br/>&nbsp;&bull;&nbsp;&nbsp;Preparing lectures and seminars\":[article:4#academic-institutions]\r\n',	1,	1345628475,	1298811240,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Academic institutions',	'',	''),
(13,	4,	24,	25,	'Enterprise & commerce',	'1-enterprise-commerce',	'home-case',	'== \"Enterprise & commerce\":[article:4#enterprise-commerce] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Initiating commercial workshops<br />&nbsp;&bull;&nbsp;&nbsp;Arranging company team-buildings<br />&nbsp;&bull;&nbsp;&nbsp;Managing exhibitions and trade fairs\":[article:4#enterprise-commerce]',	1,	1345628490,	1298811360,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Enterprise & commerce',	'',	''),
(14,	4,	26,	27,	'Public service',	'1-public-service',	'home-case',	'== \"Public service\":[article:4#public-service] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Coordinating educational seminars<br/>&nbsp;&bull;&nbsp;&nbsp;Supervising council house meetings<br/>&nbsp;&bull;&nbsp;&nbsp;Orchestrating government symposia\":[article:4#public-service]',	1,	1345628507,	1298811720,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Public service',	'',	''),
(15,	4,	28,	29,	'Enjoy Takeplace',	'1-enjoy-takeplace',	'home-box flag green first',	'== Enjoy Takeplace ==\r\n\r\nDo you need help with your events?\r\n\r\n\"Contact us » .[more]\":[article:8]\r\n',	1,	1349862557,	1298811840,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Enjoy Takeplace',	'',	''),
(16,	4,	30,	31,	'Awards we received',	'1-awards-we-received',	'home-box flag cyan',	'== Awards we received ==\r\n\r\n/--div .[clearfix]\r\n\"Institut rozvoje podnikání .[#ide]\":http://www.ide-vse.cz/home.html\r\n\"Unicredit Nápad roku .[#uni]\":http://www.napadroku.cz/aktualni-rocnik.html\r\n\\--\r\n\r\n\"Second place in category .[grey]\" Idea of the Year 2010\r\n \"Special award .[grey]\" Student Entrepreneur 2010',	1,	1345628553,	1298812020,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Awards we received',	'',	''),
(17,	4,	32,	33,	'Just start organizing',	'1-just-start-organizing',	'home-box flag blue login',	'== Just start organizing ==\r\n/--html\r\n<form method=\"post\" action=\"https://app.takeplace.eu/takeplace/secure_login\">\r\n<p>\r\n<label for=\"j_username\">Your e-mail</label>\r\n<input type=\"text\" name=\"j_username\" id=\"j_username\" class=\"placeholder\" title=\"Your e-mail\" />\r\n</p>\r\n<p>\r\n<label for=\"j_password\">Password</label>\r\n<input type=\"password\" name=\"j_password\" id=\"j_password\" class=\"placeholder\" title=\"Password\" />\r\n</p>\r\n<input type=\"hidden\" name=\"gwt\" value=\"false\" />\r\n<input type=\"submit\" value=\"Log in\" />\r\n</form>\r\n\\--\r\n\r\n\"Not registered yet?  Create your account » .[more]\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp',	1,	1349797784,	1298812680,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Just start organizing',	'',	''),
(18,	4,	34,	35,	'News from the blog',	'1-blog',	'home-box blog first',	'\"We blog too .[a-blog]\":http://blog.takeplace.eu/\r\n\r\n== News from the blog ==\r\n\r\n=== \"Takeplace - one of the best eight start-ups from Eastern Europe\":http://blog.takeplace.eu/takeplace-one-of-the-best-eight-start-ups-fro ===\r\n\r\n*October 18th, 2012*\r\n\r\nWe enrolled into QPrize during the summer and now we can celebrate the great success. What are we talking about? The QPrize is Qualcomm Venture\'s Seed investment competition ... \"Read more »\":http://blog.takeplace.eu/takeplace-one-of-the-best-eight-start-ups-fro',	1,	1352846602,	1298812800,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'News from the blog',	'',	''),
(19,	4,	36,	37,	'TP kinetic video',	'1-tp-video-tour',	'home-box video',	'\"[* video.png .(Video) *] .[videoLink]\":http://www.youtube.com/v/wHVgSL2Ca5c\r\n\r\n',	1,	1350847970,	1298813280,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'TP video tour',	'',	''),
(20,	4,	38,	39,	'Takeplace users say',	'1-testimonial',	'home-box testimonial',	'== Takeplace users say ==\r\n\r\n/--div .[bubble]\r\nTakeplace has been developed to convey the best experience to event providers, coordinators and participants. It is the ultimate time & cost-saving tool for every agile organizer. Share the joy of organizing with us!\r\n\\--\r\n**The Takeplace team**\r\n\r\n\"View more references » .[more right]\":[article:5]',	1,	1345628645,	1298813460,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Takeplace users say',	'',	''),
(21,	3,	47,	48,	'Layout-Testimonial',	'en-testimonial',	'',	'== Takeplace users say ==\r\n\r\n/--div .[bubble]\r\nTakeplace has been developed to convey the best experience to event providers, coordinators and participants. It is the ultimate time & cost-saving tool for every agile organizer. Share the joy of organizing with us!\r\n\\--\r\n**The Takeplace team**\r\n\r\n\"View more references » .[more right]\":[article:5]',	1,	1345627412,	1298839380,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Testimonial',	'',	''),
(23,	3,	49,	50,	'Layout-Try',	'en-try',	'',	'/--div .[try clearfix]\r\n\"Try Takeplace .[try-button]{ onclick:_gaq.push([\'_trackEvent\', \'Button tracking\', \'Try click\'])}\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp .[link]\r\n\r\nEnjoy your free licence! .[desc]\r\n\\--',	1,	1351594580,	1298841180,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Try',	'',	''),
(24,	3,	51,	52,	'Layout-Login',	'en-login',	'',	'<form method=\"post\" action=\"https://app.takeplace.eu/takeplace/secure_login\" onsubmit=\"_gaq.push([\'_trackEvent\', \'Button tracking\', \'Log in click\']);\">\r\n<p>\r\n<label for=\"j_username\">Your e-mail</label>\r\n<input type=\"text\" name=\"j_username\" id=\"j_username\" />\r\n</p>\r\n<p>\r\n<label for=\"j_password\">Password</label>\r\n<input type=\"password\" name=\"j_password\" id=\"j_password\" />\r\n</p>\r\n<input type=\"hidden\" name=\"gwt\" value=\"false\" />\r\n<input type=\"submit\" value=\"Log in\" />\r\n</form>\r\n\r\n\r\n\"Not registered yet?  Create your account » .[more]\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp',	1,	1351594416,	1298841300,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Login',	'',	''),
(25,	3,	53,	54,	'Layout-Pricing',	'en-pricing',	'/--div .[price-title]\r\n== Easy pricing ==\r\n\r\nTransparent charges with no hidden fees. .[price-fee-desc]\r\n\\--\r\n\r\n/--div .[price-total]\r\n**$ 0** per event per&nbsp;year\r\n\\--',	'/--div .[price-ticket]\r\n== Ticket sale ==\r\n\r\n2.9 % of ticket +&nbsp;$&nbsp;0.99 per each ticket\r\n\\--\r\n\r\nMaximum charge of $ 9.95 per each ticket. .[price-desc]\r\n\r\n<div class=\"clearfix\"></div>\r\n\r\n-----------------------\r\n\r\n/--div .[price-fee card]\r\n3 % per transaction\r\n\\--\r\n\r\n/--div .[price-fee paypal]\r\n$ 0.30 + 3,4 % per transaction\r\n\\--\r\n\r\nCharge applies only when you make a sale. .[price-desc]',	1,	1349172224,	1298841840,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Pricing',	'',	''),
(26,	3,	11,	12,	'Služba Takeplace <br />je druhým podnikatelským nápadem roku 2010',	'sluzba-takeplace-je-druhym-podnikatelskym-napadem-roku-2010',	'Webová služba Takeplace společnosti ACEMCEE získala v květnu 2010 dvě prestižní ocenění — v soutěži podnikatelských záměrů UniCredit Bank Nápad roku 2010 se umístila na druhém místě a jednatel společnosti obdržel cenu Student–podnikatel 2010.',	'Služba Takeplace se zaměřuje na organizaci různých typů odborných akcí, k nimž poskytuje vyspělé online nástroje a software pro plánování, tvorbu programu, evidenci kontaktů, e-mailovou komunikaci, přípravu textů, zajištění ubytování, vyúčtování nebo platby.\r\n\r\nSoutěž Nápad roku organizuje Institut rozvoje podnikání (IDE) při Vysoké škole ekonomické v Praze. Do třetího ročníku soutěže se přihlásilo 93 profesionálních projektů, které hodnotila desetičlenná porota složená ze zkušených podnikatelů, manažerů, investorů i zástupců médií.\r\n\r\n„Osobně mě velice zaujal vítěz kategorie Student–podnikatel s projektem Takeplace, který podle mého názoru skrývá vysoký komerční potenciál,“ řekl porotce Vít Šubert, výkonný ředitel společnosti LG Electronics CZ a jeden z finalistů prestižní soutěže Manažer roku 2009. O úspěchu služby Takeplace ve vysoce konkurenčním prostředí nakonec rozhodnul její inovační potenciál, proveditelnost v praxi, kvalita zpracování, originalita a perspektiva dalšího růstu.\r\n\r\n„Druhé umístění v soutěži Nápad roku 2010 i diskuse s porotci nám potvrdily potřebu služby Takeplace na trhu. Zájem pořadatelů odborných akcí je o takovou službu opravdu značný. Obě získané ceny jsou pro nás velkou motivací do budoucna, a zároveň satisfakcí pro celý tým společnosti ACEMCEE,“ uvedl jednatel společnosti Jaroslav Škrabálek krátce po předání cen.\r\n\r\n== O službě Takeplace  |  www.takeplace.eu ==\r\n\r\nTakeplace je moderní webová služba společnosti ACEMCEE pro efektivní organizaci a prezentaci odborných akcí. Konference, semináře, školení, workshopy a veletrhy, ať již v komerční nebo akademické sféře, jsou často pořádány těžkopádně a za použití mnoha nekonzistentních nástrojů. Webová služba Takeplace je navržena na základě požadavků odborných kruhů a své uživatele provází celým procesem pořádání odborné akce, nebo účasti na ní. Pořadatel má k dispozici sadu vzájemně provázaných nástrojů pro komunikaci, organizaci a prezentaci. Vše v jednotném grafickém stylu, s návazností na minulé ročníky. Takeplace je velmi snadno přizpůsobitelný rozsahu akce i požadavkům pořadatele. Působnost služby s akcí nekončí. Takeplace dále udržuje databázi dokumentů a profesní komunitu uživatelů. Účastník akce získá ty správné informace v ten správný čas.\r\n\r\n== O společnosti ACEMCEE  |  www.acemcee.com ==\r\n\r\nInovační společnost ACEMCEE s mezinárodní působností se zaměřuje na pořádání odborných akcí a vývoj vlastních řešení pro moderní webové a mobilní platformy. Své služby v oblasti virtuální i fyzické organizace akcí nabízí prostřednictvím expertních spolupracovníků s dlouholetými zkušenostmi (od roku 1993) v akademické i komerční sféře. V květnu 2010 společnost ACEMCEE uspěla s webovou službou Takeplace v prestižní podnikatelské soutěži Nápad roku 2010, pořádané Institutem rozvoje podnikání, kde se umístila na druhém místě. Jednatel společnosti Jaroslav Škrabálek rovněž získal ocenění Student–podnikatel 2010. Společnost dynamicky roste na českém i mezinárodním trhu; mezi její zákazníky patří univerzity, vysoké školy a školicí a poradenské společnosti.\r\n\r\n== Kontakt ==\r\n\r\nACEMCEE, s. r. o.\r\n Bratří Křičků 12/14\r\n 621 00 Brno, CZ\r\n www.acemcee.com\r\n info@acemcee.com',	1,	1298989680,	1275214560,	277,	1,	'',	'article',	0,	0,	1,	0,	'',	'Služba Takeplace je druhým podnikatelským nápadem roku 2010',	'',	''),
(27,	3,	55,	56,	'Layout-Contact',	'en-contact',	'',	'/--div .[email]\r\n=== E-mail: ===\r\n\r\ninfo@acemcee.com\r\n\\--\r\n\r\n/--div .[address]\r\n=== Address: ===\r\n\r\nACEMCEE, s. r. o.\r\n www.acemcee.com\r\n\r\nU Vodárny 3032/2a\r\n 616 00 Brno\r\n Czech Republic\r\n\\--\r\n\r\n/--div .[phone]\r\n=== Phone: ===\r\n\r\n+420 602 831 830\r\n\\--',	1,	1345627447,	1298895240,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'CONTACT INFORMATION',	'',	''),
(28,	1,	111,	112,	'Sitemap',	'sitemap',	'',	'',	1,	1298989364,	1298989320,	36,	1,	'',	'sitemap',	0,	0,	0,	0,	'',	'Sitemap',	'',	''),
(29,	3,	3,	4,	'Takeplace mobile',	'mobile',	'',	'Takeplace mobile helps you to be in touch with your event whenever and wherever you are.\r\n\r\nCheck your schedule, create your favorite one, list through participants and just enjoy the event you are attending.\r\n\r\nNo more queues during the registration. Show your ticket on the screen of your iPhone and let organizers do the check-in easily with QR codes scanning.\r\n\r\nVisit \"Takeplace.eu\":http://takeplace.eu and make your events run!\r\n\r\n== Screenshots ==\r\n\r\n.[clearfix]\r\n[* takeplace-mobile-2.png .(Event overview)[screenshot] *]\r\n[* takeplace-mobile-1.png .(Ticket)[screenshot] *]\r\n[* takeplace-mobile-4.png .(Participants)[screenshot] *]\r\n[* takeplace-mobile-3.png .(Schedule)[screenshot] *]\r\n[* takeplace-mobile-5.png .(About event)[screenshot] *]\r\n',	1,	1313694413,	1313568540,	81,	1,	'',	'about',	0,	0,	1,	0,	'',	'Takeplace mobile',	'',	''),
(30,	4,	40,	41,	'QEM banner',	'2-qem',	'',	'\"[* qem.png .(Quality event management) *]\":http://qem-2012.takeplace.eu',	1,	1345627383,	1329216900,	2,	1,	'',	'blok',	0,	0,	1,	0,	'',	'QEM banner',	'',	''),
(31,	4,	42,	43,	'Live coverage',	'1-live-coverage',	'',	'<iframe src=\"http://www.coveritlive.com/index2.php/option=com_altcaster/task=viewaltcast/altcast_code=b2cc8c73b2/height=350/width=620\" scrolling=\"no\" height=\"380px\" width=\"620px\" frameBorder=\"0\" allowTransparency=\"true\"><a href=\"http://www.coveritlive.com/mobile.php/option=com_mobile/task=viewaltcast/altcast_code=b2cc8c73b2\" >Quality Event Management</a></iframe>',	1,	1345627393,	1330499940,	2,	1,	'',	'blok',	0,	0,	1,	0,	'',	'Live coverage',	'',	''),
(34,	2,	66,	67,	'Přehled',	'prehled',	'',	'.[tabs]\r\n* \"Akademické instituce .{rel:academic-institutions}\":#academic-institutions\r\n* \"Firmy & komerce .{rel:enterprise-commerce}\":#enterprise-commerce\r\n* \"Veřejná správa .{rel:public-service}\":#public-service\r\n\r\n/--div .[tab_container]\r\n/--div .[tab_content #academic-institutions]\r\nŘízení konferencí nebo symposií je práce na plný úvazek pro každého zodpovědného organizátora. Takeplace organizátorovi šetří čas, peníze a ulehčuje celou administrativní práci kolem akce. Vrací radost z organizování akce a nechává předsedovi potřebný nadhled. Takeplace se umožňuje soustředit na to, co je skutečně důležité: účastníky akce.. .[heading]\r\n\r\n----------------------\r\n\r\n== Akademické konference ==\r\nOrganizátor konferencí může jednoduše tvořit výbory, shromažďovat příspěvky a recenze, plánovat prezentace a přednášky v programu vlastní konference i doprovodných akcí. Vlastní publikování akce je otázka několika kliků myší. Organizování nebylo nikdy tak jednoduché.\r\n\r\n== Školení a semináře ==\r\nVyučující ocení jednoduchou přípravu školících seminářů online, zadávání úloh a projektů, sbírání zpětné vazby.  Takeplace je sjednocení efektivity, spolupráce i sociální a vzdělávací provázanosti. Vyjadřuje cit pro plánování akcí a úspěšného vedení vzdělání budoucnosti.\r\n\r\n== Osobní spolupráce ==\r\nJsme velice vděčni za spolupráci respektovaných organizátorů konferencí, kteří nám pomáhali vytvářet Takeplace. Výsledkem je vskutku hodnotné dílo pro pořádání akcí navrhnuto organizátory pro organizátory.\r\n\\--\r\n\r\n/--div .[tab_content #enterprise-commerce]\r\nJestliže se pokoušíte rozjet byznys, proč nezkusíte uspořádat nějakou akci? Pozvěte potenciální klientyna profesionální prezentaci Vaší firmy! Máte obavy, že to nezvládnete? S Takeplace určitě ano! A nezáleží jestli je Vaše společnost leader na trhu nebo začínající firma. .[heading]\r\n\r\n------------------------\r\n\r\n== Dovozce lanýžového oleje? ==\r\nUspořádejte první workshop dovozců lanýžového oleje ve Vašem regionu. Pozvěte další dovozce, zahraniční exportéry a členy hospodářské komory. Pro každého si zvolte rozdílné ceny vstupného. Posbírejte a vyhodnoťte nejlepší příspěvky, vytvořte program, podělte se o svoje zkušenosti s ostatními účastníky – všechno je to velice snadné a účinné. Opakujte workshop každého půl roku s menší námahou a se zvyšujícímziskem a přínosem.\r\n\r\n== Ambiciózní vinař? ==\r\nPozvěte sommeliery z různých prestižních restaurací na testovací seminář. Umožněte jim si jednoduše rezervovat ubytování v hotelu a registrovat se na příslušný degustační program. To vše online. Jednoduše sdílejte poznámky přes telefon a u toho si vychutnávejte sklenku Merlota. Vytvářejte dlouhodobé obchodní vztahy s grácií.\r\n\r\n== Zkušený šéfkuchař? ==\r\nOtevřete kulinářskou školu pro gurmány! Takeplace můžete využít pro jednoduchou registraci a platbu – nebo pro sdílení receptů s místními kuchaři. Po každé lekci můžete účastníkům poslat děkovací email s odkazem do fotogalerie akce.\r\n\\--\r\n\r\n/--div .[tab_content #public-service]\r\nOrganizing events may be tricky when working for a government — groove agenda and tight budgets often prevent organizers from purchasing comprehensive and integrated event management tools. Luckily, Takeplace is easy to use, fully equipped and above all affordable, sparing your office hardware and software expenses. .[heading]\r\n\r\n---------------------------\r\n\r\n== Vzdělávací semináře ==\r\nOrganizace seminářů získá mnohé z jednoduchosti Takeplace. Může operovat na jakémkoli webovém vyhledavači i na většině telefonech a smartphonech. To dělá službu jednoduchou i pro začátečníka. Jedinečné funkce Takeplace přijdou vhod v mnoha rozlišných situacích.\r\n\r\n== Zasedání rady ==\r\nZasedání místní rady může být mnohem lépe zorganizováno než dosud. Každý účastník se může zapojit pouhým kliknutím do diskuse na téma jeho zájmu. Administrativní a statutární zdroje mohou být jednoduše sdíleny mezi referenty bez použití dalších kancelářských nástrojů.\r\n\r\n== Parlamentní symposia ==\r\nOrganizátoři symposií mají konečně možnost ohodnotit příspěvky včas. Publikovat zápisy, prezentace a doplňkové materiály v profesionálním dizajnu a tisku, bez jediného haléře utraceného za grafického designéra. \r\n\r\n\\--\r\n\\--\r\n\r\n----------------------\r\n\r\n== Konzultujte s námi. Chceme se postarat o chod Vaší akce. ==\r\nSdělte nám jaký druh akce pořádáte. Pomůžeme Vám najít to správné řešení.\r\n\r\nask@takeplace.eu\r\n\r\n&nbsp;\r\n\r\n\"Přečtěte si vlastnosti Takeplace » .[more]\":[article:33]',	1,	1350942409,	1298808900,	998,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Přehled',	'',	''),
(35,	2,	68,	69,	'Reference',	'reference',	'',	'== Zákazníci ==\r\n\r\n\"[* logo_mu.png .(MUNI) *]\":http://www.muni.cz\r\n\"[* logo_vut.png .(VUT Brno) *]\":http://www.vutbr.cz\r\n\"[* logo_mic.png .(MIC) *]\":http://www.msic.cz/\r\n[* loga/tedxBratislava.jpg *]\r\n[* loga/ale2012.jpg *]\r\n[* loga/piknik.jpg *]\r\n[* loga/match2nmp.jpg *]\r\n[* loga/mali_BLP_logo1_master.jpg *]\r\n[* loga/konzulat_NY.jpg *]\r\n[* loga/smart-frame.jpg *]\r\n[* loga/ideaRS.jpg *]\r\n[* loga/mdevcamp.jpg *]\r\n[* loga/mobileDevCamp.jpg *]\r\n[* loga/jic.jpg *]\r\n[* loga/designwalks.jpg *]\r\n[* blc-ref.png *]\r\n[* ny-brno-days-ref.png *]\r\n[* techsquare-ref.png *]\r\n\r\nAll logos and trademarks are the property of their respective owners. .[grey]\r\n\r\n== Čeho jsme dosáhli ==\r\n\r\n- Nápad roku 2010\r\n- Student podnikatel 2010\r\n- Cebit 2011\r\n- Cebit 2012\r\n- Seedcamp 2012\r\n- Qualcomm Venture Prize -QPrize 2012',	1,	1351593212,	1298809080,	763,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Reference',	'',	''),
(36,	2,	70,	71,	'Newsroom',	'newsroom_c',	'',	'/--div .[clearfix]\r\n/--div\r\n== Napsali o Takeplace ==\r\n\r\n* \"Týdeník EKONOM\":http://ekonom.ihned.cz/c1-44439000-hlavne-se-z-toho-nezblaznit\r\n Č. 25/2010, 24. červen, strana 22–23 .[czech]\r\n* \"Finanční noviny\":http://www.financninoviny.cz/zajimavosti/zpravy/napadem-roku-2010-se-stal-portal-ukazjakbydlis-cz/479911&id_seznam\r\n Ekonomické zpravodajství ČTK, 20. květen 2010 .[czech]\r\n* \"MUNI.cz\":http://info.muni.cz/index.php?option=com_content&task=view&id=1956&Itemid=89\r\n Měsíčník Masarykovy Univerzity, září 2010, strana 5 .[czech]\r\n* \"Reportáž v ČT\":http://www.youtube.com/watch?v=vJr82CEp5Ds&feature=plcp\r\n Události v regionech, srpen 2012 .[czech]\r\n* \"Takeplace: Jeden z nejslibnějších českých startupů\":http://zpravy.e15.cz/byznys/technologie-a-media/takeplace-jeden-z-nejslibnejsich-ceskych-startupu-876006\r\n E15,  Červenec 31.2012 .[czech]\r\n\\--\r\n\r\n/--div\r\n== Tiskové zprávy ==\r\n\r\n* \"May 30, 2010\":[article:26] \"[* pdf.png .(PDF version) *]\":http://www.takeplace.eu/press/takeplace_press_20100530.pdf\r\n* \"Takeplace launches B2B Matchmaking\":takeplace-launched-b2b-matchmaking-recently.docx\r\n* \"Takeplace spouští B2B Matchmaking\":tz-takeplace-b2bmatchmaking-akademici.docx\r\n\r\n\\--\r\n\\--\r\n\r\n/--div\r\n== Propagace==\r\n\r\n* **Takeplace Media Kit**  \"[* pdf.png .(TP flyer in PDF) *]\":mediakit-cz.pdf\r\n* Takeplace logo \"[* png.png .(TP logo in PNG) *]\":http://www.takeplace.eu/press/takeplace_logo.png \"[* pdf.png .(TP logo in PDF) *]\":http://www.takeplace.eu/press/takeplace_logo.pdf\r\n* Takeplace info flyer \"[* pdf.png .(TP flyer in PDF) *]\":http://www.takeplace.eu/press/takeplace_promo.pdf\r\n\\--',	1,	1352664508,	1298809080,	586,	1,	'',	'press',	1,	0,	1,	0,	'',	'Newsroom',	'',	''),
(37,	2,	74,	75,	'O nás',	'o-nas',	'',	'== Organizování nás baví. Pořádejte akce s námi ! ==\r\n\r\nOrganizujeme akademické konference a pořádáme obchodní setkání již od roku 1993 a za tu dobu jsme vyzkoušeli celou řadu online nástrojů pro jejich správu. Vždy nám při tom něco chybělo. Funkčnost. Použitelnost. Pohodlí. Proto jsme vytvořili Takeplace a vložili do něj vlastnosti, které jsme postrádali. Sdílejte s námi radost z organizování !\r\n\r\n[* takeplace-core-team.png *]',	1,	1352846800,	1298809140,	851,	1,	'',	'about',	1,	0,	1,	0,	'',	'O nás',	'',	''),
(38,	2,	76,	109,	'Bloky',	'cs-blocks',	'',	'',	1,	1345638350,	1298809680,	0,	1,	'',	'bloky',	0,	0,	0,	0,	'',	'Blocks',	'',	''),
(39,	3,	63,	64,	'Takeplace mobile',	'mobile_c',	'',	'Takeplace mobile helps you to be in touch with your event whenever and wherever you are.\r\n\r\nCheck your schedule, create your favorite one, list through participants and just enjoy the event you are attending.\r\n\r\nNo more queues during the registration. Show your ticket on the screen of your iPhone and let organizers do the check-in easily with QR codes scanning.\r\n\r\nVisit \"Takeplace.eu\":http://takeplace.eu and make your events run!\r\n\r\n== Screenshots ==\r\n\r\n.[clearfix]\r\n[* takeplace-mobile-2.png .(Event overview)[screenshot] *]\r\n[* takeplace-mobile-1.png .(Ticket)[screenshot] *]\r\n[* takeplace-mobile-4.png .(Participants)[screenshot] *]\r\n[* takeplace-mobile-3.png .(Schedule)[screenshot] *]\r\n[* takeplace-mobile-5.png .(About event)[screenshot] *]\r\n',	1,	1345638266,	1313568540,	11,	1,	'',	'about',	0,	0,	1,	0,	'',	'Takeplace mobile',	'',	''),
(40,	3,	77,	98,	'Home bloky',	'32-blocks',	'',	'',	1,	1345638329,	1298811060,	0,	1,	'',	'bloky',	0,	0,	0,	0,	'',	'Home-block',	'',	''),
(41,	3,	99,	100,	'Layout-Testimonial',	'cs-testimonial',	'home-box testimonial',	'== CO ŘEKLI O TAKEPLACE ==\r\n\r\n/--div .[bubble]\r\nTakeplace je služba, která v organizátorech, koordinátorech a účastnících zanechá nejlepší dojmy z pořádané akce. Ušetří čas i peníze pořadatelům nejrůznějších odborných setkání. Sdílejte s námi radost z organizování!\r\n\\--\r\n**Tým Takeplace**\r\n\r\n\"Další reference » .[more right]\":[article:35]',	1,	1345716427,	1298839380,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Testimonial',	'',	''),
(42,	3,	101,	102,	'Layout-Try',	'cs-try',	'',	'/--div .[try clearfix]\r\n\"&nbsp;Vyzkoušej&nbsp;&nbsp;TP .[try-button]{ onclick:_gaq.push([\'_trackEvent\', \'Button tracking\', \'Try click\'])}\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp .[link]\r\n\\--',	1,	1351594640,	1298841180,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Try',	'',	''),
(59,	2,	114,	115,	'Vývojář',	'vyvojar',	'== Java vývojář ==\r\n\r\nUmíš GWT, bereme tě!',	'== Java developer ==\r\n\r\nWe need you!',	0,	1350942370,	1350941760,	0,	1,	'',	'misto',	0,	0,	0,	0,	'',	'Vývojář',	'',	''),
(43,	3,	103,	104,	'Layout-Login',	'cs-login',	'',	'<form method=\"post\" action=\"https://app.takeplace.eu/takeplace/secure_login\" onsubmit=\"_gaq.push([\'_trackEvent\', \'Button tracking\', \'Log in click\']);\">\r\n<p>\r\n<label for=\"j_username\">Váš e-mail</label>\r\n<input type=\"text\" name=\"j_username\" id=\"j_username\" />\r\n</p>\r\n<p>\r\n<label for=\"j_password\">Heslo</label>\r\n<input type=\"password\" name=\"j_password\" id=\"j_password\" />\r\n</p>\r\n<input type=\"hidden\" name=\"gwt\" value=\"false\" />\r\n<input type=\"submit\" value=\"Log in\" />\r\n</form>\r\n\r\n\r\n\"Nejste registrovaní?  Vytvořte si účet » .[more]\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp',	1,	1351594669,	1298841300,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Login',	'',	''),
(44,	3,	105,	106,	'Layout-Pricing',	'cs-pricing',	'/--div .[price-title]\r\n== Easy pricing ==\r\n\r\nTransparent charges with no hidden fees. .[price-fee-desc]\r\n\\--\r\n\r\n/--div .[price-total]\r\n**$ 0** per event per&nbsp;year\r\n\\--\r\n\r\n/--div .[price-ticket]\r\n== Ticket sale ==\r\n\r\n2.9 % of ticket +&nbsp;$&nbsp;0.99 per each ticket\r\n\\--\r\n\r\n/--div .[price-fee card]\r\n3 % per transaction\r\n\\--\r\n\r\n/--div .[price-fee paypal]\r\n$ 0.30 + 3,4 % per transaction\r\n\\--\r\n\r\nMaximum charge of $ 9.95 per each ticket. Charge applies only when you make a sale. .[price-desc]',	'/--div .[price-ticket]\r\n== Prodej lístků ==\r\n\r\n2.9 % z ceny lístku\r\n + 20 Kč za každý lístek\r\n\\--\r\n\r\nMaximální poplatek 250,– Kč za lístek. .[price-desc]\r\n\r\n<div class=\"clearfix\"></div>\r\n\r\n-----------------------\r\n\r\n/--div .[price-fee card]\r\n3 % za transakci\r\n\\--\r\n\r\n/--div .[price-fee paypal]\r\n$ 0.30 + 3,4 % za transakci\r\n\\--\r\n\r\nPoplatek se aplikuje pouze za prodané lístky. .[price-desc]',	1,	1350852269,	1298841840,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Layout-Pricing',	'',	''),
(45,	3,	107,	108,	'Layout-Contact',	'cs-contact',	'',	'/--div .[email]\r\n=== E-mail: ===\r\n\r\ninfo@acemcee.com\r\n\\--\r\n\r\n/--div .[address]\r\n=== Adresa: ===\r\n\r\nACEMCEE, s. r. o.\r\n www.acemcee.com\r\n\r\nU Vodárny 3032/2a\r\n 616 00 Brno\r\n Česká republika\r\n\\--\r\n\r\n/--div .[phone]\r\n=== Telefon: ===\r\n\r\n+420 602 831 830\r\n\\--',	1,	1345716193,	1298895240,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'CONTACT INFORMATION',	'',	''),
(46,	4,	78,	79,	'Banner',	'32-banner',	'home-banner',	'/--div .[banner]\r\n<h1>Make your events run</h1>\r\n\r\nTakeplace je tu pro vaše pohodlí a radost z online\r\n organizování odborných akcí — konferencí, workshopů,\r\n symposií, seminářů, kongresů, veletrhů nebo výstav.\r\n\r\n\"<small>VYZKOUŠET ZDARMA</small> .[try-button]{ onclick:_gaq.push([\'_trackEvent\', \'Button tracking\', \'Try click\'])}\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp\r\n\\--',	1,	1351594628,	1298811060,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Banner',	'',	''),
(47,	4,	80,	81,	'Academic institutions',	'32-academic-institutions',	'home-case first',	'== \"Akademické instituce\":[article:34#academic-institutions] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Organizování akademických konferencí<br />&nbsp;&bull;&nbsp;&nbsp;Řízení vědeckých kongresů<br />&nbsp;&bull;&nbsp;&nbsp;Příprava kurzů a seminářů\":[article:34#academic-institutions]',	1,	1349864550,	1298811240,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Academic institutions',	'',	''),
(48,	4,	82,	83,	'Enterprise & commerce',	'32-enterprise-commerce',	'home-case',	'== \"Firmy & komerce\":[article:34#enterprise-commerce] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Zavádění komerčních workshopů<br />&nbsp;&bull;&nbsp;&nbsp;Vytváření firemního team-buildingu<br />&nbsp;&bull;&nbsp;&nbsp;Řízení výstav a veletrhů\":[article:34#enterprise-commerce]',	1,	1349864675,	1298811360,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Enterprise & commerce',	'',	''),
(49,	4,	84,	85,	'Public service',	'32-public-service',	'home-case',	'== \"Veřejná správa\":[article:34#public-service] ==\r\n\r\n\"&nbsp;&bull;&nbsp;&nbsp;Koordinování vzdělávacích seminářů<br/>&nbsp;&bull;&nbsp;&nbsp;Kontrola setkání městského zastupitelstva<br/>&nbsp;&bull;&nbsp;&nbsp;Organizace vládních symposií\":[article:34#public-service]',	1,	1349864772,	1298811720,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Public service',	'',	''),
(50,	4,	86,	87,	'Enjoy Takeplace',	'32-enjoy-takeplace',	'home-box flag green first',	'== Užijte si Takeplace ==\r\n\r\nPotřebujete pomoct při organizování akce?\r\n\r\n\"Kontaktujte nás » .[more]\":[article:37]',	1,	1349862737,	1298811840,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Enjoy Takeplace',	'',	''),
(51,	4,	88,	89,	'Awards we received',	'32-awards-we-received',	'home-box flag cyan',	'== Obdržená ocenění ==\r\n\r\n/--div .[clearfix]\r\n\"Institut rozvoje podnikání .[#ide]\":http://www.ide-vse.cz/home.html\r\n\"Unicredit Nápad roku .[#uni]\":http://www.napadroku.cz/aktualni-rocnik.html\r\n\\--\r\n\r\n\"Druhé místo v kategorii .[grey]\" Nápad roku 2010\r\n \"Speciální ocenění .[grey]\" Student podnikatel 2010',	1,	1345716714,	1298812020,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Awards we received',	'',	''),
(52,	4,	90,	91,	'Just start organizing',	'32-just-start-organizing',	'home-box flag blue login',	'== Začni organizovat akce ==\r\n\r\n<form method=\"post\" action=\"https://app.takeplace.eu/takeplace/secure_login\">\r\n<p>\r\n<label for=\"j_username\">Váš email</label>\r\n<input type=\"text\" name=\"j_username\" id=\"j_username\" title=\"Váš email\"  class=\"placeholder\" />\r\n</p>\r\n<p>\r\n<label for=\"j_password\">Vaše heslo</label>\r\n<input type=\"password\" name=\"j_password\" id=\"j_password\" title=\"Váše heslo\"  class=\"placeholder\" />\r\n</p>\r\n<input type=\"hidden\" name=\"gwt\" value=\"false\" />\r\n<input type=\"submit\" value=\"Log in\" />\r\n</form>\r\n\r\n\r\n\"Nejste registrovaní? Vytvořte si svůj účet » .[more]\":https://app.takeplace.eu/takeplace/takeplace.html#SignUp',	1,	1349864907,	1298812680,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Just start organizing',	'',	''),
(53,	4,	92,	93,	'Novinky z blogu',	'32-blog',	'home-box blog first',	'\"Blogujeme .[a-blog]\":http://blog.takeplace.eu/\r\n\r\n== Novinky z blogu ==\r\n\r\n=== \"Takeplace - one of the best eight start-ups from Eastern Europe\":http://blog.takeplace.eu/takeplace-one-of-the-best-eight-start-ups-fro ===\r\n\r\n*October 18th, 2012*\r\n\r\nWe enrolled into QPrize during the summer and now we can celebrate the great success. What are we talking about? The QPrize is Qualcomm Venture\'s Seed investment competition ... \"Read more »\":http://blog.takeplace.eu/takeplace-one-of-the-best-eight-start-ups-fro',	1,	1352846590,	1298812800,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'News from the blog',	'',	''),
(54,	4,	94,	95,	'TP kinetic tour',	'32-tp-video-tour',	'home-box video',	'\"[* video.png .(Video) *] .[videoLink]\":http://www.youtube.com/v/wHVgSL2Ca5c\r\n',	1,	1350849196,	1298813280,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'TP video tour',	'',	''),
(56,	2,	72,	73,	'Ceník',	'cenik',	'',	'',	0,	1350319675,	1345641000,	31,	1,	'',	'clanek',	1,	0,	1,	0,	'',	'Ceník',	'',	''),
(57,	4,	44,	45,	'Participated',	'1-participated',	'home-box-long first box-participated flag green',	'== We won or participated at ==\r\n\r\n[* cebit.png *]\r\n[* logo-nyc-partnering.png *]\r\n[* napad-roku.png *]\r\n[* q-prize.png *]\r\n[* seedcamp.png *]',	1,	1351270730,	1298811060,	0,	1,	'',	'blok',	0,	0,	0,	0,	'',	'Banner',	'',	''),
(60,	4,	96,	97,	'Participated',	'32-participated',	'home-box-long first box-participated flag green',	'== Vyhrali nebo ucastnili jsme se ==\r\n\r\n[* cebit.png *]\r\n[* logo-nyc-partnering.png *]\r\n[* napad-roku.png *]\r\n[* q-prize.png *]\r\n[* seedcamp.png *]',	1,	1351270671,	1351269660,	2,	1,	'',	'blok',	0,	0,	1,	0,	'',	'Participated',	'',	'');

DROP TABLE IF EXISTS `cms_pageparameter`;
CREATE TABLE `cms_pageparameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageId` int(10) unsigned NOT NULL,
  `parameterId` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pageId` (`pageId`,`parameterId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_pageparameter` (`id`, `pageId`, `parameterId`, `value`) VALUES
(127,	36,	1,	'press-media'),
(128,	6,	1,	'press-media'),
(125,	35,	1,	'customers'),
(124,	5,	1,	'customers'),
(121,	34,	1,	'overview'),
(74,	4,	1,	'overview'),
(109,	33,	1,	'features'),
(130,	18,	1,	'home-box blog'),
(96,	2,	1,	'features'),
(68,	30,	1,	'home-box-long first'),
(69,	31,	1,	'home-box-long first'),
(131,	8,	1,	'about-us'),
(132,	37,	1,	'about-us');

DROP TABLE IF EXISTS `cms_pageproduct`;
CREATE TABLE `cms_pageproduct` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idKategorie` (`pageId`,`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_parameter`;
CREATE TABLE `cms_parameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_parameter` (`id`, `title`, `type`) VALUES
(1,	'class',	'text');

DROP TABLE IF EXISTS `cms_photo`;
CREATE TABLE `cms_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `galleryId` int(10) unsigned DEFAULT NULL,
  `title_cs` varchar(255) DEFAULT NULL,
  `text_cs` text,
  `filename` varchar(255) DEFAULT NULL,
  `main_cs` tinyint(3) unsigned DEFAULT '0',
  `position_cs` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `galleryId` (`galleryId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_product`;
CREATE TABLE `cms_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_cs` varchar(255) NOT NULL,
  `url_cs` varchar(255) NOT NULL,
  `perex_cs` text NOT NULL,
  `active_cs` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `price` varchar(10) NOT NULL DEFAULT '0',
  `position_cs` tinyint(3) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_cs` (`url_cs`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_productparameter`;
CREATE TABLE `cms_productparameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `parameterId` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `productId` (`productId`,`parameterId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_template`;
CREATE TABLE `cms_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(255) NOT NULL DEFAULT '',
  `subtemplate` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `perex` text NOT NULL,
  `text` text NOT NULL,
  `sitemap` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_template` (`id`, `template`, `subtemplate`, `title`, `perex`, `text`, `sitemap`) VALUES
(1,	'default',	'clanek',	'Úvodní strana',	'',	'',	1),
(2,	'clanek',	'',	'Článek',	'',	'',	0),
(3,	'clanky',	'clanek',	'Články',	'',	'',	1),
(4,	'kontakt',	'',	'Kontakt',	'',	'',	1),
(5,	'bloky',	'blok',	'Bloky',	'',	'',	1),
(6,	'blok',	'',	'Blok',	'',	'',	1),
(7,	'about',	'',	'O nás',	'',	'',	1),
(8,	'sitemap',	'',	'Mapa stránek',	'',	'',	1),
(10,	'press',	'article',	'Media',	'',	'',	1),
(11,	'article',	'',	'Článek v tisku',	'',	'',	1),
(12,	'kariera',	'misto',	'Nabídka míst',	'',	'',	1),
(13,	'misto',	'',	'Místo',	'== Český titulek pozice ==\r\n\r\nLibovolný popis v češtině co má pozice obsahovat',	'== Anglický titulek pozice ==\r\n\r\nLibovolný popis v angličtině co má pozice obsahovat',	0);

DROP TABLE IF EXISTS `cms_templateparameter`;
CREATE TABLE `cms_templateparameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `templateId` int(10) unsigned NOT NULL,
  `parameterId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `templateId` (`templateId`,`parameterId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_templateparameter` (`id`, `templateId`, `parameterId`) VALUES
(12,	6,	1),
(13,	2,	1),
(14,	10,	1),
(15,	7,	1);

DROP TABLE IF EXISTS `cms_translate`;
CREATE TABLE `cms_translate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `cs` varchar(255) NOT NULL,
  `en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_translate` (`id`, `key`, `cs`, `en`) VALUES
(1,	'login',	'Přihlásit',	'Log in'),
(2,	'signup',	'Registrovat',	'Sign up'),
(3,	'or',	'nebo',	'or'),
(4,	'kariera',	'Nabídka volných míst',	'We are hiring'),
(5,	'testimonial',	'CO ŘEKLI O TAKEPLACE',	'TAKEPLACE USERS SAY');

DROP TABLE IF EXISTS `cms_user`;
CREATE TABLE `cms_user` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL DEFAULT '',
  `password` varchar(250) NOT NULL DEFAULT '',
  `firstName` varchar(50) NOT NULL DEFAULT '',
  `sureName` varchar(50) NOT NULL DEFAULT '',
  `lastLogin` int(15) NOT NULL DEFAULT '0',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `salt` varchar(8) NOT NULL DEFAULT '',
  `role` varchar(10) NOT NULL DEFAULT 'host',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_user` (`id`, `email`, `password`, `firstName`, `sureName`, `lastLogin`, `blocked`, `salt`, `role`) VALUES
(1,	'tomas.macuga@divdesign.cz',	'd0f3d0e985e9dd6b552c0949bfd02937bdba7ba7973307d4adca228b9b2e2c5c',	'Tomáš',	'Mačuga',	1365587864,	0,	'7INvcfsu',	'admin'),
(2,	'gressa@acemcee.com',	'5c5c77a9f98e979a01a5fa10ed73859c90a81f97300780c3075fa79e4c1ab47c',	'Palo',	'Grešša',	0,	0,	'qZIJIdC8',	'admin'),
(3,	'skrabalek@acemcee.com',	'7bd07e8d9ccd7e19792ebd32f2a2dffb019980f30efcd852e233135bc8b57ed3',	'Jarda',	'Škrabálek',	0,	0,	'NRlraaaL',	'admin'),
(4,	'staudek@acemcee.com',	'a01f5087d5d90215dbd4d1f2330a226babbb41dbe27e7fec25789abfaf15e4f1',	'Tom',	'Staudek',	1303670683,	0,	'uzWf36Xx',	'admin'),
(5,	'margetova@acemcee.com',	'378968ca924fc4a6a5f2ef7b6ed72b1a016046ae1606e8f33f63d821c6ee4e2d',	'Svetlana',	'Margetová',	0,	0,	'QBwTgEIX',	'admin');

-- 2013-04-10 11:58:50
